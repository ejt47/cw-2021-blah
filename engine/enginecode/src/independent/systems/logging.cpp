/*!
 * \file logging.cpp
 * \brief Logging class source file, defining the functionality declared in the Logging class header file. 
 */
#include "engine_pch.h"
#include "systems/logging.h"

namespace Engine
{
	// Initialise the shared pointers for the logger objects as a null pointers
	std::shared_ptr<spdlog::logger> Logging::s_pConsoleLogger = nullptr;
	std::shared_ptr<spdlog::logger> Logging::s_pFileLogger = nullptr;
	// Initialise the logger flag as false
	bool Logging::s_bLoggingFlag = false;

	void Logging::Start(SystemSignal init, ...)
	{
		// condition checks the logging system has not already been started
		if (!s_bLoggingFlag)
		{ 
			/*
				set_pattern function sets the formatting for the logged output see pattern flags section at the following link for further 
				info if confused in future https://github.com/gabime/spdlog/wiki/3.-Custom-formatting
				to further clarify for this particular format used (example from lab)
				%^ = start colour range, [%T] = time as HH::MM::SS encased in square brackets, %v = output message, %$ = end colour range 
			*/
			spdlog::set_pattern("%^[%T]: %v%$");
			/*
				I have looked in the documentation for set_level but couldn't find info, after some playing around I found it is like a 
				event output filter, i.e if set level to debug and you call for a trace output message nothing will be output
			*/
			spdlog::set_level(spdlog::level::trace);

			// Instantiates the console logger and points the shared pointer s_pConsoleLogger to it, passed string to give name of logger object
			s_pConsoleLogger = spdlog::stdout_color_mt("Console");

#ifdef NG_RELEASE
			// Instantiate and initialise the desired file path using a string
			// TODO: IF THIS IS ONLY USED IN RELEASE NEED TO CONSIDER MORE GENERAL PATHING FOR THE USER
			std::string sFilePath = "../engine/enginecode/logs/";
			/*
				Instantiate an empty char array buffer to record the date and time of the log as its filename, known to be 18 chars in length
				thanks to intellisense by hovering the cursor over the date time format string below, I considered using a vector but the 
				quantity of chars is not going to be dynamic, the format will always have the same quantity of chars
			*/
			char acDateTime[18];

			/* 
				time_t timeValue is an unspecified type object that can be used to represent times arithmetically, is initialised with
				std::time which returns the current time of system as time since the epoch(a specific point in time computers count from),
				std::time can be passed a nullptr or a reference to a time_t object, I tested both here actually using nullptr and &timeValue
				as arguments, the same outcome is achieved
			*/
			std::time_t timeValue = std::time(nullptr);
			/* 
				strftime function formats the time as a string, the formatted string below is copied to the empty acDateTime char array buffer
				as the system's local time which is obtained by passing a ref to our time_t object timeValue to the std::localtime function
			*/
			std::strftime(acDateTime, sizeof(acDateTime), "%d_%m_%y %H_%M_%S", std::localtime(&timeValue));
			// Call the append function twice on the sFilePath string, first passing acDateTime char array, then .txt
			sFilePath.append(acDateTime);
			sFilePath.append(".txt");

			/* 
				Initialise the s_pFileLogger shared pointer with the spdlog basic multi thread logger, passing the name for the logger as a 
				string, and the newly formatted sFilePath string with which to create the log file
			*/
			s_pFileLogger = spdlog::basic_logger_mt("File", sFilePath);
#endif

			// Flips console logging instance static bool flag to true ensures logging functions only work if logging system started
			s_bLoggingFlag = true;
		}
	}

	void Logging::Stop(SystemSignal close, ...)
	{
		// Access info function and pass a string information message
		s_pConsoleLogger->info("Stopping console logger");
		/* 
			Resets to nullptr, supposedly overkill, but better safe than sorry I say, especially because a Logging instance is created in
			application constructor on Logging system init
		*/
		s_pConsoleLogger.reset();

		// Resets console logging instance flag (just in case of funny business lol)
		s_bLoggingFlag = false;
	}
}