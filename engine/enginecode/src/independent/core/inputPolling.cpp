/*! 
 * \file inputPolling.cpp 
 * \brief InputPolling class source file through which functionality for platform specific code for input polling can be accessed through preprocessor compile time choices is defined.
 */

#include "engine_pch.h"
#include "core/inputPolling.h"

// Inclusion guards only necessary if platform specific code gets written, which currently is not the plan
//#ifdef NG_PLATFORM_WINDOWS
#include "platform/GLFW/GLFWInputPolling.h"
//#endif 


namespace Engine
{
// Inclusion guards only necessary if platform specific code gets written, which currently is not the plan
//#ifdef NG_PLATFORM_WINDOWS
	void InputPolling::SetCurrentWindow(void* currentWindow)
	{
		/*
			Initialise the input polling window when SetCurrentWindow is called, it is passed a void pointer to a GLFW window as the argument, which is static_cast back out to a 
			GLFWwindow pointer
		*/
		GLFWInputPolling::SetCurrentWindow(static_cast<GLFWwindow*>(currentWindow));
	}

	bool InputPolling::CheckKeyPressed(int32_t keyCode)
	{
		// Return the value returned by the GLFWInputPolling class function CheckKeyPressed, when passed a key identifcation code (as an integer or preprocessor define)
		return GLFWInputPolling::CheckKeyPressed(keyCode);
	}

	bool InputPolling::CheckMouseButtonPressed(int32_t buttonCode)
	{
		// Return the value returned by the GLFWInputPolling class function CheckMouseButtonPressed, when passed a button identifcation code (as an integer or preprocessor define)
		return GLFWInputPolling::CheckMouseButtonPressed(buttonCode);
	}

	glm::vec2 InputPolling::GetCurrentMousePosition()
	{
		// Return the object returned by the GLFWInputPolling class function GetCurrentMousePosition, which wiill be a glm::vec2 object representing a 2d vector
		return GLFWInputPolling::GetCurrentMousePosition();
	}
//#endif
}