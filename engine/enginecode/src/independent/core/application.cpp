/*!
 * \file application.cpp
 * \brief Application class source file to define the functionality previously declared in the Apllication class header file.
 */

#include "engine_pch.h"
#include <glad/glad.h>
#include "core/application.h"

//below inclusion guards only needed if I'm implementing platform specific windowing (currently not the plan)
//#ifdef NG_PLATFORM_WINDOWS
#include "platform/GLFW/GLFWSystem.h"
//#endif
#include "platform/GLFW/windowGLFW.h"

//#define STB_IMAGE_IMPLEMENTATION
//#include "stb_image.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "platform/OpenGL/OpenGLVertexArray.h"

#include "platform/OpenGL/OpenGLShader.h"

#include "platform/OpenGL/OpenGLTexture.h"

#include "rendering/subTexture.h"

#include "renderer/renderer3D.h"

#include "renderer/renderer2D.h"

namespace Engine {
	// Set static vars
	Application* Application::s_pApplicationInstance = nullptr;

	Application::Application()
	{
		if (s_pApplicationInstance == nullptr)
		{
			s_pApplicationInstance = this;
		}

		// Probably not the best way to wrap allocation of the memory for the different systems but will have to do for now
		MemoryAllocations();

		// Start systems here in the correct initialisation order

		// Call Start function on Logging system member management access shared pointer
		m_pLoggingSystem->Start();

		// Call Start function on Randomisation system member management access shared pointer
		m_pRandomisationSystem->Start();

		// Call Start function on Windowing system member management access shared pointer
		m_pWindowSystem->Start();

		WindowProperties windowProperties;
		// Below not fullscreen, no vysnc enabled
		//WindowProperties windowProperties("Engine", 1024, 1080);
		// Below not fullscreen, with vsync enabled
		//WindowProperties windowProperties("Engine", 1024, 1080, false, true);
		// Below is fullscreen, no vsync
		//WindowProperties windowProperties("Engine", 1024, 1080, true);
		// Below is fullscreen, with vsync enabled
		//WindowProperties windowProperties("Engine", 1024, 1080, true, true);

		/*
			Assign the pointer returned by WindowInterface::WindowCreate function to the WindowInterface member management access shared pointer m_pWindow.
			The above WindowProperties struct object must also be defined, as this is the only argument WindowCreate will take.

			For the event callbacks to work correctly this memory allocation cannot occur before the m_pWindowSystem calls its Start function,
			as such it cannot be wrapped in the MemoryAllocation function where, the memory for m_pWindowSystem is allocated. After using 
			the debugger to step through the program and find the point at which the exception was being thrown, I was able to narrow it
			down to the glfwCreateWindow function, which was returning NULL. Once I peaked the definition of this function I saw that there is a 
			preprocessor macro _GLFW_REQUIRE_INIT_OR_RETURN(NULL), which given my prior reading I immediately realised that the cause of the error
			was that glfwInit had not been called yet. Is the MemoryAllocation function a hindrance? It definitely makes it difficult to specify the
			order of allocation in relation to the System sub-class Start calls. For similar reasons InitEventCallbacks had to be moved further down in
			the order of initialisation, as event callbacks could not be bound to a window that could not be initialised in memory.
		*/
		m_pWindow.reset(WindowInterface::WindowCreate(windowProperties));
		//m_pWindow.reset(WindowGLFW::WindowCreate(windowProperties));
		/*
			Initialise EventCallbacks

			For similar reasons InitEventCallbacks had to be moved further down in the order of initialisation than it had previously been placed, as event 
			callbacks could not be bound to a window that could not be initialised in memory. 
		*/
		InitEventCallbacks();

		// Initialise the input polling system chosen at compile time (No choice currently being made, although inclusion guards are commented out where they may be needed)
		InitInputPolling();

		// Call Start function on TimingInterface member management access shared pointer
		m_pTiming->Start();
	}

	Application::~Application()
	{
		// Stop systems here in the reverse order they were initialised

		/*
			Stop timing by calling reset on the shared pointer, I believe this deletes the instance (should clean up after itself as it is a shared pointer anyway, but always
			better safe than sorry)
		*/
		m_pTiming.reset();

		// Stop Windowing system
		m_pWindowSystem->Stop();

		// Stop Randomisation system
		m_pRandomisationSystem->Stop();

		// Stop Logging system
		m_pLoggingSystem->Stop();
	}

	void Application::MemoryAllocations()
	{
		/*
			Call reset function on system member management access pointers declared in application.h, instantiate new instance of the systems
			that are being managed inside the std::shared_pointer reset function to allocate memory for these systems.
		*/

		// Logging system allocation
		m_pLoggingSystem.reset(new Logging);

		// Randomisation system allocation
		m_pRandomisationSystem.reset(new Randomisation);

		// Windowing system allocation, polymorphic, if I want to implement platform specific windowing systems need ifndef guards here to select which system is used
		//#ifdef NG_PLATFORM_WINDOWS
		m_pWindowSystem.reset(new GLFWSystem);
		//WindowProperties windowProperties("Engine", 1024, 1080, false, true);
		//m_pWindow.reset(WindowInterface::WindowCreate(windowProperties)); //THIS HAD TO BE MOVED OUT OF THIS FUNCTION, IS THIS FUNCTION A BAD IDEA??
		// IT APPEARS ONLY CLASSES WITH DEFAULT CONSTRUCTORS CAN HAVE THEIR MEMORY ALLOCATED THIS WAY, AND IF ONE ALLOCATION RELIES ON THE START CALL
		// OF ANOTHER FUNCTION THINGS WILL NOT WORK UNTIL THE ORDER IS SORTED CORRECTLY
		//#endif

		// ChronoTiming memory allocation, polymorphic so i.e. if I implement WinAPI timer need ifndef guards here to select which system used
		m_pTiming.reset(new ChronoTiming);
	}

	void Application::InitEventCallbacks()
	{
		/*
			Initialise the SetOnWindowCloseCallback function to the Application class function OnWindowClose using std::bind, where a reference to the Application class function OnWindowClose
			is passed as the calllable function, the this pointer is passed as a pointer to this instance(the application class instance), then the number of parameters is 
			passed using std::placeholders::_1 (it is the event being passed not the data for the event)

			NO NEED TO WRITE THIS COMMENT OUT A MILLION TIMES BUT ALL THE OTHER ABSTRACT EVENT CALLBACK FUNCTIONS ARE INTIALISED IN THE SAME WAY
		*/
		m_pWindow->GetEventHandler().SetOnWindowCloseCallback(std::bind(&Application::OnWindowClose, this, std::placeholders::_1));
		m_pWindow->GetEventHandler().SetOnWindowResizeCallback(std::bind(&Application::OnWindowResize, this, std::placeholders::_1));
		m_pWindow->GetEventHandler().SetOnWindowFocusCallback(std::bind(&Application::OnWindowFocus, this, std::placeholders::_1));
		m_pWindow->GetEventHandler().SetOnWindowLostFocusCallback(std::bind(&Application::OnWindowLostFocus, this, std::placeholders::_1));
		m_pWindow->GetEventHandler().SetOnWindowMoveCallback(std::bind(&Application::OnWindowMove, this, std::placeholders::_1));
		m_pWindow->GetEventHandler().SetOnKeyPressCallback(std::bind(&Application::OnKeyPress, this, std::placeholders::_1));
		m_pWindow->GetEventHandler().SetOnKeyReleaseCallback(std::bind(&Application::OnKeyRelease, this, std::placeholders::_1));
		m_pWindow->GetEventHandler().SetOnKeyTypedCallback(std::bind(&Application::OnKeyTyped, this, std::placeholders::_1));
		m_pWindow->GetEventHandler().SetOnMouseButtonPressCallback(std::bind(&Application::OnMouseButtonPress, this, std::placeholders::_1));
		m_pWindow->GetEventHandler().SetOnMouseButtonReleaseCallback(std::bind(&Application::OnMouseButtonRelease, this, std::placeholders::_1));
		m_pWindow->GetEventHandler().SetOnMouseMoveCallback(std::bind(&Application::OnMouseMove, this, std::placeholders::_1));
		m_pWindow->GetEventHandler().SetOnMouseScrollCallback(std::bind(&Application::OnMouseScroll, this, std::placeholders::_1));
	}

	bool Application::OnWindowClose(WindowClosed& windowClosed)
	{
		windowClosed.HandleEvent(true);
		m_bRunning = false;
//#ifdef NG_DEBUG
//		Logging::Debug("Window closed.");
//#endif
		return windowClosed.EventHandled();
	}

	bool Application::OnWindowResize(WindowResized& windowResized)
	{
		windowResized.HandleEvent(true);
//#ifdef NG_DEBUG
//		Logging::Debug("Window resized to {0} by {1}", windowResized.GetResizedWidth(), windowResized.GetResizedHeight());
//#endif
		return windowResized.EventHandled();
	}

	bool Application::OnWindowFocus(WindowFocus& windowFocus)
	{
		windowFocus.HandleEvent(true);
//#ifdef NG_DEBUG
//		Logging::Debug("Window is in focus.");
//#endif
		return windowFocus.EventHandled();
	}

	bool Application::OnWindowLostFocus(WindowLostFocus& windowLostFocus)
	{
		windowLostFocus.HandleEvent(true);
//#ifdef NG_DEBUG
//		Logging::Debug("Window is not in focus.");
//#endif
		return windowLostFocus.EventHandled();
	}

	bool Application::OnWindowMove(WindowMoved& windowMoved)
	{
		windowMoved.HandleEvent(true);
//#ifdef NG_DEBUG
//		Logging::Debug("Window moved to {0} in the x-axis, and {1} in the y-axis.", windowMoved.GetWindowPositionX(), windowMoved.GetWindowPositionY());
//#endif
		return windowMoved.EventHandled();
	}

	bool Application::OnKeyPress(KeyPressed& keyPressed)
	{
		keyPressed.HandleEvent(true);
//#ifdef NG_DEBUG
//		Logging::Debug("{0} key pressed, repeated: {1} (1 == true, 0 == false).", keyPressed.GetKeyCode(), keyPressed.GetKeyPressCount());
//#endif
		return keyPressed.EventHandled();
	}

	bool Application::OnKeyRelease(KeyReleased& keyReleased)
	{
		keyReleased.HandleEvent(true);
//#ifdef NG_DEBUG
//		Logging::Debug("{0} key released.", keyReleased.GetKeyCode());
//#endif
		return keyReleased.EventHandled();
	}

	bool Application::OnKeyTyped(KeyTyped& keyTyped)
	{
		keyTyped.HandleEvent(true);
//#ifdef NG_DEBUG
//		Logging::Debug("{0} key typed.", keyTyped.GetKeyCode());
//#endif
		return keyTyped.EventHandled();
	}

	bool Application::OnMouseButtonPress(MouseButtonPressed& buttonPressed)
	{
		buttonPressed.HandleEvent(true);
//#ifdef NG_DEBUG
//		Logging::Debug("{0} mouse button pressed.", buttonPressed.GetMouseButtonCode());
//#endif
		return buttonPressed.EventHandled();
	}

	bool Application::OnMouseButtonRelease(MouseButtonReleased& buttonReleased)
	{
		buttonReleased.HandleEvent(true);
//#ifdef NG_DEBUG
//		Logging::Debug("{0} mouse button released.", buttonReleased.GetMouseButtonCode());
//#endif
		return buttonReleased.EventHandled();
	}

	bool Application::OnMouseMove(MouseMoved& mouseMoved)
	{
		mouseMoved.HandleEvent(true);
//#ifdef NG_DEBUG
//		Logging::Debug("Position of the mouse moved to {0} in the x-axis, and {1} in the y-axis.", mouseMoved.GetMousePosDeltaX(), mouseMoved.GetMousePosDeltaY());
//#endif
		return mouseMoved.EventHandled();
	}

	bool Application::OnMouseScroll(MouseScrolled& mouseScrolled)
	{
		mouseScrolled.HandleEvent(true);
//#ifdef NG_DEBUG
//		Logging::Debug("Position of the mouse wheel scrolled by {0} in the x-axis, and {1} in the y-axis.", mouseScrolled.GetMouseScrollDeltaX(), mouseScrolled.GetMouseScrollDeltaY());
//#endif
		return mouseScrolled.EventHandled();
	}


	void Application::Run()
	{


// The region of code holding the raw data for the shapes to be drawn, in terms of vertex information, and index information. Data to be passed into
// OpengGL buffer objects/data types
#pragma region RAW_DATA

		/* 
			Raw data for a cube's vertex information, each line holds 8 floats, 3 representing vertex position, 3 representing the vertex normals,
			2 for the UV mapping texture coordinates, where each line holds the total information for each vertex that makes up the cube's faces,
			a cube has 6 faces (front, back, left, right, top, bottom) and each face has 4 vertices. Stored in a 8 x 24 array.
		*/
		float cubeVertices[8 * 24] = {
			//	 <------ Pos ------>  <--- normal --->  <-- UV -->
				 0.5f,  0.5f, -0.5f,  0.f,  0.f, -1.f,  0.f,   0.f,
				 0.5f, -0.5f, -0.5f,  0.f,  0.f, -1.f,  0.f,   0.5f,
				-0.5f, -0.5f, -0.5f,  0.f,  0.f, -1.f,  0.33f, 0.5f,
				-0.5f,  0.5f, -0.5f,  0.f,  0.f, -1.f,  0.33f, 0.f,

				-0.5f, -0.5f, 0.5f,   0.f,  0.f,  1.f,  0.33f, 0.5f,
				 0.5f, -0.5f, 0.5f,   0.f,  0.f,  1.f,  0.66f, 0.5f,
				 0.5f,  0.5f, 0.5f,   0.f,  0.f,  1.f,  0.66f, 0.f,
				-0.5f,  0.5f, 0.5f,   0.f,  0.f,  1.f,  0.33,  0.f,

				-0.5f, -0.5f, -0.5f,  0.f, -1.f,  0.f,  1.f,   0.f,
				 0.5f, -0.5f, -0.5f,  0.f, -1.f,  0.f,  0.66f, 0.f,
				 0.5f, -0.5f, 0.5f,   0.f, -1.f,  0.f,  0.66f, 0.5f,
				-0.5f, -0.5f, 0.5f,   0.f, -1.f,  0.f,  1.0f,  0.5f,

				 0.5f,  0.5f, 0.5f,   0.f,  1.f,  0.f,  0.f,   0.5f,
				 0.5f,  0.5f, -0.5f,  0.f,  1.f,  0.f,  0.f,   1.0f,
				-0.5f,  0.5f, -0.5f,  0.f,  1.f,  0.f,  0.33f, 1.0f,
				-0.5f,  0.5f, 0.5f,   0.f,  1.f,  0.f,  0.3f,  0.5f,

				-0.5f,  0.5f, 0.5f,  -1.f,  0.f,  0.f,  0.66f, 0.5f,
				-0.5f,  0.5f, -0.5f, -1.f,  0.f,  0.f,  0.33f, 0.5f,
				-0.5f, -0.5f, -0.5f, -1.f,  0.f,  0.f,  0.33f, 1.0f,
				-0.5f, -0.5f, 0.5f,  -1.f,  0.f,  0.f,  0.66f, 1.0f,

				 0.5f, -0.5f, -0.5f,  1.f,  0.f,  0.f,  1.0f,  1.0f,
				 0.5f,  0.5f, -0.5f,  1.f,  0.f,  0.f,  1.0f,  0.5f,
				 0.5f,  0.5f, 0.5f,   1.f,  0.f,  0.f,  0.66f, 0.5f,
				 0.5f, -0.5f, 0.5f,   1.f,  0.f,  0.f,  0.66f, 1.0f
		};

		/*
			Raw data for a pyramid's vertex information, each line holds 6 floats, 3 representing vertex position, 3 representing colour values,
			where each line holds the total information for each vertex that makes up the pyramid's faces, a pyramid has 5 faces 
			(1 square base, 4 triangle sides). The square face has 4 vertices, and the traingle face have 3 vertices. Colour is RGB values(no alpha)
		//*/
		float pyramidVertices[8 * 16] = {
			//	 <------ Pos ------>  <--- normal ---> 
				-0.5f, -0.5f, -0.5f,  0.f, -1.f, 0.f,		  0.f, 0.f,		//  square Magneta
				 0.5f, -0.5f, -0.5f,  0.f, -1.f, 0.f,		  0.f, 0.5f,
				 0.5f, -0.5f,  0.5f,  0.f, -1.f, 0.f,		  0.33f, 0.5f,
				-0.5f, -0.5f,  0.5f,  0.f, -1.f, 0.f,		  0.33f, 0.f,

				-0.5f, -0.5f, -0.5f,  -0.8944f, 0.4472f, 0.f, 0.33f, 0.5f,	//triangle Green
				-0.5f, -0.5f,  0.5f,  -0.8944f, 0.4472f, 0.f, 0.66f, 0.5f,
				 0.0f,  0.5f,  0.0f,  -0.8944f, 0.4472f, 0.f, 0.33f, 0.f,

				-0.5f, -0.5f,  0.5f,  0.f, 0.4472f, 0.8944f,  1.f, 0.f,	//triangle Red
				 0.5f, -0.5f,  0.5f,  0.f, 0.4472f, 0.8944f,  0.66f, 0.5f,
				 0.0f,  0.5f,  0.0f,  0.f, 0.4472f, 0.8944f,  1.f, 0.f,

				 0.5f, -0.5f,  0.5f,  0.8944f, 0.4472f, 0.f,  0.66f, 0.5f,	//  triangle Yellow
				 0.5f, -0.5f, -0.5f,  0.8944f, 0.4472f, 0.f,  0.33f, 1.f,
				 0.f,  0.5f,  0.0f,  0.8944f, 0.4472f, 0.f,  0.66f, 0.5f,

				 0.5f, -0.5f, -0.5f,  0.f, 0.4472f, -0.8944f, 1.f, 0.5f,		//  triangle Blue
				-0.5f, -0.5f, -0.5f,  0.f, 0.4472f, -0.8944f, 1.f, 0.5f,
				 0.0f,  0.5f,  0.0f,  0.f, 0.4472f, -0.8944f, 0.66f, 1.f
		};

		/*
			Array of fixed width unsigned 32 bit ints, holding the indices to be used for mapping the OpenGL drawing(indices) to the OpenGL
			data (vertices). The index array holds the order of which each triangle that makes up a face is drawn, using the vertices data to 
			know the position, normal and UV information(in the context of the previously defined cube). Stored in a 3 by 12 array, this array
			differs in size to the vertex array as it is the points of each triangle per row, as opposed to the vertex information per row.
			Where each face also has shared points between triangles.
		*/
		uint32_t cubeIndices[3 * 12] = {
			0, 1, 2,
			2, 3, 0, //face 1
			4, 5, 6,
			6, 7, 4, //face 2
			8, 9, 10,
			10, 11, 8, //face 3
			12, 13, 14,
			14, 15, 12, //face 4
			16, 17, 18,
			18, 19, 16, //face 5
			20, 21, 22,
			22, 23, 20 //face 6
		};

		/* 
			Array of fixed width unsigned 32 bit ints, holding the indices to be used for mapping the OpenGL drawing(indices) to the OpenGL 
			data (vertices). The index array holds the order of which each triangle that makes up a face is drawn, using the vertices data 
			to know the position and colour information(in the context of the previously defined pyramid). Stored in a 3 by 6 array, this array
			differs in size to the vertex array as it is the points of each triangle per row, as opposed to the vertex information per row.
		*/
		uint32_t pyramidIndices[3 * 6] =
		{
			0, 1, 2,
			2, 3, 0, //square base face
			4, 5, 6, //triangle 1
			7, 8, 9, //triangle 2
			10, 11, 12, //triangle 3
			13, 14, 15 //triangle 4
		};
#pragma endregion

// Create and bind the array buffers that hold the raw data above in the GPU see: https://www.khronos.org/opengl/wiki/Buffer_Object 
#pragma region GL_BUFFERS
		// Shared pointers for the cube's Vertex Array Object (VAO), the Vertex Buffer Object(VBO), the Index Buffer 
		// Object(IBO). Named variables with which the raw data above can be assigned to Vertex array and buffer objects and then identified, with
		// access to their class functions required for buffer setup.
		//API agnostic imps
		std::shared_ptr<VertexArray> cubeVAO;
		std::shared_ptr<VertexBuffer> cubeVBO;
		std::shared_ptr<IndexBuffer> cubeIBO;

		// Allocate the memory for a VertexArray object, set the cubeVAO pointer to point to it
		cubeVAO.reset(VertexArray::CreateVertexArray());

		// Instantiate a BufferLayout object for use with the cube vertex , where the elements in a stride are held in 2 sets of 3 floats and 
		// one set of 2 floats using the ShaderDataType enum class types
		BufferLayout cubeBL = { ShaderDataType::Float3, ShaderDataType::Float3, ShaderDataType::Float2 };

		// Allocate the memory for a VertexBuffer object where the cubeVertices array is the vertex data to point to, the size of the cubeVertices
		// array is assigned, and the BufferLayout object cubeBL defines the layout of each stride within the buffer
		cubeVBO.reset(VertexBuffer::CreateVertexBuffer(cubeVertices, sizeof(cubeVertices), cubeBL)); //agnostic imp

		// Allocate the memory for a IndexBuffer object, where the cubeIndices array is the index data to point to, the index total count is passed
		cubeIBO.reset(IndexBuffer::CreateIndexBuffer(cubeIndices, 36)); // agnostic imp

		// Access through the cubeVAO shared pointer the AddVertexBuffer function to bind the vertex attributes to the vertex buffer pointed to by cubeVBO
		cubeVAO->AddVertexBuffer(cubeVBO);
		// Access through the SetIndexBuffer function the SetIndexBuffer function to point the member pointer held by the VertexArray object 
		// pointed to by cubeVAO to the IndexBuffer object pointed to by cubeIBO, giving access to the index draw data required for drawing
		cubeVAO->SetIndexBuffer(cubeIBO);

		// See above, only difference really is that values appropriate for a pyramid shape are passed

		std::shared_ptr<VertexArray> pyramidVAO;

		std::shared_ptr<VertexBuffer> pyramidVBO; //agnostic imp

		std::shared_ptr<IndexBuffer> pyramidIBO;

		pyramidVAO.reset(VertexArray::CreateVertexArray());

		BufferLayout pyramidBL = { ShaderDataType::Float3, ShaderDataType::Float3};

		pyramidVBO.reset(VertexBuffer::CreateVertexBuffer(pyramidVertices, sizeof(pyramidVertices), cubeBL));
		
		pyramidIBO.reset(IndexBuffer::CreateIndexBuffer(pyramidIndices, 18));

		pyramidVAO->AddVertexBuffer(pyramidVBO);
		pyramidVAO->SetIndexBuffer(pyramidIBO);
#pragma endregion

// Define texture generation and binding
#pragma region TEXTURES
		
		std::shared_ptr<Texture> letterTexture;
		letterTexture.reset(Texture::CreateTexture("./assets/textures/letterCube.png"));

		std::shared_ptr<Texture> numberTexture;
		numberTexture.reset(Texture::CreateTexture("./assets/textures/numberCube.png"));

		unsigned char whitePx[4]{ 255, 255, 255, 255 };
		std::shared_ptr<Texture> plainWhiteTexture;	
		plainWhiteTexture.reset(Texture::CreateTexture(1, 1, 4, whitePx)); 

		std::shared_ptr<Texture> quadTester;
		quadTester.reset(Texture::CreateTexture("./assets/textures/deepbrainmachineman.png"));

		std::shared_ptr<Texture> quadTester1;
		quadTester1.reset(Texture::CreateTexture("./assets/textures/evendeeperbrain.png"));
		
		std::shared_ptr<Texture> quadTester2;
		quadTester2.reset(Texture::CreateTexture("./assets/textures/saitamaOK.png"));

#pragma endregion

// Create the shader programs from shaders stored in strings, to be passed to the GPU
#pragma region SHADERS
		//std::shared_ptr<OpenGLShader> TPShader;
		std::shared_ptr<Shader> TPShader;
		//TPShader.reset(new OpenGLShader("./assets/shaders/texturedPhong.glsl"));
		TPShader.reset(Shader::CreateShader("./assets/shaders/texturedPhong.glsl"));
#pragma endregion 

#pragma region MATERIALS

		// Instantiate a shared pointer to a Material object
		std::shared_ptr<Material> pyramidMaterial;
		// Allocate and assign a new Material object to be pointed to by the above shared pointer
		pyramidMaterial.reset(new Material(TPShader, plainWhiteTexture, { 0.f, 1.f, 0.9f, 0.5f }));

		// Instantiate a shared pointer to a Material object
		std::shared_ptr<Material> letterCubeMaterial;
		// Allocate and assign a new Material object to be pointed to by the above shared pointer
		letterCubeMaterial.reset(new Material(TPShader, letterTexture, { 1.f, 1.f, 1.f, 1.f }));

		// Instantiate a shared pointer to a Material object
		std::shared_ptr<Material> numberCubeMaterial;
		// Allocate and assign a new Material object to be pointed to by the above shared pointer
		numberCubeMaterial.reset(new Material(TPShader, numberTexture, { 1.f, 1.f, 1.f, 1.f }));

#pragma endregion


		// Instantiate and initialise a 4 by 4 view matrix for the MVP matrix, using glm::mat4, assigned to the value given 
		// by glm::lookAt, passing glm::vec3 for 3 component vectors representing eye position vector(position of camera viewpoint),
		// the centre position vector(the position of what is being looked at), and the up position vector(defining worlds upwards direction).
		// view represents the view matrix to be used in the MVP matrix(model view projection matrix)
		glm::mat4 view = glm::lookAt(glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 0.f, -1.f), glm::vec3(0.f, 1.f, 0.f));
		// Another 4 by 4 matrix using glm::vec4, the projection matrix, defined by the glm::perspective function giving the projection of the view 
		// from the camera to screen, where the parameters are, field of view in the y as an angle(in radians), the aspect ratio(should match window size),
		// float giving distance to the near plane, and a float giving distance to the far plane
		glm::mat4 projection = glm::perspective(glm::radians(45.f), 1024.f / 800.f, 0.1f, 100.f);

		// Instantiate am array of glm::mat4 matrices for each model, and initialise the translational position of each model using glm::translate
		// where the identity matrix is passed as the first param, and the translational position of the model is given as a glm::vec3 for the second param
		glm::mat4 models[3];
		models[0] = glm::translate(glm::mat4(1.0f), glm::vec3(-2.f, 0.f, -6.f));
		models[1] = glm::translate(glm::mat4(1.0f), glm::vec3(0.f, 0.f, -6.f));
		models[2] = glm::translate(glm::mat4(1.0f), glm::vec3(2.f, 0.f, -6.f));

		//2d view
		glm::mat4 view2D = glm::mat4(1.f);

		//2d projection
		glm::mat4 projection2D = glm::ortho(0.f, static_cast<float>(m_pWindow->GetWidth()), static_cast<float>(m_pWindow->GetHeight()), 0.f);

		glm::vec3 lightingInternalData[3] = { {1.f, 1.f, 1.f}, {1.f, 4.f, 6.f}, {0.f, 0.f, 0.f} };

		SceneWideUniform SWUniform3D;

		//view matrix
		SWUniform3D["u_view"] = std::pair<ShaderDataType, void*>(ShaderDataType::Mat4, static_cast<void*>(glm::value_ptr(view)));

		//projection matrix
		SWUniform3D["u_projection"] = std::pair<ShaderDataType, void*>(ShaderDataType::Mat4, static_cast<void*>(glm::value_ptr(projection)));
		//TPShader->uploadMat4("u_projection", projection);

		// upload the uniform RGB float values of the light in the shader prog with glUniform3f
		SWUniform3D["u_lightColour"] = std::pair<ShaderDataType, void*>(ShaderDataType::Float3, static_cast<void*>(glm::value_ptr(lightingInternalData[0])));

		// See above, but for light position
		SWUniform3D["u_lightPos"] = std::pair<ShaderDataType, void*>(ShaderDataType::Float3, static_cast<void*>(glm::value_ptr(lightingInternalData[1])));

		// View position
		SWUniform3D["u_viewPos"] = std::pair<ShaderDataType, void*>(ShaderDataType::Float3, static_cast<void*>(glm::value_ptr(lightingInternalData[2])));

		//2D SceneWideUniform
		SceneWideUniform SWUniform2D;

		// 2d view matrix
		SWUniform2D["u_view"] = std::pair<ShaderDataType, void*>(ShaderDataType::Mat4, static_cast<void*>(glm::value_ptr(view2D)));

		//2d projection matrix
		SWUniform2D["u_projection"] = std::pair<ShaderDataType, void*>(ShaderDataType::Mat4, static_cast<void*>(glm::value_ptr(projection2D)));


		//int iFrameCounter = 0;
		// float to record delta between frames (elapsed time)
		float fDeltaTime = 0.f;
		// float to record total delta time of code execution (accumulated time)
		//float fTotalDeltaTime = 0.f;
		// conditional while program loop
		Quad Quads[5] = {
			Quad::createHalfExtents({ 100.f, 100.f }, { 50.f, 50.f }), // half extent construct 
			Quad::createHalfExtents({ 200.f, 100.f }, 50.f), // half extent by single float val

			Quad::createTopLeftSize({ 300.f, 100.f }, { 100.f, 100.f }), // Is working correctly now, going by position of quad 2 if centred on same point as topleft of quad3
			Quad::createTopLeftSize({ 400.f, 100.f }, { 100.f }), // Is working correctly, top left aligns with quad 3 bottom right

			Quad::createTopLeftBottomRight({ 500.f, 100.f }, { 600.f, 200.f }) //Is working correctly as expected top left aligns with quad 3 bottom rugh
		};

		
		// Call glClearColor and pass RGBA values to set background colour
		glClearColor(0.6f, 0.6f, 0.6f, 1.f);

		// Initialise the 3d renderer
		Renderer3D::Init();

		// Initialise the 2d renderer
		Renderer2D::Init();

		// Float to hold the glyph rendering advance dist
		float fAdvance;

		while (m_bRunning)
		{
			// if total time exceeds 5 secs (counting starts at 0!!!) while loop bool condition is flipped
			//if (fTotalDeltaTime >= 1.f) m_bRunning = false;
			// get the frame delta time through the timing system shared pointer
			fDeltaTime = m_pTiming->GetDeltaTime();
			// Log the fps to console 
			//Logging::Debug("FPS: {0}", 1.f / fDeltaTime);
			//Logging::Debug("Frame number: {0}", iFrameCounter);
			// add frame delta time to total delta time to increment total time passed
			//fTotalDeltaTime += fDeltaTime;
			// chrono timing start function call functions as the frame delta reset (no need to code two functions that do the same thing)
			m_pTiming->Start();

			// Range based for loop counting through the model matrix, for each model rotate using glm::rotate, passing the model, the time since 
			// last step fDeltaTime(timestep), the up vector of the world space as a glm::vec3 vector
			for (auto& model : models) { model = glm::rotate(model, fDeltaTime, glm::vec3(0.f, 1.0, 0.f)); }
			// Do frame stuff
			// Call glClear and pass GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT to pass either the colour buffer or depth buffer binary representations
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
			// Call glEnable and pass GL_DEPTH_TEST to enable the depth buffer
			glEnable(GL_DEPTH_TEST); // IF NOT IN RUN LOOP LITTLE TO NO CONTROL OVER 3D ALPHA CHANNELS!!


			// begin
			Renderer3D::Begin(SWUniform3D);

			////submit
			Renderer3D::Submit(pyramidVAO, pyramidMaterial, models[0]);
			Renderer3D::Submit(cubeVAO, letterCubeMaterial, models[1]);
			Renderer3D::Submit(cubeVAO, numberCubeMaterial, models[2]);
			////end
			Renderer3D::End();

			glDisable(GL_DEPTH_TEST);

			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 

			Renderer2D::Begin(SWUniform2D);

			Renderer2D::Submit(Quads[0], glm::vec4({ 1.f, 0.f, 0.f, 1.f }));

			//Renderer2D::Submit(Quads[1], glm::vec4({ 1.f, 0.6f, 0.4f, 1.f }));
			//Renderer2D::Submit(Quads[1], quadTester);
			Renderer2D::Submit(Quads[1], glm::vec4({ 1.f, 0.6f, 0.4f, 0.47f }), quadTester, 315.f);

			Renderer2D::Submit(Quads[2], quadTester2, 135.f);

			//Renderer2D::Submit(Quads[3], glm::vec4({ 1.f, 0.5f, 1.f, 1.f }));
			//Renderer2D::Submit(Quads[3], quadTester1);
			Renderer2D::Submit(Quads[3], glm::vec4{ 1.f, 0.5f, 1.f, 0.47f }, quadTester1, 45.f);

			Renderer2D::Submit(Quads[4], glm::vec4({ 1.f, 0.5f, 0.5f, 1.f }), 225.f);

			//uint32_t /*ui32Advance */x = 550.f;
			Renderer2D::Submit('F', { 300.f, 200.f }, fAdvance, { 1.f, 1.f, 1.f, 1.f });

			Renderer2D::Submit("HOW DO U DO MY FRIEND", { 50.f, 500.f }, { 0.66f, 0.23f, 1.f, 1.f });
			Renderer2D::Submit(".. text exhausting ..", { 50.f, 550.f }, { 0.66f, 0.23f, 1.f, 1.f });

			glDisable(GL_DEPTH_TEST);
			glDisable(GL_BLEND);

			Renderer2D::End();


			//if (InputPolling::CheckKeyPressed(NG_KEY_DOWN)) Logging::Error("Down arrow key pressed.");
			//if (InputPolling::CheckMouseButtonPressed(NG_MOUSE_BUTTON_1)) Logging::Error("Left mouse button pressed.");
			//Logging::Error("Current mouse position, x = {0} : y = {1}", InputPolling::GetCurrentMousePosition().x, InputPolling::GetCurrentMousePosition().y);
			// do something here, a action frame to measure
			m_pWindow->OnUpdate(fDeltaTime);

			// increment frame counter
			//iFrameCounter++;
		};
	}
}
