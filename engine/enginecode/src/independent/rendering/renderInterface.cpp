/*! 
 *\file renderInterface.cpp
 * \brief RenderInterface spurce file, used to initialise the static instance to indicate the current graphics library in use by the renderer
 */

#include "engine_pch.h"
#include "rendering/renderInterface.h"
#include "rendering/indexBuffer.h"
#include "rendering/vertexBuffer.h"
#include "rendering/vertexArray.h"
#include "rendering/shader.h"
#include "rendering/texture.h"

#include "systems/logging.h"

#include"platform/OpenGL/OpenGLIndexBuffer.h"
#include "platform/OpenGL/OpenGLVertexBuffer.h"
#include "platform/OpenGL/OpenGLVertexArray.h"
#include "platform/OpenGL/OpenGLShader.h"
#include "platform/OpenGL/OpenGLTexture.h"


namespace Engine
{
	RenderInterface::GraphicsLib RenderInterface::sm_GraphicsLib = RenderInterface::GraphicsLib::OpenGL;

	IndexBuffer* IndexBuffer::CreateIndexBuffer(uint32_t* indices, uint32_t count)
	{
		switch (RenderInterface::GetLib())
		{
		case RenderInterface::GraphicsLib::None :
			Logging::Error("Must make use of a graphics library.");
			break;
		case RenderInterface::GraphicsLib::OpenGL : 
			Logging::Info("OpenGL in use.");
			return new OpenGLIndexBuffer(indices, count);
			break;
		case RenderInterface::GraphicsLib::Direct3D :
			Logging::Error("Direct3D selected for use, but unimplemented.");
			break;
		case RenderInterface::GraphicsLib::Vulkan :
			Logging::Info("Vulkan selected for use, but unimplemented.");
			break;
		}

		return nullptr; // if none selected
	}


	VertexBuffer* VertexBuffer::CreateVertexBuffer(void* vertices, uint32_t size, BufferLayout layout)
	{
		switch (RenderInterface::GetLib())
		{
		case RenderInterface::GraphicsLib::None :
			Logging::Error("Must make use of a graphics library.");
			break;
		case RenderInterface::GraphicsLib::OpenGL :
			Logging::Info("OpenGL in use.");
			return new OpenGLVertexBuffer(vertices, size, layout);
			break;
		case RenderInterface::GraphicsLib::Direct3D :
			Logging::Error("Direct3D selected for use, but unimplemented.");
			break;
		case RenderInterface::GraphicsLib::Vulkan :
			Logging::Info("Vulkan selected for use, but unimplemented.");
			break;
		}

		return nullptr; // if none selected
	}

	VertexArray* VertexArray::CreateVertexArray()
	{
		switch (RenderInterface::GetLib())
		{
		case RenderInterface::GraphicsLib::None :
			Logging::Error("Must make use of a graphics library.");
			break;
		case RenderInterface::GraphicsLib::OpenGL :
			Logging::Info("OpenGL in use.");
			return new OpenGLVertexArray();
			break;
		case RenderInterface::GraphicsLib::Direct3D :
			Logging::Error("Direct3D selected for use, but unimplemented.");
			break;
		case RenderInterface::GraphicsLib::Vulkan: 
			Logging::Info("Vulkan selected for use, but unimplemented.");
			break;
		}

		return nullptr; // if none selected
	}


	Shader* Shader::CreateShader(const char* vertexFP, const char* fragmentFP) //not working currently, fix if there is time
	{
		switch (RenderInterface::GetLib())
		{
		case RenderInterface::GraphicsLib::None:
			Logging::Error("Must make use of a graphics library.");
			break;
		case RenderInterface::GraphicsLib::OpenGL:
			Logging::Info("OpenGL in use.");
			return new OpenGLShader(vertexFP, fragmentFP);
			break;
		case RenderInterface::GraphicsLib::Direct3D:
			Logging::Error("Direct3D selected for use, but unimplemented.");
			break;
		case RenderInterface::GraphicsLib::Vulkan:
			Logging::Info("Vulkan selected for use, but unimplemented.");
			break;
		}

		return nullptr; // if none selected
	}

	Shader* Shader::CreateShader(const char* filePath) //working
	{
		switch (RenderInterface::GetLib())
		{
		case RenderInterface::GraphicsLib::None:
			Logging::Error("Must make use of a graphics library.");
			break;
		case RenderInterface::GraphicsLib::OpenGL:
			Logging::Info("OpenGL in use.");
			return new OpenGLShader(filePath);
			break;
		case RenderInterface::GraphicsLib::Direct3D:
			Logging::Error("Direct3D selected for use, but unimplemented.");
			break;
		case RenderInterface::GraphicsLib::Vulkan:
			Logging::Info("Vulkan selected for use, but unimplemented.");
			break;
		}

		return nullptr; // if none selected
	}

	Texture* Texture::CreateTexture(const char* filePath) //working
	{
		switch (RenderInterface::GetLib())
		{
		case RenderInterface::GraphicsLib::None:
			Logging::Error("Must make use of a graphics library.");
			break;
		case RenderInterface::GraphicsLib::OpenGL:
			Logging::Info("OpenGL in use.");
			return new OpenGLTexture(filePath);
			break;
		case RenderInterface::GraphicsLib::Direct3D:
			Logging::Error("Direct3D selected for use, but unimplemented.");
			break;
		case RenderInterface::GraphicsLib::Vulkan:
			Logging::Info("Vulkan selected for use, but unimplemented.");
			break;
		}

		return nullptr; // if none selected
	}

	Texture* Texture::CreateTexture(uint32_t width, uint32_t height, uint32_t channels, unsigned char* data) //not working either
	{
		switch (RenderInterface::GetLib())
		{
		case RenderInterface::GraphicsLib::None :
			Logging::Error("Must make use of a graphics library.");
			break;
		case RenderInterface::GraphicsLib::OpenGL :
			Logging::Info("OpenGL in use.");
			return new OpenGLTexture(width, height, channels, data);
			break;
		case RenderInterface::GraphicsLib::Direct3D :
			Logging::Error("Direct3D selected for use, but unimplemented.");
			break;
		case RenderInterface::GraphicsLib::Vulkan :
			Logging::Info("Vulkan selected for use, but unimplemented.");
			break;
		}

		return nullptr; // if none selected
	}
}