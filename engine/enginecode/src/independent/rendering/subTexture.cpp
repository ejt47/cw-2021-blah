/*!
 * \file subTexture.cpp
 * \brief SubTexture class source file, defining the functionality declared in the SubTexture class header
 */

#pragma once

#include "engine_pch.h"
#include "rendering/subTexture.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	SubTexture::SubTexture(const std::shared_ptr<Texture>& texture, const glm::vec2& UVStart, const glm::vec2 & UVEnd) : 
		ms_pTexture(texture), m_UVStart(UVStart), m_UVEnd(UVEnd)
	{
		m_Size.x = static_cast<int>((m_UVEnd.x - m_UVStart.x) * (ms_pTexture->GetWidthf()));
		m_Size.y = static_cast<int>((m_UVEnd.y - m_UVStart.y) * (ms_pTexture->GetHeightf()));
	}
	float SubTexture::GetTransformCoord_U(float U)
	{
		return m_UVStart.x + ((m_UVEnd.x - m_UVStart.x) * U); //lerp = start val x + (end val x - star val x) * tex U(tex x-axis-ish)
	}
	float SubTexture::GetTransformCoord_V(float V)
	{
		return m_UVStart.y + ((m_UVEnd.y - m_UVStart.y) * V); //lerp = start val y + (end val y - star val y) * tex v(tex y-axis-ish)
	}
	glm::vec2 SubTexture::GetTransformCoordsUV(glm::vec2 UV)
	{
		return m_UVStart + (m_UVEnd - m_UVStart) * UV; //lerp = start val + (end val - star val) * tex UV
	}
}