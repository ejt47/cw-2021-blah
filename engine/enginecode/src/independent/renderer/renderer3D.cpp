/*!
 * \renderer3D.cpp
 * \brief Source file for the Renderer3D class that defines the functionality declared by the class's header file
 */

#include "engine_pch.h"
#include "renderer/renderer3D.h"

namespace Engine
{

	std::shared_ptr<Renderer3D::InternalData> Renderer3D::ss_pData = nullptr;


	// Init function used to initialise the Renderer3D instance
	void Renderer3D::Init()
	{
		// Allocate memory for an InternalData struct object for the ss_pData static variable to be pointed to
		ss_pData.reset(new InternalData);

		// Definition of the defaultTexture variable declare instantitated by the memory allocation for the InternalData struct object pointed to by ss_pData
		unsigned char whitePx[4] = { 255.f, 255.f, 255.f, 255.f };
		ss_pData->defaultTexture.reset(Texture::CreateTexture(1, 1, 4, whitePx));

		// Definition of the default Tint variable instantiated by the memory allocation for the InternalData struct object pointed to by ss_pData
		ss_pData->defaultTint = { 1.f, 1.f, 1.f, 1.f };
	}

	/*
		Begin function begins a new render scene
	 */
	void Renderer3D::Begin(const SceneWideUniform& sceneWideUniform)
	{
		// Definitiion of the sceneWideUniform variable is initialised with the passed reference to a SceneWideUniform  that is to be uploaded by the function when called
		ss_pData->sceneWideUniform = sceneWideUniform;
	}
	/*
		Submit function submits the geometry to be rendered in order to associated rendered geometry to a model matrix
	*/
	void Renderer3D::Submit(const std::shared_ptr<VertexArray>& geometry, const std::shared_ptr<Material>& material, glm::mat4& model)
	{
		//Bind shader
		glUseProgram(material->GetShader()->GetShaderID());

		// Apply scenewideuniforms
		for (auto& pairedData : ss_pData->sceneWideUniform)
		{
			const char* uniformName = pairedData.first;
			ShaderDataType& shaderDataType = pairedData.second.first;
			void* addressOfValue = pairedData.second.second;

			//simon uses normal c-cast here, static_cast seems nicer 2 me, come back n see if it messes with the glsl upload
			// i.e. does it upload a static cast or the value returned by the static cast??
			switch (shaderDataType)
			{
			case ShaderDataType::Integer:
				material->GetShader()->uploadInt(uniformName, *static_cast<int*>(addressOfValue));
				break;
			case ShaderDataType::Float3:
				material->GetShader()->uploadFloat3(uniformName, *static_cast<glm::vec3*>(addressOfValue));
				break;
			case ShaderDataType::Float4:
				material->GetShader()->uploadFloat4(uniformName, *static_cast<glm::vec4*>(addressOfValue));
				break;
			case ShaderDataType::Mat4:
				material->GetShader()->uploadMat4(uniformName, *static_cast<glm::mat4*>(addressOfValue));
				break;
			}
		}

		// Apply material uniforms(per draw uniforms)
		material->GetShader()->uploadMat4("u_model", model);

		if (material->FlagCheck(Material::flagTexture)) glBindTexture(GL_TEXTURE_2D, material->GetTexture()->GetTextureID());
		else glBindTexture(GL_TEXTURE_2D, ss_pData->defaultTexture->GetTextureID());
		material->GetShader()->uploadInt("u_texData", 0);

		if (material->FlagCheck(Material::flagTint)) material->GetShader()->uploadFloat4("u_tint", material->GetTint());
		else material->GetShader()->uploadFloat4("u_tint", ss_pData->defaultTint);

		// Bind geometry thru VAO and IBO
		glBindVertexArray(geometry->GetRenderingID());
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry->GetIndexBuffer()->GetRenderingID());

		// Submit draw call
		glDrawElements(GL_TRIANGLES, geometry->GetDrawCount(), GL_UNSIGNED_INT, nullptr);
	}

	// End function ends the current render scene
	void Renderer3D::End()
	{
		ss_pData->sceneWideUniform.clear();
	}
}