/*! 
 * \file renderer2D.cpp  
 * \brief Source file for the Renderer2D class used to define the functionality declared in the Renderer2D class header file
 */

#include "engine_pch.h"
#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>

#include "systems/logging.h"
#include "renderer/renderer2D.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine 
{
	// Initialise the static shared pointer to the renderer2D internalData struct object to point to nullptr
	std::shared_ptr<Renderer2D::InternalData> Renderer2D::ss_pData = nullptr;

	// Init function used to initialise the Renderer2D instance
	void Renderer2D::Init()
	{
		// Allocate data for the static shared pointer to the InternalData struct object
		ss_pData.reset(new InternalData);

		// Instantiate and initialise an unsigned char to hold the colour bit data
		unsigned char whitePx[4] = { 255, 255, 255, 255 };
		// Allocate the memory for the default texture attribute of the InternalData object
		ss_pData->defaultTexture.reset(Texture::CreateTexture(1, 1, 4, whitePx));

		// Instantiate the default tint
		ss_pData->defaultTint = { 1.f, 1.f, 1.f, 1.f };

		// Allocate a default mat4 for the model attribute of the InternalData struct
		ss_pData->model = glm::mat4(1.f);

		// Allocate memory at the address pointed to by the InternalData object's shared pointer
		ss_pData->s_pShader.reset(Shader::CreateShader("./assets/shaders/quad.glsl"));

		/*
			Instantiate and initialise a 4 by 4 array of floats to describe the four vertices of a quad to be rendered,
			these vertices give a quad that is 1 by 1, a scale can be applied to this to create a quad of any size, when given varying
			pieces of data used to calculate the correct scale factor!

			vertices given describe a length of one between inverse negative values so that when scale is applied it is able to be
			applied correctly in the negative and positive areas of the quad!
		*/
		float vertices[4 * 4] =
		{
			/*--vertex--|----UV----*/
			-0.5f, -0.5f, 0.f, 0.f, //top left vertex, uv coord
			-0.5f, 0.5f, 0.f, 1.f, //bottom left vert, uv coord
			0.5f, 0.5f, 1.f, 1.f, //bottom right vert, uv coord
			0.5f, -0.5f, 1.f, 0.f //top right vert, uv coord
		};

		// Instantiate and initialise a 4 element indices array of unsigned 32bit integers
		uint32_t indices[4] = { 0, 1, 2, 3 };

		ss_pData->s_pVertexArray.reset(VertexArray::CreateVertexArray());

		// Instantiate a shared pointer to a vertexBuffer
		std::shared_ptr<VertexBuffer> vertexBuffer;
		// Allocate the memory for the vertex buffer
		vertexBuffer.reset(VertexBuffer::CreateVertexBuffer(vertices, sizeof(vertices), BufferLayout({ ShaderDataType::Float2, ShaderDataType::Float2 })));
		// Add the vertex buffer to the InternalData objects vertex array attribute
		ss_pData->s_pVertexArray->AddVertexBuffer(vertexBuffer);

		// repeat above for index buffer
		std::shared_ptr<IndexBuffer> indexBuffer;
		indexBuffer.reset(IndexBuffer::CreateIndexBuffer(indices, 4));
		ss_pData->s_pVertexArray->SetIndexBuffer(indexBuffer);

		// Initialise free type
		if (FT_Init_FreeType(&ss_pData->freeTypeLib)) Logging::Error("Error: could not initialise free type...");

		// const char pointer string to the file path of the font file
		const char* fontFilePath = "./assets/fonts/arial_narrow_7.ttf";

		//
		ss_pData->glyphBufferDimensions = { 256, 256 };
		//ss_pData->ui32Channels = 4;
		ss_pData->ui32GlyphBufferSize = ss_pData->glyphBufferDimensions.x * ss_pData->glyphBufferDimensions.y * 4 * sizeof(unsigned char);
		ss_pData->glyphBuffer.reset(static_cast<unsigned char*>(malloc(ss_pData->ui32GlyphBufferSize)));

		// Load font for use through free type
		if (FT_New_Face(ss_pData->freeTypeLib, fontFilePath, 0, &ss_pData->fontFace)) Logging::Error("Error: FreeType was unable to load font: {0} ...", fontFilePath);

		// Set the font size
		int32_t fontSize = 48;
		if (FT_Set_Pixel_Sizes(ss_pData->fontFace, 0, fontSize)) Logging::Error("Error: Freetype was unable to load the font size '{0}' ...", fontSize);


		// Fill the bbuffer using memset
		memset(ss_pData->glyphBuffer.get(), 148, ss_pData->ui32GlyphBufferSize);

		// Initialise the font texture
		ss_pData->fontTexture.reset(Texture::CreateTexture(ss_pData->glyphBufferDimensions.x, ss_pData->glyphBufferDimensions.y, 4, nullptr)); //null data

		// Edit the glyph buffer then send it to the gpu
		ss_pData->fontTexture->Edit(0, 0, ss_pData->glyphBufferDimensions.x, ss_pData->glyphBufferDimensions.y, ss_pData->glyphBuffer.get());
	};

	// Begin function begins a new 2D rendering scene
	void Renderer2D::Begin(const SceneWideUniform& sceneWideUniform)
	{
		//bind shader
		glUseProgram(ss_pData->s_pShader->GetShaderID());

		//apply scene wide unis
		for (auto& pairedData : sceneWideUniform)
		{
			const char* uniformName = pairedData.first;
			ShaderDataType shaderDataType = pairedData.second.first;
			void* addressOfValue = pairedData.second.second;

			//simon uses normal c-cast here, static_cast seems nicer 2 me, come back n see if it messes with the glsl upload
			// i.e. does it upload a static cast or the value returned by the static cast??
			switch (shaderDataType)
			{
			case ShaderDataType::Integer:
				ss_pData->s_pShader->uploadInt(uniformName, *static_cast<int*>(addressOfValue));
				break;
			case ShaderDataType::Float3:
				ss_pData->s_pShader->uploadFloat3(uniformName, *static_cast<glm::vec3*>(addressOfValue));
				break;
			case ShaderDataType::Float4:
				ss_pData->s_pShader->uploadFloat4(uniformName, *static_cast<glm::vec4*>(addressOfValue));
				break;
			case ShaderDataType::Mat4:
				ss_pData->s_pShader->uploadMat4(uniformName, *static_cast<glm::mat4*>(addressOfValue));
				break;
			}
		}

		// Bind geometry thru VAO and IBO
		glBindVertexArray(ss_pData->s_pVertexArray->GetRenderingID());
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ss_pData->s_pVertexArray->GetIndexBuffer()->GetRenderingID());

	};

	// Submit function submits quad primitives for rendering
	void Renderer2D::Submit(const Quad& quad, glm::vec4& tint)
	{
		glBindTexture(GL_TEXTURE_2D, ss_pData->defaultTexture->GetTextureID());
		ss_pData->model = glm::scale(glm::translate(glm::mat4(1.f), quad.m_translate), quad.m_scale);

		ss_pData->s_pShader->uploadInt("u_texData", 0);
		ss_pData->s_pShader->uploadFloat4("u_tint", tint);
		ss_pData->s_pShader->uploadMat4("u_model", ss_pData->model);

		glDrawElements(GL_QUADS, ss_pData->s_pVertexArray->GetDrawCount(), GL_UNSIGNED_INT, nullptr);
	};

	// Submit function submits quad primitives for rendering with texture only
	void Renderer2D::Submit(const Quad& quad, std::shared_ptr<Texture>& texture)
	{
		glBindTexture(GL_TEXTURE_2D, texture->GetTextureID());
		ss_pData->model = glm::scale(glm::translate(glm::mat4(1.f), quad.m_translate), quad.m_scale);

		ss_pData->s_pShader->uploadInt("u_texData", 0);
		ss_pData->s_pShader->uploadFloat4("u_tint", ss_pData->defaultTint);
		ss_pData->s_pShader->uploadMat4("u_model", ss_pData->model);

		glDrawElements(GL_QUADS, ss_pData->s_pVertexArray->GetDrawCount(), GL_UNSIGNED_INT, nullptr);
	};

	// Submit function submits quad primitives for rendering with tint and texture
	void Renderer2D::Submit(const Quad& quad, glm::vec4& tint, std::shared_ptr<Texture>& texture)
	{
		glBindTexture(GL_TEXTURE_2D, texture->GetTextureID());
		ss_pData->model = glm::scale(glm::translate(glm::mat4(1.f), quad.m_translate), quad.m_scale);

		ss_pData->s_pShader->uploadInt("u_texData", 0);
		ss_pData->s_pShader->uploadFloat4("u_tint", tint);
		ss_pData->s_pShader->uploadMat4("u_model", ss_pData->model);

		glDrawElements(GL_QUADS, ss_pData->s_pVertexArray->GetDrawCount(), GL_UNSIGNED_INT, nullptr);
	};


	// Submit function submits quad primitives for rendering with tint, and orientation(in degree, func converts to rad)
	void  Renderer2D::Submit(const Quad& quad, glm::vec4& tint, float angle)
	{
		glBindTexture(GL_TEXTURE_2D, ss_pData->defaultTexture->GetTextureID());
		ss_pData->model = glm::scale(glm::rotate(glm::translate(glm::mat4(1.f), quad.m_translate), glm::radians(angle), { 0.f, 0.f, 1.f }), quad.m_scale);

		ss_pData->s_pShader->uploadInt("u_texData", 0);
		ss_pData->s_pShader->uploadFloat4("u_tint", tint);
		ss_pData->s_pShader->uploadMat4("u_model", ss_pData->model);

		glDrawElements(GL_QUADS, ss_pData->s_pVertexArray->GetDrawCount(), GL_UNSIGNED_INT, nullptr);
	};

	// Submit function submits quad primitives for rendering with texture, and orientation(in degree, func converts to rad)
	void  Renderer2D::Submit(const Quad& quad, std::shared_ptr<Texture>& texture, float angle)
	{
		glBindTexture(GL_TEXTURE_2D, texture->GetTextureID());
		ss_pData->model = glm::scale(glm::rotate(glm::translate(glm::mat4(1.f), quad.m_translate), glm::radians(angle), { 0.f, 0.f, 1.f }), quad.m_scale);

		ss_pData->s_pShader->uploadInt("u_texData", 0);
		ss_pData->s_pShader->uploadFloat4("u_tint", ss_pData->defaultTint);
		ss_pData->s_pShader->uploadMat4("u_model", ss_pData->model);

		glDrawElements(GL_QUADS, ss_pData->s_pVertexArray->GetDrawCount(), GL_UNSIGNED_INT, nullptr);
	};

	// Submit function submits quad primitives for rendering with tint, texture, and orientation(in degree, func converts to rad)
	void Renderer2D::Submit(const Quad& quad, glm::vec4& tint, std::shared_ptr<Texture>& texture, float angle)
	{
		//6.283185 rad(2piRad) = 360deg, piRad = 180deg, but use glm::radians func
		glBindTexture(GL_TEXTURE_2D, texture->GetTextureID());
		ss_pData->model = glm::scale(glm::rotate(glm::translate(glm::mat4(1.f), quad.m_translate), glm::radians(angle), {0.f, 0.f, 1.f}), quad.m_scale);

		ss_pData->s_pShader->uploadInt("u_texData", 0);
		ss_pData->s_pShader->uploadFloat4("u_tint", tint);
		ss_pData->s_pShader->uploadMat4("u_model", ss_pData->model);

		glDrawElements(GL_QUADS, ss_pData->s_pVertexArray->GetDrawCount(), GL_UNSIGNED_INT, nullptr);
	}

	void Renderer2D::Submit(char glyph, const glm::vec2& position, float& advance, glm::vec4 tint)
	{
		// Get character glyph
		if (FT_Load_Char(ss_pData->fontFace, glyph, FT_LOAD_RENDER)) Logging::Error("Error: Freetype was unable to load the character glyph to render {0}...", glyph);
		else
		{
			// Get the character glyph
			uint32_t ui32GlyphWidth = ss_pData->fontFace->glyph->bitmap.width;
			uint32_t ui32GlyphHeight = ss_pData->fontFace->glyph->bitmap.rows;
			glm::vec2 glyphSize(ui32GlyphWidth, ui32GlyphHeight);
			glm::vec2 glyphOrientation(ss_pData->fontFace->glyph->bitmap_left, -ss_pData->fontFace->glyph->bitmap_top);

			// Calculate the advance(horizontal width (and height, but just width for now) to increment by after each glyph rendering, as in how far to advance distance-wise
			advance = static_cast<float>(ss_pData->fontFace->glyph->advance.x >> 6);

			// Calculate the quad to render the glyph to, by calculating two glm::vec2 ojects, one giving the quad's half extents, one giving it's centre positon
			glm::vec2 glyphHalfExtents = { ss_pData->fontTexture->GetWidthf() * 0.5f, ss_pData->fontTexture->GetHeightf() * 0.5f };
			glm::vec2 glyphCentrePos = (position + glyphOrientation) + glyphHalfExtents;

			// Instantiate and initialise the render area quad
			Quad glyphQuad = Quad::createHalfExtents(glyphCentrePos, glyphHalfExtents);
			
			//unsigned char* uPCMono2AlphaBuffer =  uPCMonochrome2Alpha(ss_pData->fontFace->glyph->bitmap.buffer, ui32GlyphWidth, ui32GlyphHeight);
			//ss_pData->fontTexture->Edit(0, 0, ui32GlyphWidth, ui32GlyphHeight, uPCMono2AlphaBuffer);
			//delete(uPCMono2AlphaBuffer);

			uPCMonochrome2Alpha(ss_pData->fontFace->glyph->bitmap.buffer, ui32GlyphWidth, ui32GlyphHeight);
			ss_pData->fontTexture->Edit(0, 0, ss_pData->glyphBufferDimensions.x, ss_pData->glyphBufferDimensions.y, ss_pData->glyphBuffer.get());

			// Submit the quad with the font texture 
			Submit(glyphQuad, tint, ss_pData->fontTexture);
		}
	}

	//! Submit a string of 'glyphs' to be rendered at a position with a coloured tint and an advance
	void Renderer2D::Submit(const char* glyphString, const glm::vec2& position, const glm::vec4 tint)
	{
		uint32_t stringLength = strlen(glyphString);
		float advance = 0.f, x = position.x;

		for (int32_t i = 0; i < stringLength; i++)
		{
			Submit(glyphString[i], { x, position.y }, advance, tint);
			x += advance;
		};
	};

	// Ends the open render scene
	void Renderer2D::End()
	{
		// TODO IMPLEMENT SOME CLEANUP
	}

	// MAKE SURE YOU'RE FREEING RAW MEM ALLOCATIONS !!!!!!!!!!!
	void Renderer2D::uPCMonochrome2Alpha(unsigned char* monoBuffer, uint32_t width, uint32_t height)
	{
		memset(ss_pData->glyphBuffer.get(), 0, ss_pData->ui32GlyphBufferSize);

		/*uint32_t ui32MonoBufferSize = width * height * 4 * sizeof(unsigned char);
		unsigned char* uPCResult = (unsigned char*)malloc(ui32MonoBufferSize);
		memset(uPCResult, 255, sizeof(ui32MonoBufferSize));*/

		unsigned char* uPCPointAndWalk = ss_pData->glyphBuffer.get();
		for (int32_t i = 0; i < height; i++)
		{
			for (int32_t j = 0; j < width; j++)
			{	// Set Green colour channel bit to blank white
				*uPCPointAndWalk = 255; uPCPointAndWalk++;  
				// Then set blue colour channel bit to white and increment to next bit
				*uPCPointAndWalk = 255; uPCPointAndWalk++;
				// set alpha channel colour depth bits to blank white setting to white then increment
				*uPCPointAndWalk = 255; uPCPointAndWalk++;
				*uPCPointAndWalk = *monoBuffer; // increment a gap and increment to red monochrome channel colour bit
				uPCPointAndWalk++;
				monoBuffer++; //Incremement to next monochrome pixel
			};
			uPCPointAndWalk += (ss_pData->glyphBufferDimensions.x - width) * 4;
		}
	};

	// Create half extents from the centre of the quad, pass 2 const references to glm::vec2 objects, for centre pos and half extent pos, for a rectangular quad(not square)
	Quad Quad::createHalfExtents(const glm::vec2& centre, const glm::vec2& halfExtents) 
	{
		// Initialise a default constructed quad(see vertices array), will always be centred at 0, 0 with line lengths of 1 between +/-0.5 x or y
		Quad quadHalfExtents;

		// translation member no calculation required, centre set by passed ref to glm::vec2 centre
		quadHalfExtents.m_translate = glm::vec3(centre, 0.f);

		/* 
			Construction of a quad object of the desired size from half extents requires a scale factor calculation, where the passed half extent's
			x and y components are multiplied by 2, giving the full length of a side(magnitude of vec between two vertices). Need to consider that
			the default geometry of the quad set by the array of floats "vertices" in the Renderer2D Init function initialises the default 1 by 1
			quad with vertex positions at equidistant positions in negative and positive, ensuring any quad of any size can be constructed by 
			scaling in negative and positive i.e. 2 by 2 quad at 0,0 (origin at quads fixed axis) would have half extents of 1, at +/-1 vertex positions 
			around that origin needs multiplication of 2 to give "full extents", if scaling from the default 1 by 1 quad at 0,0 with half extents of 0.5
			between the x and y -axis(where the distance from vertex position between that position and the other vertex position with which it draws a line 
			is always equidistant on both sides of the axes on both sides of the axes) toscale to the 2 by 2 quad from the 1 by 1 quad the multiplication  
			from the half extents of 1 by 1 quad to the half extents of the 2 by 2 quad is calculated by 0.5 to 1 is = 0.5 * 2, and from -0.5 to -1 is = -0.5 * 2
			therefore the scale factor is the half extent components multiplied by 2. A less clear example given could be half extents centred at x512, y278, 
			with half extents passed as x256, y139, where it is not immediately obvious that 512 and 278 / 2 = 256 and 139 giving the half extents, when scaled
			back up by 2 the values returned will give "full extents" of 512, and 278. 
		*/
		quadHalfExtents.m_scale = glm::vec3(halfExtents * 2.f, 1.f);

		return quadHalfExtents;
	};

	// Create half extents from the centre of the quad, pass a const reference to a glm::vec2 for centre pos and a float for half extent pos, for a exactly square quare quad
	Quad Quad::createHalfExtents(const glm::vec2& centre, float halfExtents)
	{
		//same as above but calculation of vector lengths between vertex positions is different
		Quad quadHalfExtents;

		//see above
		quadHalfExtents.m_translate = glm::vec3(centre, 0.f);
		
		// This calculation gives an exactly square quad where the half extent between all vertices is exactly the same, AS BOTH HALF EXTENTS ARE ONE SINGLE VALUE
		quadHalfExtents.m_scale = glm::vec3(halfExtents * 2, halfExtents * 2, 1.f);

		return quadHalfExtents;
	};

	/*
		Create half extents from the centre of the quad, pass a const reference to a glm::vec2 for top left pos and for size, creates quad from a top left vertex position
		and the known width and height of the created quad, orientation can then be applied post-construction, assumption that size is always positive? what if scaling down?

		MAYBE SHOULD ASSERT SIZE X AND Y MUST BE > 0 ? THIS ALLOWS FOR SCALING DOWN, IF YOU THINK ABOUT IT APPLYING A NEGATIVE SIZE IS GOING TO INVERT THE QUAD, WON'T BE AN ISSUE!
		NOT GOING TO PLACE AN ASSERT GIVEN THE PRIOR LINE, TOO COST WORTHY FOR THE RENDERER
	*/
	Quad Quad::createTopLeftSize(const glm::vec2& topLeft, const glm::vec2& size)
	{
		//same as above but calculation of vector lengths between vertex positions is different
		Quad quadHalfExtents;

		/*
			if size alway positive, then topleft x + size x = +x direction = towards the right, topleft y + size y = +y dir = downwards(inverted y-axis), so therfore
			the sum of top left in x + size in x = top right vertex pos and top left y + size y = bottom left, therefore bottom right = (top right x, bottom left y).

			so centre.x = top left x + (top right x / 2) OR top right x - (top right x / 2)

			and then centre y = top left y + (bottom left y / 2) OR other calcs but this least steps i believe?

			then HE(x) = length of topLeft x +(size.x / 2) and HE(y) = topLeft y + (size.y / 2)
		*/
		quadHalfExtents.m_translate = glm::vec3((topLeft.x /*+ (size.x * 0.5)*/), (topLeft.y /*+ (size.y * 0.5)*/), 0.f); //DON'T CHANGE NOW WORKS

		/*
			If HE(x) = length of topLeft x +(size.x * 0.5) and HE(y) = topLeft y + (size.y * 0.5), then it should be true that HE(x)*2 = scale x, HE(y)*2 = scale y
		*/
		quadHalfExtents.m_scale = glm::vec3((/*topLeft.x + */(size.x/* * 0.5*/)), (/*(topLeft.y + */(size.y/* * 0.5*/)), 1.f);

		// Return the quad with the now assigned attributes THIS NEEDS TESTING I PRAY TO JEBUS THIS WORKS
		return quadHalfExtents;
	};

	/*
		Create half extents from the centre of the quad, pass a const reference to a glm::vec2 for top left pos and a float for size, creates quad from a top left vertex position 
		and the area of the created quad, orientation can then be applied post-construction, assumption that this gives an exactly square quad
	*/
	Quad Quad::createTopLeftSize(const glm::vec2& topLeft, float size)
	{
		//same as above but calculation of vector lengths between vertex positions is different
		Quad quadHalfExtents;

		//see above
		quadHalfExtents.m_translate = glm::vec3((topLeft.x/* + (size * 0.5)*/), (topLeft.y /*+ (size * 0.5)*/), 0.f);

		// This calculation gives an exactly square quad where the half extent between all vertices is exactly the same
		// it should be posible to just scale the original by sthe passed float size in this case?
		quadHalfExtents.m_scale = glm::vec3(size, size, 1.f);

		// return quad with newly assigned atributes
		return quadHalfExtents;
	};

	Quad Quad::createTopLeftBottomRight(const glm::vec2& topLeft, const glm::vec2& bottomRight) 
	{
		//diff x = bottom right x - top left x
		//diff y = bottom right y - top left y
		//then centre is equal to ((bottom right x - top left x)*0.5, (bottom right y - top left y)*0.5, 0.f)
		//same as above but calculation of vector lengths between vertex positions is different
		Quad quadHalfExtents;

		//see above
		quadHalfExtents.m_translate = glm::vec3((bottomRight.x - (bottomRight.x - topLeft.x)/* * 0.5*/), (bottomRight.y - (bottomRight.y - topLeft.y)/* * 0.5*/), 0.f);

		// This calculation gives an exactly square quad where the half extent between all vertices is exactly the same
		quadHalfExtents.m_scale = glm::vec3((bottomRight.x - topLeft.x)/**0.5*/, (bottomRight.y - topLeft.y)/**0.5*/, 1.f);

		// return quad with newly assigned atributes
		return quadHalfExtents;
	};
}