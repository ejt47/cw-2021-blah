/*!
 * \file GLFWSystem.cpp
 * \brief GLFWSystem class source file, defining the functionality declared in the GLFWSystem class header file.
 */
#include "engine_pch.h"

#include "platform/GLFW/GLFWSystem.h"
#include "independent/systems/logging.h"

namespace Engine
{
	void GLFWSystem::Start(SystemSignal init, ...)
	{
		/*
			my implementation, based on Simon's example using the assert function to assert that glfwInit() must return 1(true), otherwise GLFW has failed to initialise and 
			will call glfwTerminate(), cleaning up after itself, see reference: https://www.glfw.org/docs/latest/intro_guide.html#intro_init where under "Initialising GLFW" it
			is stated that "If any part of initialization fails, any parts that succeeded are terminated as if glfwTerminate had been called. The library only needs to be 
			initialized once and additional calls to an already initialized library will return GLFW_TRUE immediately." 
			
			Simon does also say in his example video that "[he] thinks that we're going to have to put some assertions in at some point". Based on the information I read at
			the above referenced source, glfw should terminate itself if the returned code is anything but true(1). Can it be trusted to do so? Have I misinterpreted anything? 
			I have included an assert in the else statement that initReturn == 1 as a redundancy just in case (is this going a bit overboard?).
		*/
		auto initReturn = glfwInit();
		if (initReturn != 1)
		{
#ifdef NG_DEBUG
			Logging::Error("GLFW initialisation failed, returning code: {0}", initReturn);
#elif NG_RELEASE
			Logging::Release("GLFW initialisation failed, returning code: {0}", initReturn);
			Logging::File("GLFW initialisation failed, returning code: {0}", initReturn);
#endif

		}
		else
		{
			// assert that initReturn must equal 1 (true), just in case error not caught
			assert(initReturn == 1);
		}
	}

	void GLFWSystem::Stop(SystemSignal close, ...)
	{
		// Call the GLFW function glfwTerminate to terminate the whole windowing system
		glfwTerminate();
	}
}

