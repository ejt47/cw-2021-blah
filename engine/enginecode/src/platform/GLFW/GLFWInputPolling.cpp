/*!
 * \file GLFWInputPolling.cpp
 * \brief GLFWInputPolling class source file through which GLFW specific code for input polling is defined.
 */

#include "engine_pch.h"
#include "platform/GLFW/GLFWInputPolling.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine
{
	// Initialise the static GLFWwindow pointer with nullptr
	GLFWwindow* GLFWInputPolling::sm_pWindow = nullptr;

	bool GLFWInputPolling::CheckKeyPressed(int32_t keyCode)
	{
		// If sm_pWindow has been pointed to a GLFW window
		if (sm_pWindow)
		{
			/*
				Instantiate the variable response with the auto keyword and initialise by calling glfwGetKey, passing the static member pointer to the glfw window and the
				key identification code keyCode. This returns the last reported state of the key indentified with keyCode.
			*/
			auto getKeyResponse = glfwGetKey(sm_pWindow, keyCode);
			// Return that response indicates a key press, or repeated key press (key held down)
			return getKeyResponse == GLFW_PRESS || getKeyResponse == GLFW_REPEAT;
		}
		// return false (bool function must end by returning a true or false value) 
		return false;
	}

	bool GLFWInputPolling::CheckMouseButtonPressed(int32_t buttonCode)
	{
		// If sm_pWindow has been pointed to a GLFW window
		if (sm_pWindow)
		{
			/*
				Instantiate the variable response with the auto keyword and initialise by calling glfwGetMouseButton, passing the static member pointer to the glfw window and the
				button identification code buttonCode. This returns the last reported state of the mouse button indentified with buttonCode.
			*/
			auto getMouseButtonResponse = glfwGetMouseButton(sm_pWindow, buttonCode);
			return getMouseButtonResponse == GLFW_PRESS;
		}
		// return false (bool function must end by returning a true or false value) 
		return false; 
	}

	glm::vec2 GLFWInputPolling::GetCurrentMousePosition()
	{
		// If sm_pWindow has been pointed to a GLFW window
		if (sm_pWindow)
		{
			// Instantiate doubles to store the x and y values returned by glm
			double x;
			double y;

			/*
				Call the glfw function glfwGetCursorPos, and pass the static member pointer to the glfw window, alongside references to the previously instantiated doubles 
				x and y, giving addresses to store the x and y values returned by glfwGetCursorPos.
			*/
			glfwGetCursorPos(sm_pWindow, &x, &y);
			// Return a glm::vec2 object where the held values are the doubles x and y, static_cast to floats (glm uses floats, glfw uses doubles)
			return glm::vec2(static_cast<float>(x), static_cast<float>(y));
		}

		// return a glm::vec2 where the values are -1.f, -1.f if the above statment could not be carried out (function has to have a redundancy that returns a glm::vec2)
		return { -1.f, -1.f };
	}

}