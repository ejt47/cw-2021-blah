/*! 
 *\file GLFWOpenGLGraphicsContext.cpp
 *\brief TODO WRITE BRIEF
 */

#include "engine_pch.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "platform/GLFW/GLFWOpenGLGraphicsContext.h"
#include "systems/logging.h"

void Engine::GLFWOpenGLGraphicsContext::Init()
{
	/* 
		Call the glfw funtcion glfwMakeContextCurrent to make the OpengGL context of the specified window current on the calling thread
		see: https://www.glfw.org/docs/latest/context_guide.html#context_current & https://www.glfw.org/docs/latest/group__context.html#ga1c04dc242268f827290fe40aa1c91157
	*/
	glfwMakeContextCurrent(m_nativeWindowGLFW);
	// Instantiate an int to hold the result when gladLoadGLLoader is called and passed glfwGetProcAddress(cast to the correct type through reinterpet_cast)
	int result = gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress));
	// Perform a check to see if OpenGL context loaded, outputs error through Logging system if not loaded (result can be 1 or 0, if 0, not loaded)
	if (!result) Logging::Error("Could not initialise an OpenGL context for current glfw window: {0}", result);

	// Enable debugging output from opengl
	glEnable(GL_DEBUG_OUTPUT);

	// Error callback functionality using a lambda generating Logging systen info with OpenGL debug output, for parameter info see: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glDebugMessageCallback.xhtml 
	auto ErrorCallbackLambda = [](GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
	{
		switch (severity)
		{
		case GL_DEBUG_SEVERITY_HIGH :
			Logging::Error(message);
			break;
		case GL_DEBUG_SEVERITY_MEDIUM :
			Logging::Warning(message);
			break;
		case GL_DEBUG_SEVERITY_LOW :
			Logging::Info(message);
			break;
		case GL_DEBUG_SEVERITY_NOTIFICATION : 
			Logging::Trace(message);
			break;
		}
	};

	/* 
		Pass the ErrorCallback lambda to the glDebugMessageCallback function, alonside a nullptr for the the const void* userParam paramter
		see: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glDebugMessageCallback.xhtml
	*/
	glDebugMessageCallback(ErrorCallbackLambda, nullptr);

}

void Engine::GLFWOpenGLGraphicsContext::SwapBuffers()
{
	// Call the glfw function glfwSwapBuffers and pass the pointer to the window instance
	glfwSwapBuffers(m_nativeWindowGLFW);
}
