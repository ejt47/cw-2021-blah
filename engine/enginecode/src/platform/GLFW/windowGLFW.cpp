/*!
 * \file windowGLFW.cpp 
 * \brief windowGLFW class source file, defining the functionality declared in the windowGLFW class header file. 
 */

#include "engine_pch.h"
#include "platform/GLFW/windowGLFW.h"
#include "platform/GLFW/GLFWOpenGLGraphicsContext.h"


 /*!
  * \namespace Engine
  * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
  * every defintion and declaration. Keeps the codebase looking more organised and tidy.
  */

namespace Engine
{
	//DO NOT COMMENT THIS FUNCTION DEFINITION OUT YOU SPENT TWO DAYS WORKING OUT WHY YOU HAD LINKER ERRORS
	//BECAUSE YOU HAD COMMENTED OUT THIS FUNCTION
	//#ifdef NG_PLATFORM_WINDOWS // the inclusion guard here is only necessary if it is decided that other WindowInterface implementations are going to be used
	WindowInterface* WindowInterface::WindowCreate(const WindowProperties& windowProperties)
	{
		// Allocate memory for and return a WindowGLFW object with its properties passed by reference to a WindowProperties struct object
		return new WindowGLFW(windowProperties);
	}
	//#endif

	WindowGLFW::WindowGLFW(const WindowProperties& windowProperties)
	{
		// Call the WindowGLFW function Init, passing the reference to the WindowProperties struct object passed as the argument to the WindowGLFW constructor
		Init(windowProperties);
	}

	void WindowGLFW::Init(const WindowProperties& windowProperties)
	{
		// Initialise the m_windowsProperties member with the passed const reference to a WindowsProperties object windowProperties
		m_windowProperties = windowProperties;

		// Initialise the m_fAspectRatio member by dividing the window width by the window height (both static_cast to float to avoid type coercion)
		m_fAspectRatio = static_cast<float>(m_windowProperties.m_ui32Width) / static_cast<float>(m_windowProperties.m_ui32Width);

		// If m_windowProperties m_bFullScreen member bool is equal to true
		if (m_windowProperties.m_bFullScreen)
		{
			/*
				Initialise the WindowGLFW object member pointer m_nativeWindowGLFW by calling the glfwCreateWindow function and passing it the m_windowProperties struct object
				members m_ui32Width, m_ui32Height, m_sWindowTitle.c_str()(to convert the std::string to a char* as required by the arg list), the GLFW function glfwGetPrimaryMonitor,
				and a nullptr. Passing glfwGetPrimaryMonitor will force the window into full screen mode on the primary monitor(consideration for setups with more than one monitor).
				Reference: https://www.glfw.org/docs/latest/window_guide.html
			*/
			m_nativeWindowGLFW = glfwCreateWindow(m_windowProperties.m_ui32Width, m_windowProperties.m_ui32Height, m_windowProperties.m_sWindowTitle.c_str(), glfwGetPrimaryMonitor(), nullptr);
			// Call the SetVSync function, passing the m_windowProperties member m_bVSync
			SetVSync(m_windowProperties.m_bVSync);
//#ifdef NG_DEBUG
			//Logging::Debug("Window in fullscreen.");
//#endif
		}
		else
		{
			/*
				Initialise the WindowGLFW object member pointer m_nativeWindowGLFW by calling the glfwCreateWindow function and passing it the m_windowProperties struct object
				members m_ui32Width, m_ui32Height, m_sWindowTitle.c_str()(to convert the std::string to a char* as required by the arg list), a nullptr, and another nullptr
			*/
			m_nativeWindowGLFW = glfwCreateWindow(m_windowProperties.m_ui32Width, m_windowProperties.m_ui32Height, m_windowProperties.m_sWindowTitle.c_str(), nullptr, nullptr);
			//SetNativeWindow(glfwCreateWindow(m_windowProperties.m_ui32Width, m_windowProperties.m_ui32Height, m_windowProperties.m_sWindowTitle.c_str(), nullptr, nullptr));
			// Call the SetVSync function, passing the m_windowProperties member m_bVSync
			SetVSync(m_windowProperties.m_bVSync);
//#ifdef NG_DEBUG
			//Logging::Debug("Window in windowed mode.");
//#endif
		}

		/*
			Allocate the memory for a GLFWOpenGLGraphicsContext object, by calling reset on the member shared pointer to a GLFWOpenGLGraphicsContext
			instance
		*/
		m_graphicsContext.reset(new GLFWOpenGLGraphicsContext(m_nativeWindowGLFW));
		// Call the GLFWOpenGLGRaphicsContext function Init, to initialise the graphics context
		m_graphicsContext->Init();

		/* 
			Set the glfw window user pointer to a reference to the event handler member object(accessed through the GetEventHandler function),
			static cast to a void* for usage through glfw
		*/
		glfwSetWindowUserPointer(m_nativeWindowGLFW, static_cast<void*>(&GetEventHandler()));

		// Attempts at unit testing are indicating this should not be tied to the window init function...
		InitEventCallbacks();
	}

	void WindowGLFW::Close()
	{
		// Call the GLFW function glfwDestroyWindow and pass the WindowGLFW member pointer to a GLFW window object m_nativeWindowGLFW
		glfwDestroyWindow(m_nativeWindowGLFW);
	}

	void WindowGLFW::OnUpdate(float fDeltaTime)
	{
		// Call the GLFW function glfwPollEvents to poll for events in the event queue since last update 
		glfwPollEvents();
		// Call through the member shared pointer to the graphics context, the SwapBuffers function to swap to the back buffer
		m_graphicsContext->SwapBuffers();
	}

	void WindowGLFW::SetVSync(bool bVSync)
	{
		// Initialise the m_windowProperties member m_bVSync with the passed bool argument bVSync
		m_windowProperties.m_bVSync = bVSync;

		// If bVSync is true, then swap interval is true(1), turning VSync on to match the monitor refresh rate, Logging system outputs "VSync enabled." in debug mode.
		if (m_windowProperties.m_bVSync)
		{
			glfwSwapInterval(1);
//#ifdef NG_DEBUG
			//Logging::Debug("VSync enabled.");
//#endif
		}
		// Else bVsync must be false, the swap interval is false(0), turning VSync off, this should then set the refresh rate to go as fast as possible
		// (potentially won't, may cause problems), but left as the alternative option, Logging system outputs "VSync disabled." in debug mode.
		else
		{
			glfwSwapInterval(0);
#ifdef NG_DEBUG
			//Logging::Debug("VSync disabled.");
#endif
		}
	}

	void WindowGLFW::InitEventCallbacks()
	{
		/*
			Generally speaking, glfw (and probably other windowing APIs) has functions that correlate to the Events previously defined in the Events class. Previously
			the EventHandling class has also been defined to handle these events, through the wrapping of Event class objects to return a bool variable through std::function
			objects that define in abstract terms an Event callback(see the comments in the EventHandler class header file for a more detailed description). So essentially
			there is already implemented Events that are handled as bool functions via the EventHandler class.

			The GLFWAPI also has callback functions like the EventHandler class, all of these functions, from what I can gather by peeking the GLFWAPI definition, take as
			a first parameter a pointer to a GLFW window, the following argument seems to always be another function, in the case of Window callbacks these seem to
			typically be function pointers. As seems to be the case with input callbacks. see reference: https://www.glfw.org/docs/latest/group__window.html (Window Events) &
			https://www.glfw.org/docs/latest/group__input.html (Input events). Any case where function pointers can be used, anonymous lambda expressions can also be used, this
			means that an anonymous function can be utilised as an argument in the glfw callback functions. Depending on the event different parameters will be used, the Window
			Close callback for example simply requires a GLFWwindow* to the window context, where as the Window Resize callback requires extra numerical data. These arguments
			are essentially used in the logic that the anonymous lambda function encapsulates. If you were to look at the below expression you would see that an EventHandler
			class pointer pEventHandler is instantiated, and assigned to the the results when the returned value from the glfwGetWindowUserPointer, which compares the type
			of the passed GLFWwindow* against the known required type GLFWwindow* (had to peek the GLFWAPI defintion to understand that), and if evaluated to be the correct
			type will then return the previously stored EventHanler pointer that was cast to a void pointer by glfwSetWindowUserPointer in the above WindowGLFW::Init function,
			which is then cast back to an EventHandler pointer. Through this EventHandler pointer the previously defined abstract callback functions can be accessed to handle
			the glfw callbacks, in the context of the Application singleton instance where the Window system is initialised. Further reference: https://en.cppreference.com/w/cpp/language/lambda
			& https://www.youtube.com/watch?v=mWgmBBz0y8c .
		*/

		// BELOW COMMENTED OUT SECTION BASED ON SIMON'S EXAMPLE LEFT FOR POSTERITY, SIMILAR BUT DIFFERENT APPROACH TAKEN FOR THE REST OF THE CODE, TO SHOW I CAN TRY AND THINK FOR 
		// MYSELF, BASED OFF FURTHER READING AND LEARNINGS. TO BE FAIR IT ONLY REALLY DIFFERS SYNTACTICALLY TO MY IMPLEMENTATION, BUT I WANTED THINGS TO BE MORE MAINTANABLE AND
		// READABLE, SO OPTED FOR A SLIGHTLY DIFFERENT SOLUTION.
		//glfwSetWindowCloseCallback(m_nativeWindowGLFW,
		//	[](GLFWwindow* window)
		//{
		//	// Cast the stored user pointer (known to be an EventHandler pointer), back out to an EventHandler pointer from its void pointer state.
		//	EventHandler* pEventHandler = static_cast<EventHandler*>(glfwGetWindowUserPointer(window));
		//	/* 
		//		Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the WindowClosed event callback, and assign its
		//		functional value using the appropriate accessor function GetWindowCloseCallback, named OnWindowClose.
		//	*/
		//	auto& OnWindowClose = pEventHandler->GetOnWindowCloseCallback();

		//	// Instantiate a WindowClosed event object named windowClosed
		//	WindowClosed windowClosed;
		//	// Pass the WindowClosed event object to the OnWindowClose callback function
		//	OnWindowClose(windowClosed);
		//}
		//	);

		/*
			Below I have taken a slightly different approach instead of defining the lambda expression inside the glfw set callback functions, for readability purposes I have
			declared using auto, defined, and then passed the lambda expressions in to the glfw set callback functions by name.
		*/
		auto WindowCloseLambda = [](GLFWwindow* window)
		{
			// Cast the stored user pointer (known to be an EventHandler pointer), back out to an EventHandler pointer from its void pointer state.
			EventHandler* pEventHandler = static_cast<EventHandler*>(glfwGetWindowUserPointer(window));
			/*
				Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the WindowClosed event callback, and assign its
				functional value using the appropriate accessor function GetWindowCloseCallback, named OnWindowClose.
			*/
			auto& OnWindowClose = pEventHandler->GetOnWindowCloseCallback();

			// Instantiate a WindowClosed event object named windowClosed
			WindowClosed windowClosed;
			// Pass the WindowClosed event object to the OnWindowClose callback function
			OnWindowClose(windowClosed);
		};

		// Call glfwSetWindowCloseCallback and pass the WindowGLFW class member pointer to a GLFWwindow m_nativeWindowGLFW, and the WindowCloseLambda as the arguments
		glfwSetWindowCloseCallback(m_nativeWindowGLFW, WindowCloseLambda);

		auto WindowResizeLambda = [](GLFWwindow* window, int resizeWidth, int resizeHeight)
		{
			// Cast the stored user pointer (known to be an EventHandler pointer), back out to an EventHandler pointer from its void pointer state.
			EventHandler* pEventHandler = static_cast<EventHandler*>(glfwGetWindowUserPointer(window));

			/*
				Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the WindowResized event callback, and assign its
				functional value using the appropriate accessor function GetWindowResizeCallback, named OnWindowResize.
			*/
			auto& OnWindowResize = pEventHandler->GetOnWindowResizeCallback();

			// Instantiate a WindowResized event object named windowResized
			WindowResized windowResized(resizeWidth, resizeHeight);
			// Pass the WindowResized event object to the OnWindowResize callback function
			OnWindowResize(windowResized);
		};

		// Call glfwSetWindowSizeCallback and pass the WindowGLFW class member pointer to a GLFWwindow m_nativeWindowGLFW, and the WindowResizeLambda as the arguments
		glfwSetWindowSizeCallback(m_nativeWindowGLFW, WindowResizeLambda);

		auto WindowFocusLambda = [](GLFWwindow* window, int bFocus)
		{
			// Cast the stored user pointer (known to be an EventHandler pointer), back out to an EventHandler pointer from its void pointer state.
			EventHandler* pEventHandler = static_cast<EventHandler*>(glfwGetWindowUserPointer(window));

			if (bFocus)
			{
				/*
					Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the WindowFocus event callback, and assign its
					functional value using the appropriate accessor function GetWindowFocusCallback, named OnWindowFocus.
				*/
				auto& OnWindowFocus = pEventHandler->GetOnWindowFocusCallback();

				// Instantiate a WindowFocus event object named windowFocus
				WindowFocus windowFocus;
				// Pass the WindowFocus event object to the OnWindowFocus callback function
				OnWindowFocus(windowFocus);
			}
			else
			{
				/*
					Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the WindowLostFocus event callback, and assign its
					functional value using the appropriate accessor function GetWindowLostFocusCallback, named OnWindowLostFocus.
				*/
				auto& OnWindowLostFocus = pEventHandler->GetOnWindowLostFocusCallback();

				// Instantiate a WindowLostFocus event object named windowLostFocus
				WindowLostFocus windowLostFocus;
				// Pass the WindowLostFocus event object to the OnWindowLostFocus callback function
				OnWindowLostFocus(windowLostFocus);
			}
		};

		// Call glfwSetWindowFocusCallback and pass the WindowGLFW class member pointer to a GLFWwindow m_nativeWindowGLFW, and the WindowFocusLambda as the arguments
		glfwSetWindowFocusCallback(m_nativeWindowGLFW, WindowFocusLambda);

		auto WindowMoveLambda = [](GLFWwindow* window, int movePosX, int movePosY)
		{
			// Cast the stored user pointer (known to be an EventHandler pointer), back out to an EventHandler pointer from its void pointer state.
			EventHandler* pEventHandler = static_cast<EventHandler*>(glfwGetWindowUserPointer(window));

			/*
				Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the WindowMoved event callback, and assign its
				functional value using the appropriate accessor function GetWindowMoveCallback, named OnWindowMove.
			*/
			auto& OnWindowMove = pEventHandler->GetOnWindowMoveCallback();

			// Instantiate a WindowMoved event object named windowMoved
			WindowMoved windowMoved(movePosX, movePosY);
			// Pass the WindowMoved event object to the OnWindowMove callback function
			OnWindowMove(windowMoved);
		};

		// Call glfwSetPosCallback and pass the WindowGLFW class member pointer to a GLFWwindow m_nativeWindowGLFW, and the WindowMoveLambda as the arguments
		glfwSetWindowPosCallback(m_nativeWindowGLFW, WindowMoveLambda);

		auto KeyEventsLambda = [](GLFWwindow* window, int keyCode, int scanCode, int actionCode, int modCode)
		{
			// Cast the stored user pointer (known to be an EventHandler pointer), back out to an EventHandler pointer from its void pointer state.
			EventHandler* pEventHandler = static_cast<EventHandler*>(glfwGetWindowUserPointer(window));
			
			if (actionCode == GLFW_PRESS)
			{
				/*
					Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the KeyPressed event callback, and assign its
					functional value using the appropriate accessor function GetKeyPressCallback, named OnKeyPress.
				*/
				auto& OnKeyPress = pEventHandler->GetOnKeyPressCallback();

				/*
					Instantiate a KeyPressed event object named keyPressed, initialised with keyCode(or scanCode for platform specific characters represented by keyCode integers),
					with a repeat count 0(false) indicating key is not being held down
				*/
				KeyPressed keyPressed(keyCode, 0);
				// Pass the KeyPressed event object to the OnKeyPress callback function
				OnKeyPress(keyPressed);

				/*
					Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the KeyTyped event callback, and assign its
					functional value using the appropriate accessor function GetKeyTypedCallback, named OnKeyTyped.
				*/
				auto& OnKeyTyped = pEventHandler->GetOnKeyTypedCallback();

				// Instantiate a KeyTyped event object named KeyTyped, initialised with keyCode(or scanCode for platform specific characters represented by keyCode integers)
				KeyTyped keyTyped(keyCode);
				// Pass the KeyTyped event object to the KeyTyped callback function
				OnKeyTyped(keyTyped);
			}

			else if (actionCode == GLFW_REPEAT)
			{
				/*
					Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the KeyPressed event callback, and assign its
					functional value using the appropriate accessor function GetKeyPressCallback, named OnKeyPress.
				*/
				auto& OnKeyPress = pEventHandler->GetOnKeyPressCallback();

				/*
					Instantiate a KeyPressed event object named keyPressed, initialised with keyCode(or scanCode for platform specific characters represented by keyCode integers),
					with a repeat count 1(true) indicating key is being held down 
				*/
				KeyPressed keyPressed(keyCode, 1);
				// Pass the KeyPressed event object to the OnKeyPress callback function
				OnKeyPress(keyPressed);
			}

			else if (actionCode == GLFW_RELEASE)
			{
				/*
					Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the KeyReleased event callback, and assign its
					functional value using the appropriate accessor function GetKeyReleaseCallback, named OnKeyRelease.
				*/
				auto& OnKeyRelease = pEventHandler->GetOnKeyReleaseCallback();

				// Instantiate a KeyReleased event object named keyReleased, initialised with keyCode(or scanCode for platform specific characters represented by keyCode integers)
				KeyReleased keyReleased(keyCode);
				// Pass the KeyReleased event object to the KeyRelease callback function
				OnKeyRelease(keyReleased);
			}
		};

		// Call the glfwSetKeyCallback function and pass the WindowGLFW class member pointer to a GLFWwindow m_nativeWindowGLFW, and the KeyEventsLambda as the arguments
		glfwSetKeyCallback(m_nativeWindowGLFW, KeyEventsLambda);

		auto MouseButtonEventsLambda = [](GLFWwindow* window, int buttonCode, int actionCode, int modCode)
		{
			// Cast the stored user pointer (known to be an EventHandler pointer), back out to an EventHandler pointer from its void pointer state.
			EventHandler* pEventHandler = static_cast<EventHandler*>(glfwGetWindowUserPointer(window));

			if (actionCode == GLFW_PRESS)
			{
				/*
					Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the MouseButtonPressed event callback, and assign its
					functional value using the appropriate accessor function GetMouseButtonPressCallback, named OnMouseButtonPress.
				*/
				auto& OnMouseButtonPress = pEventHandler->GetOnMouseButtonPressCallback();

				// Instantiate a MouseButtonPressed event object named mouseButtonPress
				MouseButtonPressed mouseButtonPress(buttonCode);
				// Pass the MouseButtonPressed event object to the mouseButtonPress callback function
				OnMouseButtonPress(mouseButtonPress);
			}

			else if (actionCode == GLFW_RELEASE)
			{
				/*
					Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the MouseButtonReleased event callback, and assign its
					functional value using the appropriate accessor function GetMouseButtonReleaseCallback, named OnMouseButtonRelease.
				*/
				auto& OnMouseButtonRelease = pEventHandler->GetOnMouseButtonReleaseCallback();

				// Instantiate a MouseButtonReleased event object named mouseButtonRelease
				MouseButtonReleased mouseButtonRelease(buttonCode);
				// Pass the MouseButtonReleased event object to the mouseButtonRelease callback function
				OnMouseButtonRelease(mouseButtonRelease);
			}
		};

		// Call the glfwSetMouseButtonCallback function and pass the WindowGLFW class member pointer to a GLFWwindow m_nativeWindowGLFW, and the MouseButtonEventsLambda as the arguments
		glfwSetMouseButtonCallback(m_nativeWindowGLFW, MouseButtonEventsLambda);

		auto MouseMoveEventLambda = [](GLFWwindow* window, double xPos, double yPos)
		{
			// Cast the stored user pointer (known to be an EventHandler pointer), back out to an EventHandler pointer from its void pointer state.
			EventHandler* pEventHandler = static_cast<EventHandler*>(glfwGetWindowUserPointer(window));

			/*
				Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the MouseMoved event callback, and assign its
				functional value using the appropriate accessor function GetOnMouseMoveCallback, named OnMouseMove.
			*/
			auto& OnMouseMove = pEventHandler->GetOnMouseMoveCallback();

			// Instantiate a MouseMoved event object named mouseMoved
			MouseMoved mouseMoved(xPos, yPos);
			// Pass the MouseMoved event object to the mouseMoved callback function
			OnMouseMove(mouseMoved);
		};

		// Call the glfwSetCursorPosCallback function and pass the WindowGLFW class member pointer to a GLFWwindow m_nativeWindowGLFW, and the MouseMoveEventLambda as the arguments
		glfwSetCursorPosCallback(m_nativeWindowGLFW, MouseMoveEventLambda);

		auto MouseScrollEventLambda = [](GLFWwindow* window, double xOffset, double yOffset)
		{
			// Cast the stored user pointer (known to be an EventHandler pointer), back out to an EventHandler pointer from its void pointer state.
			EventHandler* pEventHandler = static_cast<EventHandler*>(glfwGetWindowUserPointer(window));

			/*
				Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the MouseScrolled event callback, and assign its
				functional value using the appropriate accessor function GetOnMouseScrollCallback, named OnMouseScroll.
			*/
			auto& OnMouseScroll = pEventHandler->GetOnMouseScrollCallback();

			// Instantiate a MouseScrolled event object named mouseScrolled
			MouseScrolled mouseScrolled(xOffset, yOffset);
			// Pass the MouseScrolled event object to the mouseScrolled callback function
			OnMouseScroll(mouseScrolled);
		};

		// Call the glfwSetCursorPosCallback function and pass the WindowGLFW class member pointer to a GLFWwindow m_nativeWindowGLFW, and the MouseMoveEventLambda as the arguments
		glfwSetScrollCallback(m_nativeWindowGLFW, MouseScrollEventLambda);
	}
}