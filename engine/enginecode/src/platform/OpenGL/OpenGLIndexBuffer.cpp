/*!
 * \file OpenGLIndexBuffer.cpp
 * \brief TODO write brief
 */

#include "engine_pch.h"

#include <glad/glad.h>

#include "platform/OpenGL/OpenGLIndexBuffer.h"

Engine::OpenGLIndexBuffer::OpenGLIndexBuffer(uint32_t* indices, uint32_t count) : m_ui32Count(count)
{
	// Carry out the same steps for the Index Buffer Object, where GL_ELEMENT_ARRAY_BUFFER is used instead, indicating the buffer as a source of the
	// data for indexed rendering
	// Create 1 buffer, using the indentifier uint32_t member m_ui32OpenGL_ID
	glCreateBuffers(1, &m_ui32OpenGL_ID);
	// Bind the buffer with target type GL_ELEMENT_ARRAY_BUFFER, identifying with m_ui32OpenGL_ID
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ui32OpenGL_ID);
	/*
		Call the OpenGL function glBufferData, passing the type of buffer(GL_ELEMENT_ARRAY_BUFFER), the size of the index array which is 
		calculated by multiplying the returned value of sizeof(uint32_t) with the passed uint32_t count value, a pointer to the array of uint32_t 
		variables that holds the index data(indices)and a GLenum to designate usage of GL_STATIC_DRAW or GL_DYNAMIC_DRAW, indicating the usage of the
		data store contents when it is used to draw, in this case GL_DYNAMIC_DRAW, meaning data store contents will be modified once and used 
		many times as the source for GL drawing commands.
	*/
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * count, indices, GL_STATIC_DRAW);
}

Engine::OpenGLIndexBuffer::~OpenGLIndexBuffer()
{
	// Call the OpenGL function glDeleteBuffers, passing the number of buffers to delete, and the uint32_t used as the buffer identifier
	glDeleteBuffers(1, &m_ui32OpenGL_ID);
}
