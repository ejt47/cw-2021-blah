/*!
 *\file OpenGLTexture.cpp
 */

#include "engine_pch.h"

#include <glad/glad.h>

#include "platform/OpenGL/OpenGLTexture.h"


#define STB_IMAGE_IMPLEMENTATION //THIS NEEDS TO BE PUT ONLY ONCE, CURRENTLY IN APPLICATION SRC FILE, COMMENT HERE AS REMINDER OF THIS 
#include "stb_image.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	OpenGLTexture::OpenGLTexture(const char * filePath)
	{
		// Instantiate fixed width 32 bit integer variables to hold the width, height and RGBA channel count of the loaded texture image
		int width;
		int height;
		int channels;

		/*
			Unsigned char pointer data to store the information loaded by stbi_load, which is passed the file path of the texture as a const char*
			(the passed parameter for this constructor), a reference tothe width, height, and channels int32_ts, instantiated above, which holds the
			width, height and RGBA channel count of the texture image at the given path
		*/
		unsigned char* data = stbi_load(filePath, &width, &height, &channels, 0);

		Init(width, height, channels, data);

		// free the allocated memory for the unsigned char pointer to the image data
		stbi_image_free(data);

		//stbi_failure_reason();
	}

	OpenGLTexture::OpenGLTexture(uint32_t width, uint32_t height, uint32_t channels, unsigned char* data)
	{
		//if there is texture image data, call the Init function
		Init(width, height, channels, data);
		// free the allocated memory for the unsigned char pointer to the image data
		//stbi_image_free(data); // calling this throws exception, whilst file path version of this function does not THROWS ERROR BCUS TEXTURE NOT LOADED VIA STBI !!!
		//stbi_failure_reason();
	}

	OpenGLTexture::~OpenGLTexture()
	{
		glDeleteTextures(1, &m_ui32OpenGL_ID);
	}

	void OpenGLTexture::Edit(uint32_t xOffset, uint32_t yOffset, uint32_t width, uint32_t height,  unsigned char* data)
	{
		/*
			Call the OpenGL function glBindTexture and pass the type of texture being bound to the buffer (GL_TEXTURE_2D), and the uint32_t
			identification member variable m_ui32OpenGL_ID
		*/
		glBindTexture(GL_TEXTURE_2D, m_ui32OpenGL_ID);

		if (data)
		{
			if (m_ui32Channels == 1/* && width % 4 == 0 && height % 4 == 0*/)
			{
				/*
					Call glTexImage2D, pass target texture type (GL_TEXTURE_2D), level-of-detail(0(Level nth is the nth mipmap reduction image)),
					internal format GL_RED, width(loaded into data, by stbi_load), height(loaded into data, by stbi_load), width of border(0(has to be)),
					format(GL_RED(has to match internal format param)), data type(GL_UNSIGNED_BYTE), the texture image data pointer(data).
				*/
				glTextureSubImage2D(m_ui32OpenGL_ID, 0, xOffset, yOffset, width, height, GL_RED, GL_UNSIGNED_BYTE, data);
			}
			else if (m_ui32Channels == 3/* && width % 4 == 0 && height % 4 == 0*/)
			{
				// see above for 1 channel, except match for GL_RGB 3 component format
				glTextureSubImage2D(m_ui32OpenGL_ID, 0, xOffset, yOffset, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);
			}
			// Else if 4 channels assumed RGBA, see above, but swap GL_RGB for GL_RGBA
			else if (m_ui32Channels == 4/* && width % 4 == 0 && height % 4 == 0*/)
			{
				// see above for 1 channel, except match for GL_RGBA 4 component format
				glTextureSubImage2D(m_ui32OpenGL_ID, 0, xOffset, yOffset, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);
			}
			else return;
		}
	}
	void OpenGLTexture::Init(uint32_t width, uint32_t height, uint32_t channels, unsigned char* data)
	{
		/*
			Call the OpenGL function glGenTextures to generate a texture buffer, passing the texture count(1), and a reference to the uint32_t
			identification member variable m_ui32OpenGL_ID
		*/
		glGenTextures(1, &m_ui32OpenGL_ID);
		/*
			Call the OpenGL function glBindTexture and pass the type of texture being bound to the buffer (GL_TEXTURE_2D), and the uint32_t
			identification member variable m_ui32OpenGL_ID
		*/
		glBindTexture(GL_TEXTURE_2D, m_ui32OpenGL_ID);

		/*
			Call the OpenGL function glTexParameteri(1 dimension?), pass the type macro(typedef?) GL_TEXTURE_2D identifying the texture type(dimensionally),
			the macro GL_TEXTURE_WRAP_S to define the texture as wrapping horizontally(S = X axis), the macro GL_CLAMP_TO_EDGE forces OpenGL to only
			sample pixels from the actual texture
		*/
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		// Same as above but wrapping vertically(T = Y-Axis)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		/*
			Define how to sample the texture if the object that is having the texture applied is much smaller(GL_TEXTURE_MIN_FILTER) or much larger
			(GL_TEXTURE_MAG_FILTER), can be thought of as 'minifying' and 'magnifying', texture is sampled linearly(GL_LINEAR(returning the avg of the
			four texture elements that are closest to the center of the pixel being textured))
			see: https://www.khronos.org/registry/OpenGL-Refpages/es2.0/xhtml/glTexParameter.xml
		*/
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		/*
			If there are 3 channels(example code given assumes these 3 channels are RGB), call the OpenGL function glTexImage2D and pass the
			GL_TEXTURE_2D macro as the target param, 0 for the level of detail param, GL_RGB macro as the internalformat param(colour component),
			the width and height of the image, border value(must be 0 according to docs), the GL_RGB macro again as the format of the pixel data,
			GL_UNSIGNED_BYTE macro for the data type of the pixel data, and the unsigned char pointer data to the image data in memory

			see: https://www.khronos.org/opengl/wiki/Common_Mistakes#Image_precision &
			https://www.opengl.org/archives/resources/code/samples/sig99/advanced99/notes/node51.htmlfor best precision 'image format', should be kept
			in mind that RGB8 gives a bit (or colour) depth of 8 to each component 'R' 'G' 'B', where a bit has 2 possible states, and 2^8 possible
			combinations per 8 bit component(2*2*2*2*2*2*2*2) which = 256 distinct colour values per component, 1st link gives the exact example Simon
			gave us, states that "GL will accept GL_RGB, it is up to the driver to decide an appropriate precision. We recommend that you be specific
			and write GL_RGB8", they also state "most GPUs will internally convert GL_RGB8 into GL_RGBA8". Given this information is there any actual
			point in specifying RGB8? If the GPU does not convert it to the 4 component RGBA, it is then also important to use glPixelStore to set
			GL_UNPACK_ALIGNMENT (see below link), to pad the pixel array row allignment to an even number. Contrary to the example code on the lecture
			slide. This is especially important if the loaded texture image is not a multiple of the default alignment used in most cases of 4. see:
			https://www.khronos.org/opengl/wiki/Pixel_Transfer#Pixel_layout for example error case.

			see: https://www.khronos.org/opengl/wiki/Common_Mistakes#Texture_upload_and_pixel_reads for best practice for use of
			GL_UNPACK_ALIGNMENT with GL_RGB & https://gamedev.net/forums/topic/451408-alignment-parameter-of-function-glpixelstore/3984146/
			for a more simplified explanation that the 1, 2, 4, or 8 values that can be passed to glPixelStore are 'padding' values, so
			RGB with 3 components, should be padded by 1 to get a 4 component pixel array row for precision.
			https://forums.developer.nvidia.com/t/how-does-gl-unpack-alignment-work/39432/3GL_RED recommends that GL_RED, which is 1 component,
			requrires an alignment of 2. Why? Is this making the assumption that a fourth alpha channel is being included? In which case what happens
			to GL_RGB if the alignment is set to 1 and an alpha channel is added by the GPU? Does GL_UNPACK_ALIGNMENT account for this and pad with
			an alpha value??? I think I will go with the recommendations I've found and see what happens, I've seen a lot of contradictory information
			and I'm now not even sure what it the correct syntax here, does glPixelStore come before or after glTexImage2D?

			DECISION MADE, NOT GOING TO WASTE TIME RIGHT NOW FIGURING THIS OUT, GO WITH WHAT YOU KNOW WORKS, AND TAKE CARE TO USE APPROPRIATELY
			SIZED TEXTURE IMAGES (DIMENSIONS DIVISABLE BY 4 !!!) POSSIBLE TO ASSERT (HEIGHT % 4 == 0 && WIDTH % 4 == 0) I think that should work !

			Leaving all this info for posterity, might be useful if I come back to this topic.
		*/
		if (channels == 1/* && width % 4 == 0 && height % 4 == 0*/)
		{
			/*
				Call glTexImage2D, pass target texture type (GL_TEXTURE_2D), level-of-detail(0(Level nth is the nth mipmap reduction image)),
				internal format GL_RED, width(loaded into data, by stbi_load), height(loaded into data, by stbi_load), width of border(0(has to be)),
				format(GL_RED(has to match internal format param)), data type(GL_UNSIGNED_BYTE), the texture image data pointer(data).
			*/
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, data);
			Logging::Info("1 channel, R only.");
		}
		else if (channels == 3/*&& width % 4 == 0 && height % 4 == 0*/)
		{
			// see above for 1 channel, except match for GL_RGB 3 component format
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			Logging::Info("3 channels, RGB only.");
		}
		// Else if 4 channels assumed RGBA, see above, but swap GL_RGB for GL_RGBA
		else if (channels == 4/* && width % 4 == 0 && height % 4 == 0*/)
		{
			// see above for 1 channel, except match for GL_RGBA 4 component format
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			Logging::Info("4 channel, RGBA.");
		}
		/*
			Call the OpenGL function glGenerateMipmap to generate a mipmap(a pre-calculated, optimised sequence of images
			see: https://en.wikipedia.org/wiki/Mipmap) from the GL_TEXTURE_2D target texture buffer that the texture was previously loaded into
			*/
		glGenerateMipmap(GL_TEXTURE_2D);
		/*
			Assign the member variables m_ui32Width, m_ui32Height, m_ui32Channels to  the passed width, height and RGBA channel count of the
			loaded texture image
		*/
		m_ui32Width = width;
		m_ui32Height = height;
		m_ui32Channels = channels;
	}
}