/*! 
 * \file OpenGLVertexArray.h 
 * \brief TODO WRITE BRIEF
 */

#include "engine_pch.h"

#include <glad/glad.h>

#include "platform/OpenGL/OpenGLVertexArray.h"
#ifdef NG_DEBUG
#include "systems/logging.h"
#endif

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	namespace SDT 
	{
		static GLenum toGLType(ShaderDataType dataType)
		{
			//if (ui32GLenum[static_cast<int>(dataType)] < 1 | ui32GLenum[static_cast<int>(dataType)] > 4) return GL_INVALID_ENUM;
			//if (SDT::m_ui32Count[static_cast<int>(dataType)] < 1 | SDT::m_ui32Count[static_cast<int>(dataType)] > 4) return GL_INVALID_ENUM;
			//else return GL_FLOAT;
			// Switch to classify float types as GL_FLOATs
			switch (dataType)
			{
				// ShaderDataType::Integer returns a GL_INT
			case ShaderDataType::Integer: return GL_INT;
				// ShaderDataType::Integer2 returns a GL_INT
			case ShaderDataType::Integer2: return GL_INT;
				// ShaderDataType::Integer3 returns a GL_INT
			case ShaderDataType::Integer3: return GL_INT;
				// ShaderDataType::Integer4 returns a GL_INT
			case ShaderDataType::Integer4: return GL_INT;
				// ShaderDataType::Float returns a GL_FLOAT
			case ShaderDataType::Float: return GL_FLOAT;
				// ShaderDataType::Float2 returns a GL_FLOAT
			case ShaderDataType::Float2: return GL_FLOAT;
				// ShaderDataType::Float3 returns a GL_FLOAT
			case ShaderDataType::Float3: return GL_FLOAT;
				// ShaderDataType::Float4 returns a GL_FLOAT
			case ShaderDataType::Float4: return GL_FLOAT;
				// ShaderDataType::Mat3 returns a 
			case ShaderDataType::Mat3: return 9;
				// where there is one 4 by 4 Matrix
			case ShaderDataType::Mat4: return 16;
				// where there is one Boolean 
			case ShaderDataType::Boolean: return 1;
				// where there is one 2D Sampler
			case ShaderDataType::Sampler2D: return 1;
			default: return GL_INVALID_ENUM;
			}
		}
	}

	OpenGLVertexArray::OpenGLVertexArray()
	{
		// Call the OpenGL function glCreateVertexArray, passing the qunatity of Vertex Arrays to be created, and a reference to the member
		// fixed width 32 bit unsigned integer m_ui32OpenGL_ID for identification of the Vertex Array object
		glCreateVertexArrays(1, &m_ui32OpenGL_ID);
		// Call the OpenGL function glBindVertexArray to bind the vertex array object
		glBindVertexArray(m_ui32OpenGL_ID);
	}

	OpenGLVertexArray::~OpenGLVertexArray()
	{
		// Call glDeleteVertexArrays OpenGL function passing the quantity to delete(1), and a reference to the identifier of the vertex array to delete
		glDeleteVertexArrays(1, &m_ui32OpenGL_ID);
	}

	void OpenGLVertexArray::AddVertexBuffer(const std::shared_ptr</*OpenGL*/VertexBuffer>& vertexBuffer)
	{
		// notes have down that this push_back function call must be here to prevent bugged rendering in 2d
		m_vpVertexBuffer.push_back(vertexBuffer);

		// Call OpenGL function glBindVertexArray to add the attributes to the current vertex buffer object in the GPU
		glBindVertexArray(m_ui32OpenGL_ID);
		/* 
			Call OpenGL function glBindBuffer to bind the target array buffer to the identifier of the VBO by accessing via the pointer to the VBO
			the GetRenderingID function returning the VBO identifier 
		*/
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer->GetRenderingID());


		// Instantiate and initialise a const reference to the BufferLayout object member stored by the OpenGLVertexBuffer
		const auto& layout = vertexBuffer->GetLayout();
		//Start a range based for loop to iterate through each BufferElement that makes up the BufferLayout, and set the attributes.
		for (const auto& element : layout)
		{
			// Initialise a fixed width 32 bit unsigned integer normalised to GL_FALSE(0) IS THIS NECESSARY??
			//uint32_t normalised = GL_FALSE;
			// Initialise normalised to GL_FALSE if element.m_BNormalised == GL_TRUE
			//if (element.m_bNormalised) { normalised == GL_TRUE; };
			// Call the OpenGL function glEnableVertexAttribArray, passing the current value of the member m_ui32AttributeIndex(the iteration count basically)
			glEnableVertexAttribArray(m_ui32AttributeIndex);
			/* 
				Call the OpenGL function glVertexAttribPointer, passing the: m_ui32AttributeIndex value, the component count of the current element's
				m_dataType member, the data type of the current element's m_dataType member, the current element's m_bNormalised bool value, the current
				size of the BufferLayout object's stride, the position of the current element in the current stried through a reinterpret_cast to a 
				void ptr operated on the current element's m_ui32Offset member giving the stride offset(position)
			*/
			glVertexAttribPointer(
				m_ui32AttributeIndex, 
				SDT::ComponentCount(element.m_dataType), 
				SDT::toGLType(element.m_dataType), 
				element.m_bNormalised,
				layout.GetStride(), 
				reinterpret_cast<void*>(element.m_ui32Offset)); //position
			//Logging system debug string to ensure everything is as expected, inclusion guards to keep restricted to debug build
#ifdef NG_DEBUG
			Logging::Info(
				"Attrib index count: {0}, Component Count: {1}, GL Data Type: {2}, Normalised: {3}, Stride: {4}, Offset Mem Adr: {5}, Offset value: {6}", 
				m_ui32AttributeIndex, 
				SDT::ComponentCount(element.m_dataType), 
				SDT::toGLType(element.m_dataType), 
				element.m_bNormalised, layout.GetStride(), 
				reinterpret_cast<void*>(element.m_ui32Offset), 
				element.m_ui32Offset);
#endif 
			m_ui32AttributeIndex++;
		}
	}

	//may need to revert shared ptr ref to IndexBuffer back 2 OpenGLIndexBuffer
	void OpenGLVertexArray::SetIndexBuffer(const std::shared_ptr<IndexBuffer>& indexBuffer)
	{
		// Initialise the m_pIndexBuffer pointer to point to the passed const reference to a shared pointer to a OpenGLIndexBuffer object
		m_pIndexBuffer = indexBuffer;
	}
}