/*!
 * \file OpenGLVertexBuffer.cpp
 * \brief TODO write brief
 */
#include "engine_pch.h"

#include <glad/glad.h>

#include "platform/OpenGL/OpenGLVertexBuffer.h"


/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine
{
	// initialise m_bufferLayout member BufferLayout object with the passed BufferLayout object argument layout
	OpenGLVertexBuffer::OpenGLVertexBuffer(void* vertices, uint32_t size, BufferLayout layout) : m_bufferLayout(layout)
	{
		// Call the OpenGL function glCreateBuffers, passing the value 1(how many buffer array objects to create), and a reference to the unsigned int
		// member variable m_ui32OpenGL_ID, which is used as the handle of the vertex buffer
		glCreateBuffers(1, &m_ui32OpenGL_ID);
		/*
			Call the OpenGL function glBindBuffer, passing the macro GL_ARRAY_BUFFER (indicates the buffer is a source for vertex data), and the handle
			of the created buffer using the uint32_T member variable m_ui32OpenGL_ID, binding the created and named vertex buffer to the type of buffer 
			GL_ARRAY_BUFFER target
		*/
		glBindBuffer(GL_ARRAY_BUFFER, m_ui32OpenGL_ID);
		/*
			Call the OpenGL function glBufferData, passing the type of buffer(GL_ARRAY_BUFFER), the size of the vertex array(size), a pointer to the
			array that holds the vertex data(vertices), and a GLenum to designate usage of GL_STATIC_DRAW or GL_DYNAMIC_DRAW, indicating the usage of the
			data store contents when it is used to draw, in this case GL_DYNAMIC_DRAW, meaning data store contents will be modified repeatedly and used 
			many times as the source for GL drawing commands.
		*/
		glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_DYNAMIC_DRAW);
	}

	OpenGLVertexBuffer::~OpenGLVertexBuffer()
	{
		// Call the OpenGL function glDeleteBuffers, passing the number of buffers to delete(1), and a reference to the uint32_t member identifier (m_OpengGL_ID)
		glDeleteBuffers(1, &m_ui32OpenGL_ID);
	}

	void OpenGLVertexBuffer::Edit(void* vertices, uint32_t size, uint32_t offset) const 
	{
		/*
			Call the OpenGL function glBindBuffer, passing the macro GL_ARRAY_BUFFER (indicates the buffer is a source for vertex data), and the handle
			of the created buffer using the uint32_T member variable m_ui32OpenGL_ID, re-binding the created and named vertex buffer to the type of buffer
			GL_ARRAY_BUFFER target
		*/
		glBindBuffer(GL_ARRAY_BUFFER, m_ui32OpenGL_ID);
		/*
			Call the OpenGL function glBufferSubData, passing the target buffer macro type GL_ARRAY_BUFFER, the uint32_t offset value, the uint32_t size
			value, and the pointer to the array holding the vertex data vertices
		*/
		glBufferSubData(GL_ARRAY_BUFFER, offset, size, vertices);
	}
}