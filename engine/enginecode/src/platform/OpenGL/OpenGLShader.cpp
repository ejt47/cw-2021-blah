/*!
 * \file OpenGLShader.cpp
 * \brief TODO WRITE BRIEF 
 */

#include "engine_pch.h"

#include<glad/glad.h>

#include<glm/gtc/type_ptr.hpp>

#include <fstream>
#include <array>

#include "platform/OpenGL/OpenGLShader.h"
#include "systems/logging.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine 
{
	OpenGLShader::OpenGLShader(const char* vertexFP, const char* fragmentFP)
	{
		// Instantiate string variables to hold the characters on a line read in from the files passed to the function, and for each separate shader
		std::string sLine;
		// vertex shader source code string
		std::string sVertexSource;
		// fragment shader source code string
		std::string sFragmentSource;

		/*
			Filestream object to read the file into the instantiated sLine string, to be then stored in the sVertexSource string, passed the filePath
			string passed to the constructor to locate the file, as well as std::ios::in to set the filestream to read data in mode
		*/
		std::fstream vertexID(vertexFP, std::ios::in);

		// Check file is open
		if (vertexID.is_open())
		{
			// While getline still detects character strings to read in after a newline
			while (getline(vertexID, sLine))
			{
				// Add sLine to the sVertexSource string, concatenating the two strings (where the current contents of sLine + \n newline are appended)
				sVertexSource += (sLine + "\n");
			}
		}
		else
		{
			// Otherwise output through the logging system an error message to indicate the file could not be opened
			Logging::Error("Vertex shader source file could not be opened: {0}", vertexFP);
			return;
		}

		// close the vertex shader file
		vertexID.close();

		// see aboe, same steps taken for fragment shader file
		std::fstream fragmentID(fragmentFP, std::ios::in);

		if (fragmentID.is_open())
		{
			while (getline(fragmentID, sLine))
			{
				sFragmentSource += (sLine + "\n");
			}
		}
		else
		{
			Logging::Error("Fragment shader source file could not be opened: {0}", fragmentFP);
			return;
		}

		fragmentID.close();

		// Compile and link the vertex and fragment shader source code, passing to the function as cstrings for usage with OpenGL functions
		CompileAndLink(sVertexSource.c_str(), sFragmentSource.c_str());
	}

	OpenGLShader::OpenGLShader(const char* filePath)
	{
		// Local enum to assign bitwise category flags some various possible shader types, currently the class only supports vertex and fragment shaders
		enum Regions 
		{ 
			None = -1, 
			Vertex = 0, 
			Fragment, 
			Geometry, 
			TessellationControl, 
			TessellationEvaluation, 
			Compute 
		};

		// Instantiate a string to store the characters from the current line being read in from the file located at the file path passed to the constructor
		std::string sLine;

		/*
			Instantiate a std library array object, passing std::string as the data type and the size passed as the last of the enum category flags + 1
			used to store and separate the shader source code (separates by index based on the delimiter #region "shadertype"
		*/
		std::array<std::string, Regions::Compute + 1> shaderSource;

		//see above comments on file reading 
		std::fstream filePathID(filePath, std::ios::in);

		// instantiate a unisgned 32 bit fixed width integer to hold the index of the current shader source file region
		uint32_t ui32Region = Regions::None;

		if (filePathID.is_open())
		{
			while (getline(filePathID, sLine))
			{
				// If the current line contains the character string "#region Vertex", and the end of the source file string has not been reached
				if (sLine.find("#region Vertex") != std::string::npos)
				{
					// Set the array index variable to the enum category flag representing the vertex shader source code region 
					ui32Region = Regions::Vertex;
					// continue onto next line (don't want to read in #region Vertex to the string as it has no use apart from delimitation)
					continue;
				}
				//see above
				else if (sLine.find("#region Fragment") != std::string::npos)
				{
					ui32Region = Regions::Fragment;
					continue;
				}
				//see above
				else if (sLine.find("#region Geometry") != std::string::npos)
				{
					ui32Region = Regions::Geometry;
					continue;
				}
				//see above
				else if (sLine.find("#region TessellationControl") != std::string::npos)
				{
					ui32Region = Regions::TessellationControl;
					continue;
				}
				//see above
				else if (sLine.find("#region TessellationEvaluation") != std::string::npos)
				{
					ui32Region = Regions::TessellationEvaluation;
					continue;
				}
				//see above
				else if (sLine.find("#region Compute") != std::string::npos)
				{
					ui32Region = Regions::Compute;
					continue;
				}

				/* 
					If the variable holding the current region index does not == no regions (according to enum values), concatenate the shader source
					string held in that region with the current line string sLine + \n (newline char)
				*/
				if (ui32Region != Regions::None) shaderSource[ui32Region] += (sLine + "\n");
			}
		}
		else
		{
			//Otherwise output error message 
			Logging::Error("Shader source file could not be opened: {0}", filePath);
			return;
		}

		// CLose the file
		filePathID.close();

		// compile and link the vertex and fragment shader source code stored in the array, as c strings for use with OpenGL funcs
		CompileAndLink(shaderSource[Regions::Vertex].c_str(), shaderSource[Regions::Fragment].c_str());
	}

	OpenGLShader::~OpenGLShader()
	{
		// Call OpenGL function glDeleteShaders, passing the member identifier uint32_t to identify the shader to be deleted
		glDeleteShader(m_ui32OpenGL_ID);
	}

	void OpenGLShader::uploadInt(const char* name, int value)
	{
		/* 
			Instantiate and initialise a uint32_t with the OpenGL func glGetUniformLocation, in order to obtain the location in the shader 
			program to upload the integer value passed to the containing function to, pass the passed const char* (string), to search the
			shader program for the matching uniform variable
		*/
		uint32_t uniformLocation = glGetUniformLocation(m_ui32OpenGL_ID, name);
		// Call glUniform1i to set the uniform integer variable, passing obtained location of uniform var, and new integer value to be uploaded 
		glUniform1i(uniformLocation, value);
	}

	void OpenGLShader::uploadFloat(const char* name, float value)
	{
		// See above, except for a single float value
		uint32_t uniformLocation = glGetUniformLocation(m_ui32OpenGL_ID, name);
		glUniform1f(uniformLocation, value);
	}

	void OpenGLShader::uploadFloat2(const char* name, glm::vec2& value)
	{
		// See above, except uploading to 2d float vector 
		uint32_t uniformLocation = glGetUniformLocation(m_ui32OpenGL_ID, name);
		glUniform2f(uniformLocation, value.x, value.y);
	}

	void OpenGLShader::uploadFloat3(const char* name, glm::vec3& value)
	{
		// Se above but uploading to a 3d float vector
		uint32_t uniformLocation = glGetUniformLocation(m_ui32OpenGL_ID, name);
		glUniform3f(uniformLocation, value.x, value.y, value.z);
	}

	void OpenGLShader::uploadFloat4(const char* name, glm::vec4& value)
	{
		// See above but uploading to a 4d float vector
		uint32_t uniformLocation = glGetUniformLocation(m_ui32OpenGL_ID, name);
		glUniform4f(uniformLocation, value.x, value.y, value.z, value.w);
	}

	void OpenGLShader::uploadMat4(const char* name, glm::mat4& value)
	{
		// See above, but uploading to a 4d matrix, more details below
		uint32_t uniformLocation = glGetUniformLocation(m_ui32OpenGL_ID, name);
		/* 
			Call the glUniformMatrix4fv function, to upload the required values to the obtained uniform in the shader program, passing the 
			previously obtained uniformLocation uint32_t, with a count of 1, not transposed, and a glm::value_ptr to the 4 by 4 matrix that 
			needs to be uploaded by value to the uniform 4 by 4 in the shadr program
		*/
		glUniformMatrix4fv(uniformLocation, 1, GL_FALSE, glm::value_ptr(value));
	}

	void OpenGLShader::CompileAndLink(const char* vertexSource, const char* fragmentSource)
	{
		/*
			Instantiate and initialise uint32_t to hold created vertexShader, with the data returned by the glCreateShader function when passed 
			the macro(typedef?) GL_VERTEX_SHADER
		*/
		uint32_t vertexShader = glCreateShader(GL_VERTEX_SHADER);

		// Instantiate a const GLchar pointer source, with vertexSource string variable when the c_str function is applied(convert to a c style char array string)
		const GLchar* source = vertexSource;
		/* 
			Call the OpenGL function glShaderSource function passing the uint32_t vertexShader(the created vertex shader), the shader count(1),
			the reference to the source const GLchar ptr, the length of the string holding the shader(0, to indicate the string is null terminated, 
			which can kind of be thought of as letting the function know the string itself will tell it is ended with '\0' escape char, but that
			for the purposes of the function, it shouldbe assumed the length is unknown(? Imight be wrong?))
		*/
		glShaderSource(vertexShader, 1, &source, 0);
		// Call the OpenGL function glCompileShader, passing the uint32_t vertexShader to compile the shader
		glCompileShader(vertexShader);

		// int32_t isCompiled initialised as 0, to be used to hold the return compile status of the vertex shader
		int32_t isCompiled = 0;
		/*
			Call the OpenGL function glGetShaderiv, pass the uint32_T vertexShader, the GL_COMPILE_STATUS macro to check if the shader is compiled,
			and a reference to the int32_t isCompiled to store the return value of the compile status check
		*/
		glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &isCompiled);
		// If the returned compile status is false (0)
		if (isCompiled == GL_FALSE)
		{
			// Instantiate a GLint maxLength as 0
			int32_t maxLength = 0;
			/* 
				Call the OpenGL function glGetShaderiv, pass the uint32_t vertexShader,the GL_INFO_LOG_LENGTH macro returning the number of chars in the
				information log for the shader vertexShader, and a reference to maxLength to initialise with the returned value of GL_INFO_LOG_LENGTH
			*/
			glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);

			// Instantiate a vector of GLchars named infoLog, with a size of maxLength
			std::vector<GLchar> infoLog(maxLength);
			/*
				Call the OpenGL function glGetShaderInfoLog and pass the vertexShader Guint32_t, the length of the info log string held in maxLength, a 
				reference to the int32_t maxLength, and a reference to the first value stored in the vector infoLog (starting point of the vector to 
				read the info log into
			*/
			glGetShaderInfoLog(vertexShader, maxLength, &maxLength, &infoLog[0]);
			/*
				Output the error info to the console through the logging system, where the vector is read into an output string from the beginning
				to the end of the vector infoLog
			*/
			Logging::Error("Shader compile error: {0}", std::string(infoLog.begin(), infoLog.end()));

			// Call the OpenGL function glDeleteShader and pass the vertexShader identified by uint32_t as the shader to delete
			glDeleteShader(vertexShader);
			return;
		}

		// Repeat the same steps for the fragment shader as above for the vert shader
		uint32_t fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

		source = fragmentSource;
		glShaderSource(fragmentShader, 1, &source, 0);
		glCompileShader(fragmentShader);

		glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &isCompiled);
		if (isCompiled == GL_FALSE)
		{
			int32_t maxLength = 0;
			glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> infoLog(maxLength);
			glGetShaderInfoLog(fragmentShader, maxLength, &maxLength, &infoLog[0]);
			Logging::Error("Shader compile error: {0}", std::string(infoLog.begin(), infoLog.end()));

			//Deletes both shaders (typically fragment is compiled after vertex, typically shader compilation always follow that order
			glDeleteShader(fragmentShader);
			glDeleteShader(vertexShader);

			return;
		}

		// Assign the uint32_t identifier m_ui32OpenGL_ID to the return data from glCreateProgram(Sets m_ui32OpenGL_ID identify a shader program)
		m_ui32OpenGL_ID = glCreateProgram();
		// Attach the vertex shader with the ID as the member m_ui32OpenGL_ID 
		glAttachShader(m_ui32OpenGL_ID, vertexShader);
		// Attach the fragment shader with the ID as the member m_ui32OpenGL_ID 
		glAttachShader(m_ui32OpenGL_ID, fragmentShader);
		// Link the shaders now attached to the program together with the ID as the member m_ui32OpenGL_ID 
		glLinkProgram(m_ui32OpenGL_ID);

		// Performs similar check as applied to each shader compilation above to the linked shader program, checking link status with GL_LINK_STATUS
		int32_t isLinked = 0;
		glGetProgramiv(m_ui32OpenGL_ID, GL_LINK_STATUS, static_cast<int32_t*>(&isLinked));
		if (isLinked == GL_FALSE)
		{
			int32_t maxLength = 0;
			glGetProgramiv(m_ui32OpenGL_ID, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> infoLog(maxLength);
			glGetProgramInfoLog(m_ui32OpenGL_ID, maxLength, &maxLength, &infoLog[0]);
			Logging::Error("Shader linking error: {0}", std::string(infoLog.begin(), infoLog.end()));

			// Delete the shaders AND the created, but unsuccesfully linked shader program
			glDeleteProgram(m_ui32OpenGL_ID);
			glDeleteShader(vertexShader);
			glDeleteShader(fragmentShader);

			return;
		}

		// Detach the shaders, as they are no longer needed after being linked to the shader program
		glDetachShader(m_ui32OpenGL_ID, vertexShader);
		glDetachShader(m_ui32OpenGL_ID, fragmentShader);
	}
}

