/*!
 * \file OpenGLTexture.h 
 * \brief TODO WRITE BRIEF
 */

#pragma once

#include <cstdint>

#include "rendering/texture.h"
#include "systems/logging.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	/*!
	 * \class OpenGLTexture
	 * \brief Derived class to construct and destruct an OpenGLTexture object, containing the functionality to construct a texture object loaded in from a file,
	 * bind said texture to a model, and destory the texture object. Derived from the API agnostic texture base class
	 */
	class OpenGLTexture : public Texture
	{
	public:
		/*!
		 * \param filePath
		 * is a const char pointer to a string of characters representing the file path where the texture image file can be located
		 * 
		 * Constructor to create a texture loaded from an image file at the location indicated by the file path char pointer string passed to 
		 * the constructor
		 */
		OpenGLTexture(const char* filePath);
		/*!
		 * \param width
		 * is a 32 bit fixed width unsigned integer representing the width of the texture to be created
		 *
		 * \param height
		 * is a 32 bit fixed width unsigned integer representing the height of the texture to be created
		 *
		 * \param channels
		 * is a 32 bit fixed width unsigned integer representing the count of the (rgb(a)) channels
		 *
		 * \param data
		 * is a const char pointer to a string of characters representing the file path where the texture image file can be located
		 *
		 * Constructor to create a texture loaded from data, where the attributes are 32 bit fixed width unsigned integers representing
		 * width, height, (rgb(a))channel count, and an unsigned char pointer representing the data (can be thought of as a pointer of bytes)
		 */
		OpenGLTexture(uint32_t width, uint32_t height, uint32_t channels, unsigned char* data);
		//! Destructor for the OpenGLTexture object to destroy it when it is no longer needed
		~OpenGLTexture();
		/*!
		 * \param xOffset
		 * is a 32 bit fixed width unsigned integer indicating stride position to change with the width param to edit the x-axis
		 *
		 * \param yOffset
		 * is a 32 bit fixed width unsigned integer indicating stride position to change with the width param to edit the x-axis
		 *
		 * \param width
		 * is a 32 bit fixed width unsigned integer representing the width of the texture to be created
		 *
		 * \param height
		 * is a 32 bit fixed width unsigned integer representing the height of the texture to be created
		 *
		 * \param channels
		 * is a 32 bit fixed width unsigned integer representing the count of the (rgb(a)) channels
		 *
		 * \param data
		 * is a const char pointer to a string of characters representing the file path where the texture image file can be located
		 *
		 * Edit function to edit an OpenGLTexture object after construction, where the attributes are 32 bit fixed width unsigned integers representing
		 * x-axis offset, y-axis offset, width, height, (rgb(a))channel count, and an unsigned char pointer representing the data (can be thought of 
		 * as a pointer of bytes)
		 */
		virtual void Edit(uint32_t xOffset, uint32_t yOffset, uint32_t width, uint32_t height, unsigned char* data);
		//! Accessor function GetTextureID to obtain the m_ui32OpenGL_ID uint32_t identifier member variable 
		virtual inline uint32_t GetTextureID() override { return m_ui32OpenGL_ID; };
		//! Accessor function GetWidth to obtain the m_ui32Width uint32_t member variable storing the width of the texture
		virtual inline uint32_t GetWidth() override { return m_ui32Width; };
		//! Accessor function GetHeight to obtain the m_ui32Height uint32_t member variable storing the height of the texture
		virtual inline uint32_t GetHeight() override { return m_ui32Height; };
		//! Accessor function GetWidth to obtain the float member variable of the derived class storing the width of the texture
		virtual inline float GetWidthf() override { return m_fWidth = static_cast<float>(m_ui32Width); };
		//! Accessor function GetHeight to obtain the float member variable of the derived class storing the height of the texture
		virtual inline float GetHeightf() override { return m_fHeight = static_cast<float>(m_ui32Height); };
		//! Accessor function GetChannels to obtain the m_ui32Channels uint32_t member variable storing the channel count of the texture
		virtual inline uint32_t GetChannels() override { return m_ui32Channels; };
	private:
		//! Fixed width 32 bit unsigned integer member variable to store the identifier used for the constructed OpenGLTexture object by the GPU
		uint32_t m_ui32OpenGL_ID;
		//! Fixed width 32 bit unsigned integer member variable to store the width of the texture
		uint32_t m_ui32Width;
		//! Fixed width 32 bit unsigned integer member variable to store the height of the texture
		uint32_t m_ui32Height;
		//! Float member variable to store the width of the texture
		float m_fWidth;
		//! Float member variable to store the height of the texture
		float m_fHeight;
		//! Fixed width 32 bit unsigned integer member variable to store the channel count (1 = R (monochrome red), 3 = RGB, 4 = RGBA)
		uint32_t m_ui32Channels;
		/*!
		 * \param width
		 * is a 32 bit fixed width unsigned integers representing the width of the texture to be created
		 *
		 * \param height
		 * is a 32 bit fixed width unsigned integers representing the height of the texture to be created
		 *
		 * \param channels
		 * is a 32 bit fixed width unsigned integers representing the count of the (rgb(a)) channels
		 *
		 * \param data
		 * is a const char pointer to a string of characters representing the file path where the texture image file can be located
		 *
		 * Init function to init an OpenGLTexture object, where the attributes are 32 bit fixed width unsigned integers representing
		 * width, height, (rgb(a))channel count, and an unsigned char pointer representing the data (can be thought of as a pointer of bytes)
		 */
		virtual void Init(uint32_t width, uint32_t height, uint32_t channels, unsigned char* data);
	};
}