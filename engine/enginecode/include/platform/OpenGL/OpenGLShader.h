/*!
 * \file OpenGLShader.h 
 * \brief TODO WRITE BRIEF
 */

#pragma once

#include <cstdint>
#include <glm/glm.hpp>

#include "rendering/shader.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	/*!
	 * \class OpenGLShader
	 * \brief Class to construct and destruct an OpenGLShader object, containing the functionality to read shader code in from a file,
	 * to upload the required information, to access the shader via an identifier, and to compile and link the shader code into a shader program
	 */
	class OpenGLShader : public Shader
	{
	public:
		/*!
		 *\param vertexFP 
		 * is a const char* to the file path where the vertex shader can be located (string, not worth the extra operations to use std::string)
		 *\param fragmentFP 
		 * is a const char* to the file path where the fragment shader can be located (string, not worth the extra operations to use std::string)
		 *
		 * This constructor constructs a OpenGLShader object out of one or more files containing shader code (currently has the functionality for two)
		 * when given a string of chars through a const char*, can easily enough be expanded to include the other types of shaders such as geometry shaders
		 */
		OpenGLShader(const char* vertexFP, const char* fragmentFP);
		/*!
		 *\param filePath
		 * is a const char* to the file path where the vertex shader can be located
		 *
		 * This constructor constructs a OpenGLShader object out of one or more files containing shader code (currently has the functionality for two)
		 * when given a string of chars through a const char*, can easily enough be expanded to include the other types of shaders such as geometry 
		 * that are included in its local enum
		 */
		OpenGLShader(const char* filePath);
		/*! 
		 * Destructor for the OpenGLShader class, calls the OpenGL function gLDeleteShaders which is passed the member fixed width 32 bit unsigned 
		 * integer m_ui32OpenGL_ID that identifies the shader on the gpu
		 */
		~OpenGLShader();
		//! GetShaderID is the accessor function for the member fixed width 32 bit integer used to identify a shader held on the gpu
		virtual uint32_t GetShaderID() const { return m_ui32OpenGL_ID; };

		/*!
		 *\param name
		 * is a const char* that holds the name string of the uniform integer veriable to be uploaded to in the shader program (string, not worth the 
		 * extra operations to use std::string)
		 *\param value
		 * is an integer representing the value to be uploaded to the named uniform integer variable in the shader program
		 *
		 * Uploads an integer to a named uniform variable in the shader program
		 */
		virtual void uploadInt(const char* name, int value);
		/*!
		 *\param name
		 * is a const char* that holds the name string of the uniform float variable to be uploaded to in the shader program (string, not worth the
		 * extra operations to use std::string)
		 *\param value
		 * is a float representing the value to be uploaded to the named uniform float variable in the shader program
		 *
		 * Uploads an float to a named uniform variable in the shader program
		 */
		virtual void uploadFloat(const char* name, float value);
		/*!
		 *\param name
		 * is a const char* that holds the name string of the uniform 2d float vector variable to be uploaded to in the shader program (string, not 
		 * worth the extra operations to use std::string)
		 *\param value
		 * is an glm::vec2 representing the value to be uploaded to the named uniform 2d float vector variable in the shader program
		 *
		 * Uploads an glm::vec2 2d float vector to a named uniform 2d float vector variable in the shader program
		 */
		virtual void uploadFloat2(const char* name, glm::vec2& value);
		/*!
		 *\param name
		 * is a const char* that holds the name string of the uniform 3d float vector variable to be uploaded to in the shader program (string, not
		 * worth the extra operations to use std::string)
		 *\param value
		 * is an glm::vec3 representing the value to be uploaded to the named uniform 3d float vector variable in the shader program
		 *
		 * Uploads an glm::vec3 3d float vector to a named uniform 3d float vector variable in the shader program
		 */
		virtual void uploadFloat3(const char* name, glm::vec3& value);
		/*!
		 *\param name
		 * is a const char* that holds the name string of the uniform 4d float vector variable to be uploaded to in the shader program (string, not
		 * worth the extra operations to use std::string)
		 *\param value
		 * is an glm::vec4 representing the value to be uploaded to the named uniform 4d float vector variable in the shader program
		 *
		 * Uploads an glm::vec4 4d float vector to a named uniform 4d float vector variable in the shader program
		 */
		virtual void uploadFloat4(const char* name, glm::vec4& value);

		/*!
		 *\param name
		 * is a const char* that holds the name string of the uniform 4d matrix variable to be uploaded to in the shader program (string, not
		 * worth the extra operations to use std::string)
		 *\param value
		 * is a glm::mat4 representing the value to be uploaded to the named uniform 4d float vector variable in the shader program
		 *
		 * Uploads a glm::mat4 4d matrix to a named uniform 4d matrix variable in the shader program
		 */
		virtual void uploadMat4(const char* name, glm::mat4& value);

	private:
		//! Fixed width 32bit unsigned integer member variable used to identify the shader program stored on the gpu
		uint32_t m_ui32OpenGL_ID;
		// Can and should be expanded to include other kinds of shaders, for now just includes vertex and fragment to be linked together
		/*!
		 *\param vertexSource
		 * is a const char* that holds the name string of the string variable holding the shader program source code read in from the vertex shader
		 * source file (string, not worth the extra operations to use std::string)
		 *\param fragmentSource
		 * is a const char* that holds the name string of the string variable holding the shader program source code read in from the fragment shader
		 * source file (string, not worth the extra operations to use std::string)
		 *
		 * Compiles and links the shader source code read in from a file into a string for usage on the gpu as exectuable code, currently supports  
		 * vertex and fragment shaders only.
		 */
		virtual void CompileAndLink(const char* vertexSource, const char* fragmentSource);
	};
}
