/*!#
 * \file OpenGLIndexBuffer.h
 * \brief TODO WRITE BRIEF
 */
#pragma once

#include "rendering/indexBuffer.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	class OpenGLIndexBuffer : public IndexBuffer
	{
	public:
		/*!
		 * \param indices
		 * is a pointer to an array of fixed width unsigned 32 bit integers representing the indices data to be used for indexed rendering
		 * \param count
		 * is a fixed width 32 bit unsigned integer representing the count of the total indices 
		 *
		 * Constructor for the OpenGLIndexBuffer class
		 */
		OpenGLIndexBuffer(uint32_t* indices, uint32_t count);
		//! The destructor for the OpenGLIndexBuffer class
		virtual ~OpenGLIndexBuffer();
		//! Inline function returning the fixed width 32 bit unsigned integer member variable m_ui32OpenGL_ID used as an identifier for the OpenGLIndexBuffer
		virtual inline uint32_t GetRenderingID() const override { return m_ui32OpenGL_ID; };
		//! Inline function returning the fixed width 32 bit unsigned integer member variable m_ui32Count holding the total count of indices
		virtual inline uint32_t GetCount() const override { return m_ui32Count; };
	private:
		//! Member uint32_t variable to store the rendering ID Handle
		uint32_t m_ui32OpenGL_ID;
		//! Member uint32_t variable to store the indices count
		uint32_t m_ui32Count;
	};
}
