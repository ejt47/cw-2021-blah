/*!
 * \file OpenGLVertexArray.h 
 *\brief TODO WRITE BRIEF
 */

#pragma once

#include <vector>
#include <memory>

#include "rendering/vertexArray.h"

#include "OpenGLVertexBuffer.h"
#include "OpenGLIndexBuffer.h"


/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	/*!
	 * \class OpenGLVertexArray
	 * \brief Derived class to construct and destruct an OpenGLVertexArray object, containing the functionality to describe how the vertex attributes
	 * are stored in the vertex buffer object, derived from the VertexArray API agnostic base class
	 */
	class OpenGLVertexArray : public VertexArray
	{
	public:
		//! OpenGLVertexArray constructor creates and binds the Vertex Array Object using the uint32_t member identifier m_ui32OpenGL_ID
		OpenGLVertexArray();
		//! OpenGLVertexArray destructor deletes the Vertex Array Object
		~OpenGLVertexArray();
		/*!
		 * \param vertexBuffer
		 * is a const reference to a polymorphic VertexBuffer shared pointer, used to add the vertex attributes to the Vertex Buffer Object
		 */
		virtual void AddVertexBuffer(const std::shared_ptr</*OpenGL*/VertexBuffer>& vertexBuffer);
		/*!
		 * \param indexBuffer
		 * is a const reference to a polymorphic VertexBuffer shared pointer, used to keep track of the draw count between indices held in the
		 * Index Buffer Object in the function GetDrawCount
		 */
		//void SetIndexBuffer(const std::shared_ptr<OpenGLIndexBuffer>& indexBuffer);
		//api agnostic imp
		virtual void SetIndexBuffer(const std::shared_ptr<IndexBuffer>& indexBuffer);

		virtual inline std::shared_ptr<IndexBuffer> GetIndexBuffer() const { return m_pIndexBuffer; };

		virtual inline std::vector<std::shared_ptr<VertexBuffer>> GetVertexBuffer() const { return m_vpVertexBuffer; };

		//! Inline function GetRenderingID returning with constness the member uint32_t m_ui32OpenGL_ID used to identify the Vertex Array Object
		virtual inline uint32_t GetRenderingID() const { return m_ui32OpenGL_ID; };
		/*! 
		 * Inline function GetDrawCount, inline because small enough function. Used to keep track of the current draw count between indices accessed
		 * via the pointer to the Index Buffer Object m_pIndexBuffer
		*/
		virtual inline uint32_t GetDrawCount() const override
		{
			// if m_pIndexBuffer doesn't return a nullptr
			if (m_pIndexBuffer)
			{
				// access the OpenGLIndexBuffer function GetCount to return the draw count between indices
				return m_pIndexBuffer->GetCount();
			}
			//otherwise if nullptr return 0
			else return 0;
		};

	private:
		//! Fixed width 32 bit unsigned integer member variable m_ui32OpenGL_ID
		uint32_t m_ui32OpenGL_ID;
		//! Fixed width 32 bit unsigned integer member variable m_ui32AttributeIndex, used to iterate through the vertex attribute array
		uint32_t m_ui32AttributeIndex = 0;
		//! Vector of polymorphic shared pointers to VertexBuffer objects m_pVertexBuffer
		//std::vector<std::shared_ptr<OpenGLVertexBuffer>> m_pVertexBuffer;
		//api agnostic imp
		std::vector<std::shared_ptr<VertexBuffer>> m_vpVertexBuffer;
		//! Shared pointer to a polymorphic IndexBuffer object m_pIndexBuffer
		//std::shared_ptr<OpenGLIndexBuffer> m_pIndexBuffer;
		//api agnostic imp
		std::shared_ptr<IndexBuffer> m_pIndexBuffer;
	};
}
