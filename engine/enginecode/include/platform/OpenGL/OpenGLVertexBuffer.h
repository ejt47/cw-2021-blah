/*! 
 * \file OpenGLVertexBuffer.h
 * \brief OpenGLVertexBuffer class to encapsulate the construction, binding, editing and destruction of OpenGLVertexBuffers
 */

#pragma once

#include "rendering/vertexBuffer.h"
//#include "rendering/bufferLayout.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	/*!
	 * \class OpenGLVertexBuffer
	 * \brief OpenGLVertexBuffer class to encapsulate the construction, binding, editing and destruction of OpenGLVertexBuffers, derived from the 
	 * VetexBuffer base class
	 */
	class OpenGLVertexBuffer : public VertexBuffer
	{
	public:
		/*!
		 * \param vertices
		 * is a void pointer to vertices data stored in an array to be added to the buffer
		 * \param size
		 * is a uint32_t holding the size of the buffer
		 * \param layout
		 * is a BufferLayout object holding some of the basic information and functions needed to fill the VertexBuffer 
		 *
		 * Constructor for the derived OpenGLVertexBuffer class.
		 */
		OpenGLVertexBuffer(void* vertices, uint32_t size, BufferLayout layout);
		//! Destructor for the OpenGLVertexBuffer class
		~OpenGLVertexBuffer();
		/*!
		 * \param vertices
		 * is a void pointer to the new vertices data stored in an array to be added to the buffer
		 * \param size
		 * is a uint32_t holding the new size of the buffer
		 * \param offset
		 * is a uint32_t holding the offset of the the buffer
		 *
		 * Edit function to edit the data held in an OpenGLBuffer object
		 */
		virtual void Edit(void* vertices, uint32_t size, uint32_t offset) const override;
		//! Inline function GetRenderingID returns the member uint32_t m_ui32OpenGL_ID used as the handle for the OpenGLVertexBuffer object
		virtual inline uint32_t GetRenderingID() const override { return m_ui32OpenGL_ID; };
		//! Inline function GetLayout returns a const reference to the member BufferLayout object m_bufferLayout
		virtual inline BufferLayout& GetLayout() /*const*/ override { return m_bufferLayout; }; // not const override bcsu would not compile, const can't be converted to const& error
	private:
		//! Member uint32_t variable to store the rendering ID Handle
		uint32_t m_ui32OpenGL_ID;
		//! Member BufferLayout object m_bufferLayout to store the OpenGLVertexBufferObject layout
		BufferLayout m_bufferLayout;
	};
}
