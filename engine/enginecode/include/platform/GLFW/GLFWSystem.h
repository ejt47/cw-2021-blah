/*!
 * \file GLFWSystem.h
 * \brief GLFWSystem class header file, declaring the GLFWSystem class functionality, inheriting from the System interface class
 */

#pragma once

#include "systems/system.h"
#include <GLFW/glfw3.h>

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	/*!
	 * \class GLFWSystem
	 * \brief GLFWSystem class inheriting from the System interface class to handle the initialisation and termination of the GLFW windowing system.
	 *
	 * GLFWSystem class to provide the functionality for the intialisation and termination of the GLFW windowing system, this is done as a System derived class because
	 * glfwInit is not used to open a window but to intialise a windowing system that can be used to then open one or more windows. Calling glfwInit more than once will
	 * essentially break the windowing system, and cause undesired behaviour by initialising the windowing system multiple times. 
	 *
	 * The same reasoning applies to glfwTerminate, which if called the glfw windowing system will then terminate. This will close the window it is is called upon to terminate,
	 * but will also terminate the GLFW windowing system, which will then cause any other GLFW windows that are open to stop working. 
	 *
	 * Therefore it makes sense to start and stop the windowing system alongside other system class implementations in the desired order as done previously. This effectively
	 * isolates these functions to the start and stop methods of the application instance. Although care must still be taken not to use these function calls elsewhere.
	 */
	class GLFWSystem : public System
	{
	public:
		//! Virtual void function Start used here to initialise the GLFW system using glfwInit() wrapped in some logic to ensure glfw initialised correctly
		virtual void Start(SystemSignal init = SystemSignal::None, ...) override;
		//! Virtual void function Stop used here to terminate the GLFW system using glfwTerminate()
		virtual void Stop(SystemSignal close = SystemSignal::None, ...) override;
	};
}
