/*!
 * \file GLFWOpenGLGraphicsContext.h
 * \brief TODO WRITE BRIEF
 */
#pragma once

#include "core/graphicsContext.h"

 /*!
  * \namespace Engine
  * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
  * every defintion and declaration. Keeps the codebase looking more organised and tidy.
  */

namespace Engine
{
	class GLFWOpenGLGraphicsContext : public GraphicsContext
	{
	public:
		//! Constructor so that the GLFWwindow pointer m_nativeWindowGLFW member can be intialised via list
		GLFWOpenGLGraphicsContext(GLFWwindow* window) : m_nativeWindowGLFW(window) {};
		//! Virtual void function Init to initialise 
		virtual void Init() override;
		//! Virutal void function SwapBuffers to swap the front and back buffers for double buffering
		virtual void SwapBuffers() override;
	private:
		//! Pointer to the glfw window instance
		GLFWwindow* m_nativeWindowGLFW;
	};
}