/*! 
 * \file GLFWInputPolling.h 
 * \brief GLFWInputPolling class header file through which GLFW specific code for input polling is declared.
 */

#pragma once

#include <glm/glm.hpp>
#include "GLFW/glfw3.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine
{
	/*!
	 * \class GLFWInputPolling
	 * \brief GLFWInputPolling is a platform specific class for polling the current state of a keyboard or mouse input through GLFW. 
	 */
	class GLFWInputPolling
	{
	public:
		/*!
		 * \param currentWindow
		 * is a GLFWwindow pointer to a glfw window that will be cast back out to a glfw window pointer to the native application window.
		 *
		 * SetCurrentWindow is a static function returning void, it is passed a GLFWwindow pointer to the current application window, which is used to initialise the
		 * sm_pWindow static member GLFWwindow pointer. In practice this will then be utilised by the central InputPolling class that makes the compile-time choice
		 * to use GLFW or another platform specific API as a void pointer that is cast back to the appropriate pointer based on the preprocessing compile time choice.
		 * (Currently only GLFW is being implemented.)
		 */
		static void SetCurrentWindow(GLFWwindow* currentWindow) { sm_pWindow = currentWindow; }
		/*!
		 * \param keyCode
		 * is a fixed width 32bit integer representing a keyboard key identification code
		 *
		 * CheckKeyPressed is a static function returning a bool, it is passed a keyboard key identification code as a fixed width 32bit integer
		 */
		static bool CheckKeyPressed(int32_t keyCode);
		/*!
		 * \param buttonCode
		 * is a fixed width 32bit integer representing a mouse button identification code
		 *
		 * CheckMouseButtonPressed is a static function returning a bool, it is passed a mouse button identification code as a fixed width 32bit integer
		 */
		static bool CheckMouseButtonPressed(int32_t buttonCode);
		//! GetCurrentMousePosition is a static function returning a glm::vec2, to give the x, y coordinates of the mouse cursor position as a glm 2d vector object
		static glm::vec2 GetCurrentMousePosition();
	private:
		//! The static GLFWwindow pointer used to access the current application window
		static GLFWwindow* sm_pWindow;
	};
}