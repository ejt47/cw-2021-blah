/*!
 * \file windowInterface.h
 * \brief WindowInterface header file, the interface class for any window classes. 
 * 
 * The WindowInterface header file contains a struct WindowProperties to hold information on the window's properties, that can be passed by reference to some of
 * the functions defined within the WindowInterface class for initialisation and creation of a window. Alongside the pure virtual function declarations needed for
 * platform agnostic window classes, as well as the defined functions to get the EventHandler class object and create a window, and an instantiated EventHandler class
 * object.  
 */
#pragma once

#include "events/eventHandler.h"
#include "systems/logging.h"
#include "core/graphicsContext.h"

 /*!
  * \namespace Engine
  * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
  * every defintion and declaration. Keeps the codebase looking more organised and tidy.
  */

namespace Engine
{
	/*! 
	 * \struct WindowProperties
	 * \brief WindowProperties struct holds platform independent information required of any platform specific window to be created through the WindowInterface class.
	 */
	struct WindowProperties
	{
		//! String sWindowTitle to hold the window title string
		std::string m_sWindowTitle;
		//! Fixed width 32 bit unsigned integer m_ui32Width to hold the window width
		uint32_t m_ui32Width;
		//! Fixed width 32 bit unsigned integer m_ui32Height to hold the window height
		uint32_t m_ui32Height;
		//! Bool to indicate whether the window is in full screen or not
		bool m_bFullScreen;
		//! Bool to indicate whether CheckVSync is turned on or not
		bool m_bVSync;

		/*!
		 * \param sTitle
		 * is a std::string, which will hold the window title string
		 * \param ui32Width 
		 * is a fixed width 32bit unsigned integer to hold the width of the window 
		 * \param ui32Height 
		 * is a fixed width 32bit unsigned integer to hold the height of the window
		 * \param bFullScreen 
		 * is a bool flag to indicate whether the window is in full screen or not
		 * \param bVSync
		 * is a bool flag to indicate whether CheckVSync is turned on or off
		 *
		 * WindowProperties constructor, holding default window settings, can be assigned other values when a constructed WindowProperties object reference is passed to the
		 * CreateWindow function. Initialised through an initialisation list.
		 */
		WindowProperties(const std::string& sTitle = "Default Window", uint32_t ui32Width = 800, uint32_t ui32Height = 600, bool bFullScreen = false, bool bVSync = false)
			: m_sWindowTitle(sTitle), m_ui32Width(ui32Width), m_ui32Height(ui32Height), m_bFullScreen(bFullScreen), m_bVSync(bVSync) {}
	};

	/*!
	 * \class WindowInterface
	 * \brief Platform independent abstract base class for implementation of platform specific Window system classes programmed via this interface.
	 *
	 * The abstract interface base class makes use of a struct WindowProperties to hold the window properties, with default settings. These properties are: the window title as 
	 * a string, the window width as a uint32_t, the window height as a uint32_t, a bool flag for full screen, a bool flag for vsync.
	 */

	class WindowInterface
	{
	public:
		/*!
		 * \param windowProperties
		 * is a const reference to a WindowProperties object
		 *
		 * Pure virtual void function to initialise a window. 
		 */
		virtual void Init(const WindowProperties& windowProperties) = 0;
		//! Pure virtual void function to close a window.
		virtual void Close() = 0;
		/*!
		 * \param fDeltaTime
		 * is a float that holds the time passed since the last update.
		 * 
		 * Pure virtual void function to perform some action upon frame update
		 */
		virtual void OnUpdate(float fDeltaTime) = 0;
		/*!
		 * \param bVSync
		 * is a bool that is used to indicate whether Vsync is turned on or off
		 *
		 * Pure virtual void function that will be used to turn CheckVSync on or off
		 */
		virtual void SetVSync(bool bVSync) = 0;
		//! Pure virtual void function to get the width of the window 
		virtual uint32_t GetWidth() const = 0;
		//! Pure virtual void function to get the height of the window
		virtual uint32_t GetHeight()const = 0;
		//! Pure virtual void* function to get the void* to the native window context
		virtual void* GetNativeWindow() const = 0;
		//! Pure virtual void function to check if full screen is on or off through a returned bool flag
		virtual bool CheckFullScreenMode() const = 0;
		//! Pure virtual void function to check if VSync is on or off through a returned bool flag 
		virtual bool CheckVSync() const = 0;

		//! Inline function returning a reference to the EventHandler object member
		inline EventHandler& GetEventHandler() { return m_eventHandler; };

		/*!
		 * \param windowProperties
		 * is a const reference to a WindowProperties struct object
		 *
		 * Static WindowInterface* function to create a window based on the passed const reference to a WindowProperties struct object 
		 */
		static WindowInterface* WindowCreate(const WindowProperties& windowProperties = WindowProperties());
	protected:
		//! Member shared pointer to a GraphicsContext object
		std::shared_ptr<GraphicsContext> m_graphicsContext;
		/*!
		 *	EventHandler member object m_eventHandler, I agree with Simon in his example that it makes most sense to have this here, no point repeating myself for every
		 *	WindowInterface implementation (even if currently the plan is to only implement one WindowInterface sub-class)
		 */
		EventHandler m_eventHandler;
	};
}
