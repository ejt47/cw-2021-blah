/*! 
 * \file graphicsContext.h
 * \brief TO DO: WRITE OUT SOME INFO WHEN UNDERSTANDING IS THERE
 */
#pragma once

 /*!
  * \namespace Engine
  * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
  * every defintion and declaration. Keeps the codebase looking more organised and tidy.
  */

namespace Engine
{
	class GraphicsContext
	{
	public:
		//! Virtual void function Init to initialise 
		virtual void Init() = 0;
		//! Virutal void function SwapBuffers to swap the front and back buffers for double buffering
		virtual void SwapBuffers() = 0;
	};
}
