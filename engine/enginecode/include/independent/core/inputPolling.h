/*! 
 * \file inputPolling.h 
 * \brief InputPolling class header file through which functionality for platform specific code for input polling can be accessed through preprocessor compile time choices is declared.
 */

#pragma once

#include <glm/glm.hpp>

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine
{
	/*!
	* \class InputPolling
	* \brief InputPolling is a platform independent class for polling the current state of an input. 
	* 
	* InputPolling class will access platform specific InputPolling classes and can then pre-processor compile time choices to decide which platform specific code to run.
	* Is used to find the current state of input coming from mouse and keyboard.
	*/
	class InputPolling
	{
	public:
		/*! 
		 * \param currentWindow 
		 * is a void pointer to a glfw window that will be cast back out to a glfw window pointer to the native applicatiopn window.
		 * 
		 * SetCurrentWindow is a static function returning void, it is passed a void pointer to the current application window, which is cast back out to a GLFWwindow pointer
		 */
		static void SetCurrentWindow(void* currentWindow);
		/*!
		 * \param keyCode
		 * is a fixed width 32bit integer representing a keyboard key identification code
		 *
		 * CheckKeyPressed is a static function returning a bool, it is passed a keyboard key identification code as a fixed width 32bit integer
		 */
		static bool CheckKeyPressed(int32_t keyCode);
		/*!
		 * \param buttonCode
		 * is a fixed width 32bit integer representing a mouse button identification code
		 *
		 * CheckMouseButtonPressed is a static function returning a bool, it is passed a mouse button identification code as a fixed width 32bit integer
		 */
		static bool CheckMouseButtonPressed(int32_t buttonCode);
		//! GetCurrentMousePosition is a static function returning a glm::vec2, to give the x, y coordinates of the mouse cursor position as a glm 2d vector object
		static glm::vec2 GetCurrentMousePosition();
		//! GetCurrentMousePositionX is a static function returning a float, to give the x coordinate of the mouse cursor position
		inline static float GetCurrentMousePositionX() { return GetCurrentMousePosition().x; };
		//! GetCurrentMousePositionY is a static function returning a float, to give the y coordinate of the mouse cursor position
		inline static float GetCurrentMousePositionY() { return GetCurrentMousePosition().y; };
	};
}
