/*!
 * \file application.h
 * \brief Application class header file, a fundamental class of the engine. A singleton instance which runs the game loop indefinitely.
 */
#pragma once

//#include "systems/logging.h"
#include "core/timing/chronoTiming.h"
//#include "systems/randomisation.h"
#include "systems/systemHeaders.h"
//#include "events/eventHeaders.h" //not needed if eventHandler.h is included as that already contains the eventHeaders.h include
#include "events/eventHandler.h"
#include "core/windows/windowInterface.h"
#include "core/inputPolling.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine {

	/*!
	 * \class Application
	 * \brief Application class, a fundamental class of the engine. A singleton instance which runs the game loop indefinitely.
	 *
	 * Provides member pointers and objects to access the systems that will be started and stopped in the order desired inside the Application 
	 * instance through its constructor and destructor. Class essentially manages all the engine sub systems. An access point through which most things
	 * will happen in the engine.
	 */

	class Application
	{
	protected:
		//! Constructor
		Application(); 

		//! Member shared pointer to access polymorphically the logging system's functions
		std::shared_ptr<System> m_pLoggingSystem;
		//! Member shared pointer to access polymorphically the randmoisation system's functions
		std::shared_ptr<System> m_pRandomisationSystem;
		//! Member shared pointer to access polymorphically the windowing system (allowing for compile time windowing system choice)
		std::shared_ptr<System> m_pWindowSystem;

		//! Member shared pointer to access polymorphically a window object (right now only using a GLFW implementation)
		std::shared_ptr<WindowInterface> m_pWindow;
		//! Member shared pointer to access polymorphically a timing object (right now only using std::chrono)
		std::shared_ptr<TimingInterface> m_pTiming;

		/*! 
		 * \param windowClosed
		 * windowClosed is a reference to a WindowClosed event object.
		 *
		 * OnWindowClose function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, m_bRunning, the bool that controls the game loop
		 * is set to false (ending the game while loop), returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 *
		 * Realistically this doesn't seem to be the best method to close a window and will probably need revisiting when the Windowing system is set up.
		 */
		bool OnWindowClose(WindowClosed& windowClosed);

		/*!
		 * \param windowResized 
		 * windowResized is a reference to a WindowResized event object.
		 *
		 * OnWindowResize function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, performs some placeholder event logic through
		 * the logging system, then returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 */
		bool OnWindowResize(WindowResized& windowResized);

		/*!
		 * \param windowFocus
		 * windowFocus is a reference to a WindowFocus event object.
		 *
		 * OnWindowFocus function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, performs some placeholder event logic through
		 * the logging system, then returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 */
		bool OnWindowFocus(WindowFocus& windowFocus);

		/*!
		 * \param windowLostFocus
		 * windowLostFocus is a reference to a WindowLostFocus event object.
		 *
		 * OnWindowLostFocus function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, performs some placeholder event logic
		 * through the logging system, then returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 */
		bool OnWindowLostFocus(WindowLostFocus& windowLostFocus);

		/*!
		 * \param windowMoved
		 * windowMoved is a reference to a windowMoved event object.
		 *
		 * OnWindowMove function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, performs some placeholder event logic through
		 * the logging system, then returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 */
		bool OnWindowMove(WindowMoved& windowMoved);

		/*!
		 * \param keyPressed
		 * keyPressed is a reference to a KeyPressed event object. 
		 *
		 * OnKeyPress function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, performs some placeholder event logic through
		 * the logging system, then returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 */
		bool OnKeyPress(KeyPressed& keyPressed);

		/*!
		 * \param keyReleased
		 * keyReleased is a reference to a KeyReleased event object.
		 *
		 * OnKeyRelease function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, performs some placeholder event logic through
		 * the logging system, then returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 */
		bool OnKeyRelease(KeyReleased& keyReleased);

		/*!
		 * \param keyTyped
		 * keyTyped is a reference to a KeyTyped event object.
		 *
		 * OnKeyTyped function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, performs some placeholder event logic through
		 * the logging system, then returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 */
		bool OnKeyTyped(KeyTyped& keyTyped);

		/*!
		 * \param buttonPressed
		 * buttonPressed is a reference to a MouseButtonPressed event object.
		 *
		 * OnMouseButtonPress function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, performs some placeholder event logic 
		 * through the logging system, then returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 */
		bool OnMouseButtonPress(MouseButtonPressed& buttonPressed);

		/*!
		 * \param buttonReleased
		 * buttonReleased is a reference to a MouseButtonReleased event object.
		 *
		 * OnMouseButtonRelease function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, performs some placeholder event logic
		 * through the logging system, then returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 */
		bool OnMouseButtonRelease(MouseButtonReleased& buttonReleased);

		/*!
		 * \param mouseMoved
		 * mouseMoved is a reference to a MouseMoved event object.
		 *
		 * OnMouseMove function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, performs some placeholder event logic through
		 * the logging system, then returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 */
		bool OnMouseMove(MouseMoved& mouseMoved);

		/*!
		 * \param mouseScrolled
		 * mouseScrolled is a reference to a MouseScrolled event object.
		 *
		 * OnMouseScroll function calls the Event class function HandleEvent, passing true, to indicate the event has been handled, performs some placeholder event logic
		 * through the logging system, then returns the Event class function EventHandled, indicating the event was handled if true, or not if false.
		 */
		bool OnMouseScroll(MouseScrolled& mouseScrolled);

	private:
		//! Singleton instance of the application
		static Application* s_pApplicationInstance; 
		//! Is the application running?
		bool m_bRunning = true; 
		/*!
		 * Current idea is to allocate memory for each system here and call it first in Application constructor before starting systems,
		 * need to ensure all system Stop functions reset shared pointers, I think shared pointers will manage themselves anyway, but better
		 * safe than sorry.
		 */
		void MemoryAllocations();
		//! Function to wrap the initialisation of all of the abstract event callback setter functions
		void InitEventCallbacks();
		//! Void function to initialise InputPolling window instance pointer to the pointer returned when m_pWindow accesses the GetNativeWindow function
		void InitInputPolling() { InputPolling::SetCurrentWindow(m_pWindow->GetNativeWindow()); }
	public:
		//! Deconstructor
		virtual ~Application(); 
		//! Instance getter from singleton pattern
		inline static Application& GetInstance() { return *s_pApplicationInstance; } 
		/*!
		 *	Function GetApplicationWindow returns a reference to the shared pointer to the application window, returning a reference avoids incrementation and decrementation 
		 *	of the shared pointer ownership count, as this is going to be called by the input poller quite often.
		 */
		//inline std::shared_ptr<WindowInterface>& GetApplicationWindow() { return m_pWindow; }
		//! Main loop
		void Run(); 
	};

	//! Function definition which provides an entry hook
	Application* StartApplication(); 
}