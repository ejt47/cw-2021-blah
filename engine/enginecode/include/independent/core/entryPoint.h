/*!
 * \file entryPoint.h
 * \brief EntryPoint header file to define the main function that executes the start-up of the engine's Apllication singleton, deletes the singleton instance when game loop ends.
 */
#pragma once

#include "core/application.h"

extern Engine::Application* Engine::StartApplication();

int main(int argc, char** argv)
{
	auto application = Engine::StartApplication();
	application->Run();
	delete application;

	return 0;
}

