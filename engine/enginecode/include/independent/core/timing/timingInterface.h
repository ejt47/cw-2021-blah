/*!
 * \file timingInterface.h
 * \brief TimingInterface header file, the interface class for any timing classes.
 */
#pragma once

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	/*!
	 * \class TimingInterface
	 * \brief TimingInterface class is a pure virtual class through which either cross platform libraries such as std::chrono, or platform specific libraries such as WinAPI
	 * can be built through an interface.
	 */

	class TimingInterface
	{
		/* JUST AS A NOTE TO MYSELF, IS HAVING A START AND RESET FUNCTION NOT A BIT REDUNDANT? DOES RESET NOT JUST START THE TIMER??? */
	public:
		//! Pure virtual void abstract interface Start function
		virtual void Start() = 0;
		//! Pure virtual void abstract interface Reset function CURRENTLY UNUSED AND UNDEFINED, DECIDE LATER IF NECESSARY
		virtual void Reset() = 0;
		//! Pure virtual float abstract interface GetDeltaTime function 
		virtual float GetDeltaTime() = 0;
	};
}