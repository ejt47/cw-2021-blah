/*!
 * \file chronoTiming.h
 * \brief ChronoTiming class header file, class that uses std::chrono::steady_clock to measure timings. Deriving from TimerInterface base interface class.
 */
#pragma once

#include "timingInterface.h"
#include <chrono>

 /*!
  * \namespace Engine
  * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
  * every defintion and declaration. Keeps the codebase looking more organised and tidy.
  */

namespace Engine 
{
	/*!
	 * \class ChronoTiming
	 * \brief ChronoTiming class that uses std::chrono::steady_clock to measure timings. Deriving from TimerInterface base interface class.
	 *
	 * Deriving from TimerInterface base interface class, timing based on std::chrono will provide some degree of portability.
	 * I've decided to use std::chrono::steady_clock here as according to https://en.cppreference.com/w/cpp/chrono/high_resolution_clock
	 * std::chrono::high_resolution_clock is not implemented consistently across different c++ std library implementations, despite granting
	 * the smallest tick period, if the aim of this particular ChronoTiming class is portability, then it is suggested by the above link to
	 * use the monotonic std::chrono::steady_clock for duration measurements. 
	 */

	class ChronoTiming : public TimingInterface
	{
	public:
		/*! 
		 * Virtual inline void function Start records the time of the start of a duration period using std::chrono::steady_clock::now(),
		 * has been left as virtual in case I have need for further abstraction based off this class.
		 */
		virtual inline void  Start() override { m_chronoStartPoint = std::chrono::steady_clock::now(); };
		/*! 
		 * I am going to leave this Reset function unimplemented for now because it would literally be defined the same way as Start(),
		 * there may be some use for it in a more specific platform timer class, but I can't see the point in repeating myself.
		 */
		virtual inline void  Reset() override {};
		/*!
		 * Virtual float function GetDeltaTime obtains the delta time (change in time(elapsed time)) between two std::chrono::time_points 
		 * by recording the std::chrono::time_point at the end of the interval to m_chronoEndPoint by calling the std::chrono::steady_clock::now
		 * function to get the current time, it then subtracts the previously recorded std::chrono::time_point m_chronoStartPoint from 
		 * m_chronoEndPoint and records the resulting delta into the std::chrono::duration m_chonoDelta, the std::chrono::duration count function
		 * is then called on this m_chronoDelta, returning the count of ticks (or frames) that occured during the m_chronoDelta interval between
		 * m_chronoStartPoint and m_chronoEndPoint. Depending on how the std::chrono::duration object is formatted, conversions may need to be 
		 * made, I am using seconds, but if it were for example milliseconds the returned value from the count function would need to be divided
		 * by 1000.f to convert the tick count per millisecond to the tick count per second
		 */
		virtual float GetDeltaTime() override
		{
			m_chronoEndPoint = std::chrono::steady_clock::now();
			m_chronoDelta = m_chronoEndPoint - m_chronoStartPoint;

			return m_chronoDelta.count();
		};
	private:
		//! std::chrono::time_point<std::chrono::steady_clock> m_chronoStartPoint represents the start point of a timed interval
		std::chrono::time_point<std::chrono::steady_clock> m_chronoStartPoint;
		//! std::chrono::time_point<std::chrono::steady_clock> m_chronoEndPoint represents the end point of a timed interval
		std::chrono::time_point<std::chrono::steady_clock> m_chronoEndPoint;
		/*!
		 * std::chrono::duration<float, std::ratio<1>> m_chronoDelta represents the delta time of the interval (length of / elapsed time),
		 * float is the data type of the output, std::ratio<1> simply represents a second, in example std::ratio<60> would be a minute, 
		 * and instead for smaller time fragments such as milliseconds or nanoseconds are represented with std::milli or std::nano
		 */
		std::chrono::duration<float, std::ratio<1>> m_chronoDelta;
	};
}