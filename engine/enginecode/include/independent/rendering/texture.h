/*! 
 * \file texture.h 
 * \brief api agnostic texture rendering base class header file
 */

#pragma once

#include <cstdint>
#include <glm/glm.hpp>

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine
{
	/*!
	 * \class Texture
	 * \brief Base class to construct and destruct Texture objects, containing the functionality to construct a texture object loaded in from a file,
	 * bind said texture to a model, and destory the texture object when implmented in derived API specific classes. 
	 */
	class Texture 
	{
	public:
		//! Destructor for the OpenGLTexture object to destroy it when it is no longer needed
		virtual ~Texture() = default;
		/*!
		 * \param xOffset
		 * is a 32 bit fixed width unsigned integer indicating stride position to change with the width param to edit the x-axis
		 *
		 * \param yOffset
		 * is a 32 bit fixed width unsigned integer indicating stride position to change with the width param to edit the x-axis
		 *
		 * \param width
		 * is a 32 bit fixed width unsigned integer representing the width of the texture to be created
		 *
		 * \param height
		 * is a 32 bit fixed width unsigned integer representing the height of the texture to be created
		 *
		 * \param channels
		 * is a 32 bit fixed width unsigned integer representing the count of the (rgb(a)) channels
		 *
		 * \param data
		 * is a const char pointer to a string of characters representing the file path where the texture image file can be located
		 *
		 * Edit function to edit a derived Texture object after construction, where the attributes are 32 bit fixed width unsigned integers representing
		 * x-axis offset, y-axis offset, width, height, (rgb(a))channel count, and an unsigned char pointer representing the data (can be thought of
		 * as a pointer of bytes)
		 */
		virtual void Edit(uint32_t xOffset, uint32_t yOffset, uint32_t width, uint32_t height, unsigned char* data) = 0;
		
		//! Accessor function GetTextureID to obtain the uint32_t identifier member variable of the derived class
		virtual	inline uint32_t GetTextureID() = 0;
		//! Accessor function GetWidth to obtain the uint32_t member variable of the derived class storing the width of the texture
		virtual inline uint32_t GetWidth() = 0;
		//! Accessor function GetHeight to obtain the uint32_t member variable of the derived class storing the height of the texture
		virtual inline uint32_t GetHeight() = 0;
		//! Accessor function GetWidth to obtain the float member variable of the derived class storing the width of the texture
		virtual inline float GetWidthf() = 0;
		//! Accessor function GetHeight to obtain the float member variable of the derived class storing the height of the texture
		virtual inline float GetHeightf() = 0;
		//! Accessor function GetChannels to obtain the uint32_t identifier member variable of the derived class storing the channel count of the texture
		virtual inline uint32_t GetChannels() = 0;


		/*!
		 * \param filePath
		 * is a const char pointer to a string of characters representing the file path where the texture image file can be located
		 *
		 * Used by derived classes to create a texture loaded from an image file at the location indicated by the file path char pointer string passed to
		 * the constructor
		 */
		static Texture* CreateTexture(const char* filePath);

		/*!
		 * \param width
		 * is a 32 bit fixed width unsigned integer representing the width of the texture to be created
		 *
		 * \param height
		 * is a 32 bit fixed width unsigned integer representing the height of the texture to be created
		 *
		 * \param channels
		 * is a 32 bit fixed width unsigned integer representing the count of the (rgb(a)) channels
		 *
		 * \param data
		 * is a const char pointer to a string of characters representing the file path where the texture image file can be located
		 *
		 * Used by derived classes to create a texture loaded from data, where the attributes are 32 bit fixed width unsigned integers representing
		 * width, height, (rgb(a))channel count, and an unsigned char pointer representing the data (can be thought of as a pointer of bytes)
		 */
		static Texture* CreateTexture(uint32_t width, uint32_t height, uint32_t channels, unsigned char* data);

	protected:
		/*!
		 * \param width
		 * is a 32 bit fixed width unsigned integers representing the width of the texture to be created
		 *
		 * \param height
		 * is a 32 bit fixed width unsigned integers representing the height of the texture to be created
		 *
		 * \param channels
		 * is a 32 bit fixed width unsigned integers representing the count of the (rgb(a)) channels
		 *
		 * \param data
		 * is a const char pointer to a string of characters representing the file path where the texture image file can be located
		 *
		 * Edit function used by derived classes to edit texture objects after construction, where the attributes are 32 bit fixed width unsigned integers representing
		 * width, height, (rgb(a))channel count, and an unsigned char pointer representing the data (can be thought of as a pointer of bytes)
		 */
		virtual void Init(uint32_t width, uint32_t height, uint32_t channels, unsigned char* data) = 0;
	};
}