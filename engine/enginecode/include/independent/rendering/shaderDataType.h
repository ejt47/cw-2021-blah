/*! 
 * \file shaderDataType.h 
 * \brief ShaderDataType header file, containing bare bones information required to know about a ShaderDataType
 */
#pragma once

#include <cstdint>

 /*!
  * \namespace Engine
  * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
  * every defintion and declaration. Keeps the codebase looking more organised and tidy.
  */

namespace Engine
{
	// For now, using floats, will need to add other data types most likely
	enum class ShaderDataType
	{
		None = 0, 
		Integer, Integer2, Integer3, Integer4, 
		Float, Float2, Float3, Float4, 
		Mat3, Mat4, 
		Boolean, 
		Sampler2D
	};

	/*!
	 * \namespace SDT
	 * Provides SDT(ShaderDataType) namespace for functions that make up the ShaderDataType (might need expanding, current examples given, size,
	 * opengl type(don't actually want to do that in this file if aiming for an API-Agostic rendering API), component count)
	 */

	namespace SDT
	{
		/*
			Array of fixed width unsigned 32 bit integers, with 4 elements, where the data held is in reference to the size of 1, 2, 3, and 4
			float variables. Pre-allocated array is appropriate given the static nature of the function.
		*/
		//uint32_t m_ui32Size[5] = {0, 4, 8, 12, 16 };

		/*!
		 * \param dataType
		 * is a ShaderDataType enum class value, representing a ShaderDataType.
		 *
		 * Static function Size returns a fixed width 32bit unsigned integer indicating the size of the passed ShaderDataType type by accessing an
		 * indexed location in a pre-allocated array holding the known size values.
		 */
		static uint32_t Size(ShaderDataType dataType)
		{
			// Simon does say that it is probably faster and better(especially as this is a static function), to prepopulate an array holding
			// the individual ShaderDataType to be accessed by index, but implements a switch statement
			switch (dataType)
			{
			// where size of a float is 4 bytes
			case ShaderDataType::Integer : return 4;
			// where size of a float is 8 bytes
			case ShaderDataType::Integer2 : return 8;
			// where size of a float is 12 bytes
			case ShaderDataType::Integer3 : return 12;
			// where size of a float is 16 bytes
			case ShaderDataType::Integer4 : return 16;
			// where size of a float is 4 bytes
			case ShaderDataType::Float : return 4;
			// where size of two floats is 8 bytes
			case ShaderDataType::Float2 : return 8;
			// where size of three floats is 12 bytes
			case ShaderDataType::Float3 : return 12;
			// where size of four floats is 16 bytes
			case ShaderDataType::Float4 : return 16;
			// where size of a Mat3 is 36 bytes
			case ShaderDataType::Mat3: return 36;
			// where size of a Mat4 is 64 bytes
			case ShaderDataType::Mat4: return 64;
			// where size of a Boolean is 1 byte
			case ShaderDataType::Boolean : return 1;
			// where size of a Sampler2D is 1 byte
			case ShaderDataType::Sampler2D : return 1;
			}

			// Returns the value at the index position of the m_ui32Size array using the passed ShaderDataType dataType, when static cast to an integer
			//return m_ui32Size[static_cast<int>(dataType)];
		}

		/*
			Array of fixed width unsigned 32 bit integers, with 4 elements, where the data held is in reference to the count of 1, 2, 3, and 4
			float variables. Pre-allocated array is appropriate given the static nature of the function.
		*/
		//uint32_t m_ui32Count[5] = {0, 1, 2, 3, 4 };

		/*!
		 * \param dataType
		 * is a ShaderDataType enum class value, representing a ShaderDataType.
		 *
		 * Static function ComponentCount returns a fixed width 32bit unsigned integer indicating the component count of the passed ShaderDataType 
		 * dataType by accessing an indexed location in a pre-allocated array holding the known component count values.
		 */
		static uint32_t ComponentCount(ShaderDataType dataType)
		{
			switch (dataType)
			{
			// where there is one Integer
			case ShaderDataType::Integer: return 1;
			// where there is two Integers
			case ShaderDataType::Integer2: return 2;
			// where there is three Integers
			case ShaderDataType::Integer3: return 3;
			// where there is four Integers
			case ShaderDataType::Integer4: return 4;
			// where there is one Float
			case ShaderDataType::Float: return 1;
			// where there is two Floats
			case ShaderDataType::Float2: return 2;
			// where there is three Floats
			case ShaderDataType::Float3: return 3;
			// where there is four Floats
			case ShaderDataType::Float4: return 4;
			// where there is one 3 by 3 Matrix
			case ShaderDataType::Mat3: return 9;
			// where there is one 4 by 4 Matrix
			case ShaderDataType::Mat4: return 16;
			// where there is one Boolean 
			case ShaderDataType::Boolean: return 1;
			// where there is one 2D Sample
			case ShaderDataType::Sampler2D: return 1;
			}

			//Returns the value at the index position of the m_ui32Count array using the passed ShaderDataType dataType, when static cast to an integer
			//return m_ui32Count[static_cast<int>(dataType)];
		}
		//BELOW FUNCTION MOVED TO OPENGLVERTEXARRAY CLASS, RRENDER ABSTRACTION DOES NOT HAVE OPENGL ACCESS AT THIS STAGE IN CONSTRUCTION PIPELINE
		//static GLenum toGLType(ShaderDataType dataType)
		//{
		//	//if (ui32GLenum[static_cast<int>(dataType)] < 1 | ui32GLenum[static_cast<int>(dataType)] > 4) return GL_INVALID_ENUM;
		//	if (SDT::m_ui32Count[static_cast<int>(dataType)] < 1 | SDT::m_ui32Count[static_cast<int>(dataType)] > 4) return GL_INVALID_ENUM;
		//	else return GL_FLOAT;
		//}
		//static GLenum toGLType(ShaderDataType dataType)
		//{
		//	//if (ui32GLenum[static_cast<int>(dataType)] < 1 | ui32GLenum[static_cast<int>(dataType)] > 4) return GL_INVALID_ENUM;
		//	//if (SDT::m_ui32Count[static_cast<int>(dataType)] < 1 | SDT::m_ui32Count[static_cast<int>(dataType)] > 4) return GL_INVALID_ENUM;
		//	//else return GL_FLOAT;
		//	// Switch to classify float types as GL_FLOATs
		//	switch (dataType)
		//	{
		//	// ShaderDataType::Integer returns a GL_INT
		//	case ShaderDataType::Integer : return GL_INT;
		//	// ShaderDataType::Integer2 returns a GL_INT
		//	case ShaderDataType::Integer2 : return GL_INT;
		//	// ShaderDataType::Integer3 returns a GL_INT
		//	case ShaderDataType::Integer3 : return GL_INT;
		//	// ShaderDataType::Integer4 returns a GL_INT
		//	case ShaderDataType::Integer4 : return GL_INT;
		//	// ShaderDataType::Float returns a GL_FLOAT
		//	case ShaderDataType::Float : return GL_FLOAT;
		//	// ShaderDataType::Float2 returns a GL_FLOAT
		//	case ShaderDataType::Float2 : return GL_FLOAT;
		//	// ShaderDataType::Float3 returns a GL_FLOAT
		//	case ShaderDataType::Float3 : return GL_FLOAT;
		//	// ShaderDataType::Float4 returns a GL_FLOAT
		//	case ShaderDataType::Float4 : return GL_FLOAT;
		//	// ShaderDataType::Mat3 returns a 
		//	case ShaderDataType::Mat3: return 9;
		//	// where there is one 4 by 4 Matrix
		//	case ShaderDataType::Mat4: return 16;
		//	// where there is one Boolean 
		//	case ShaderDataType::Boolean: return 1;
		//	// where there is one 2D Sampler
		//	case ShaderDataType::Sampler2D: return 1;
		//	default: return GL_INVALID_ENUM;
		//	}
		//}

	}
}