/*!
 *\file vertexArray.h
 *\brief Vertex Array API agnostic abstraction
 */

#pragma once

#include <cstdint>
#include <memory>

#include "vertexBuffer.h"
#include "indexBuffer.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine 
{
	/*!
	 * \class VertexArray
	 * \brief API agnostic base class to encapsulate the shared attributes of any VertexArray object (although from what I understand VAOs are only OpenGL)
	 */
	class VertexArray
	{
	public:
		/*!
		 * Virtual VertexArray destructor initialised to the default destructor, forces compiler to generate a destructor special member function so that derived classes
		 * can be destroyed properly in case of triviality
		 */
		virtual ~VertexArray() = default;

		/*!
		 * \param vertexBuffer
		 * is a const reference to a VertexBuffer shared pointer, used to add the vertex attributes to the Vertex Buffer Object
		 *
		 * Virtual function AddVertexBuffer is to be used by derived classes to add the vertex attributes used to organise vertices
		 */
		virtual void AddVertexBuffer(const std::shared_ptr<VertexBuffer>& vertexBuffer) = 0;

		/*!
		 * \param indexBuffer
		 * is a const reference to a IndexBuffer shared pointer, 
		 * 
		 * Virtual void function used by derived classes to keep track of the draw count between indices held in an Index Buffer Object 
		 * in the function GetDrawCount
		 */
		 //void SetIndexBuffer(const std::shared_ptr<OpenGLIndexBuffer>& indexBuffer);
		 //api agnostic imp
		virtual void SetIndexBuffer(const std::shared_ptr<IndexBuffer>& indexBuffer) = 0;

		//! Virtual inline function to get the member IndexBuffer shared pointer to the initialised IndexBuffer
		virtual std::shared_ptr<IndexBuffer> GetIndexBuffer() const = 0;

		//! Virtual inline function to get the member vector of shared pointer to the added VertexBuffer objects
		virtual inline std::vector<std::shared_ptr<VertexBuffer>> GetVertexBuffer() const = 0;

		/*!
		 * Virtual inline function GetRenderingID is used by derived classes to return with constness the member uint32_t m_ui32OpenGL_ID used to 
		 * identify the Vertex Array Object
		 */
		virtual inline uint32_t GetRenderingID() const = 0;

		/*!
		 * Virtual inline function GetDrawCount, inline because small enough function. Used by derived classes to keep track of the current draw count
		 * between indices accessed via the pointer to the Index Buffer Object m_pIndexBuffer
		*/
		virtual inline uint32_t GetDrawCount() const = 0;

		/*!
		 * Virtual function that in dervied classes is used to Instantiate at run time the external static instance of the instantiated and initalised Vertex
		 * Array Object created by derived classes
		 */
		static VertexArray* CreateVertexArray();
	};
}