/*!
 * \file renderInterface.h
 * \brief renderInterface header
 */

#pragma once

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	/*!
	 * \class RenderInterface
	 * \brief RenderInterface class used to identify the graphics API in use and create graphics requirements as needed externally, at run time
	 */
	class RenderInterface
	{
	public:
		//! Enum class GraphicsLib can identify no graphics context, OpenGL, and the unimplemented Direct3D and Vulkan contexts
		enum class GraphicsLib {None = 0, OpenGL = 1, Direct3D = 2, Vulkan = 3};
		//! Inline sttaic function to access the information held by sm_GraphicsLib used to identify the current graphics context
		inline static GraphicsLib GetLib() { return sm_GraphicsLib; };
	private:
		//! Static member object sm_GraphicsLib holds the identifying information indicative of the graphics API in use
		static GraphicsLib sm_GraphicsLib;
	};
}
