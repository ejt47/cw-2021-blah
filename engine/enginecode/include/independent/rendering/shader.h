/*!
 *\file shader.h
 *\brief shader api agnostic abstraction base class header file
 */

#pragma once

#include <cstdint>
#include <glm/glm.hpp>

namespace Engine
{
	class Shader
	{
	public:
		/*! 
		 * Virtual Shader destructor initialised to the default destructor, forces compiler to generate a destructor special member function so that derived classes 
		 * can be destroyed properly 
		 */
		virtual ~Shader() = default;

		//! GetShaderID is a virtual accessor function for any derived member fixed width 32 bit integer used to identify a shader held on the gpu
		virtual uint32_t GetShaderID() const = 0;

		/*!
		 *\param name
		 * is a const char* that in derived classes holds the name string of the uniform integer veriable to be uploaded to in the shader program (string, not worth the
		 * extra operations to use std::string)
		 *\param value
		 * is an integer that in derived classes represents the value to be uploaded to the named uniform integer variable in the shader program
		 *
		 * Virtual base class function, when defined in a derived class is used to upload an integer to a named uniform variable in the shader program
		 */
		virtual void uploadInt(const char* name, int value) = 0;
		/*!
		 *\param name
		 * is a const char* that in derived classes holds the name string of the uniform float variable to be uploaded to in the shader program (string, not worth the
		 * extra operations to use std::string)
		 *\param value
		 * is a float that in derived classes represents the value to be uploaded to the named uniform float variable in the shader program
		 *
		 * Virtual base class function, when defined in a derived class is used to upload a float to a named uniform variable in the shader program
		 */
		virtual void uploadFloat(const char* name, float value) = 0;
		/*!
		 *\param name
		 * is a const char* that in derived classes holds the name string of the uniform 2d float vector variable to be uploaded to in the shader program 
		 * (string, not worth the extra operations to use std::string)
		 *\param value
		 * is an glm::vec2 that in derived classes represents the value to be uploaded to the named uniform 2d float vector variable in the shader program
		 *
		 * Virtual base class function, when defined in a derived class is used to upload a glm::vec2 2d float vector to a named uniform 2d float vector 
		 * variable in the shader program
		 */
		virtual void uploadFloat2(const char* name, glm::vec2& value) = 0;
		/*!
		 *\param name
		 * is a const char* that in derived classes holds the name string of the uniform 3d float vector variable to be uploaded to in the shader program 
		 * (string, not worth the extra operations to use std::string)
		 *\param value
		 * is a glm::vec3 that in derived classes represents the value to be uploaded to the named uniform 3d float vector variable in the shader program
		 *
		 * Virtual base class function, when defined in a derived class is used to upload a glm::vec3 3d float vector to a named uniform 3d float vector 
		 * variable in the shader program
		 */
		virtual void uploadFloat3(const char* name, glm::vec3& value) = 0;
		/*!
		 *\param name
		 * is a const char* that in derived classes holds the name string of the uniform 4d float vector variable to be uploaded to in the shader program 
		 * (string, not worth the extra operations to use std::string)
		 *\param value
		 * is an glm::vec4 that in derived classes represents the value to be uploaded to the named uniform 4d float vector variable in the shader program
		 *
		 * Virtual base class function, when defined in a derived class is used upload an glm::vec4 4d float vector to a named uniform 4d float vector variable 
		 * in the shader program
		 */
		virtual void uploadFloat4(const char* name, glm::vec4& value) = 0;

		/*!
		 *\param name
		 * is a const char* that in derived classes holds the name string of the uniform 4d matrix variable to be uploaded to in the shader program (string, not
		 * worth the extra operations to use std::string)
		 *\param value
		 * is a glm::mat4 that in derived classes represents the value to be uploaded to the named uniform 4d float vector variable in the shader program
		 *
		 * Virtual base class function, when defined in a derived class is used upload a glm::mat4 4d matrix to a named uniform 4d matrix variable in the shader 
		 * program
		 */
		virtual void uploadMat4(const char* name, glm::mat4& value) = 0;

		/*!
		 *\param vertexFP
		 * is a const char* that in derived classes points to the file path where the vertex shader to be located (string, not worth the extra operations to use std::string)
		 *\param fragmentFP
		 * is a const char* that in derived classes points to the fragment shader to be located (string, not worth the extra operations to use std::string)
		 *
		 * This static function is used to define derived Shader objects out of one or more files containing shader code (currently has the functionality for two)
		 * when given a string of chars through a const char*, can easily enough be expanded to include the other types of shaders such as geometry shaders, definition
		 * carried out in the renderInterface header file, this overloaded function takes a file path to multiple shader files, currently implementing vertex and fragment
		 */
		static Shader* CreateShader(const char* vertexFP, const char* fragmentFP);

		/*!
		 *\param filePath
		 * is a const char* is a const char* that in derived classes points to the file path where the the shader file holding required shader source can be located
		 *
		 * This static function is used to define derived Shader objects out of one files  containing all of the required shader code (currently has the functionality for two)
		 * when given a string of chars through a const char*, can easily enough be expanded to include the other types of shaders such as geometry shaders, definition
		 * carried out in the renderInterface header file
		 */
		static Shader* CreateShader(const char* filePath);

	protected:
		/*!
		 *\param vertexSource
		 * is a const char* that is used in derived claases to hold the name string of the string variable holding the shader program source code read in from the vertex shader
		 * source file (string, not worth the extra operations to use std::string)
		 *\param fragmentSource
		 * is a const char* that is used in derived claases to hold the name string of the string variable holding the shader program source code read in from the fragment shader
		 * source file (string, not worth the extra operations to use std::string)
		 *
		 * Virtual function that in dervied classes is used to compile and links the shader source code read in from a file into a string for usage on the gpu as exectuable code, 
		 * currently supports vertex and fragment shaders only.
		 */
		virtual void CompileAndLink(const char* vertexSource, const char* fragmentSource) = 0;
	};
}