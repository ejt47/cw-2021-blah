/*! 
 *\file subTexture.h
 *\brief SubTexture class header file for texture packing via an atlas
 */

#pragma once

#include <memory>
#include <glm/glm.hpp>

#include "texture.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine 
{
	/*!
	 * \class SubTexture
	 * \brief Base class to construct and destruct SubTexture objects, containing the functionality to construct multiple texture object loaded in from a single file,
	 * bind said textures to models, and destory the texture objects. Currently only implementation, not sure if there is a point in makin ga derived class, if not planning
	 * to implement further than 2 dimensional sub textures? SHOULD THIS BE DERIVED FROM TEXTURE CLASS?? PROBABLY BETTER AS ITS OWN BASE CLASS?? HMMMMM SHOULD OPENGLTEXTURE
	 * BE MULTIPLY INHERITANCED DERIVED?? TODO: MAKE THIS DECISION SOONER RATHER THAN LATER, IMPLEMENT FIRST THEN REFACTOR BEST BET
	 */
	class SubTexture
	{
	public:
		//! SubTexture class constructor
		SubTexture() {};
		/*! 
		 * \param texture
		 * is a const reference to a shared pointer to a polymorphic Texture object
		 * \param UVStart
		 * is a const reference to a glm::vec2 object representative of the first in the sequence of UV texture mapping coordinates.
		 * \param UVEnd
		 * is a cosnt reference to a glm::vec2 object representative of the last in a sequence of UV texture mapping coordinates.
		 *
		 * Overloaded SubTexture class constructor is passed the above described arguments to construct sub textures out of a larger texture atlas image
		 */
		SubTexture(const std::shared_ptr<Texture>& texture, const glm::vec2& UVStart, const glm::vec2& UVEnd);
		//! Inline function returning the glm::vec2 member object giving the beginning of the sub texture in terms of texture coordinates of the larger texture atlas image
		inline glm::vec2 GetUVStart() { return m_UVStart; };
		//! Inline function returning the glm::vec2 member object giving the end of the sub texture in terms of texture coordinates of the larger texture atlas image
		inline glm::vec2 GetUVEnd() { return m_UVEnd; };
		//! Function returning the member glm::ivec2 (integer component vector2d) object of the sub texture in terms of space taken up of the larger texture atlas image
		glm::ivec2 GetSize() { return m_Size; };
		/*!
		 * Function returning the glm::vec2 (float component vector2d) object of the sub texture in terms of space taken up of the larger texture atlas image, comprised
		 * of the two components of the member integer vector 2d when cast statically to floats
		 */
		glm::vec2 GetSizef() { static_cast<float>((m_Size.x), static_cast<float>(m_Size.y)); };
		//! 
		float GetTransformCoord_U(float U);
		//!
		float GetTransformCoord_V(float V);
		//!
		glm::vec2 GetTransformCoordsUV(glm::vec2 UV);
	private:
		//! Member shared pointer toa polymorphic texture image ms_pTexture is used to point to the come back to this after checking notes, fairly sure it points to the texture atlas???
		std::shared_ptr<Texture> ms_pTexture;
		//! Member glm::vec2 object representative of the start of the secuence of texture coordinates descriptive of the sub texture in the texture atlas image
		glm::vec2 m_UVStart;
		//! Member glm::vec2 object representative of the end of the secuence of texture coordinates descriptive of the sub texture in the texture atlas image
		glm::vec2 m_UVEnd;
		//! Member glm::ivec2 (integer component vector2d) object of the sub texture describing in terms of integers the space on the larger texture atlas image
		glm::ivec2 m_Size; 
	};
}