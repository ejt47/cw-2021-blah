/*!
 *\file vertexBuffer.h
 *\brief Api agnostic interface for the vertex buffers
 */

#pragma once

#include <cstdint>

#include "rendering/bufferLayout.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine
{
	/*!
	 * \class VertexBuffer
	 * \brief VertexBuffer class to encapsulate the functionality to be implemented for construction, binding, editing and destruction of VertexBuffers,
	 * derived from the VetexBuffer base class.
	 */
	class VertexBuffer
	{
	public:
		/*! 
	 	 * Virtual destructor for the VertexBuffer API agnostic base class, initialised to the default destructor, forces compiler to generate a destructor
		 * special member function so that derived classes can be destroyed properly
		 */
		virtual ~VertexBuffer() = default;

		/*!
		 * \param vertices
		 * is a void pointer to the new edited vertices data stored in an array to be added to the derivd buffer object
		 * \param size
		 * is a uint32_t holding the new edited size to be added to the derived buffer object
		 * \param offset
		 * is a uint32_t holding the new edited offset to be added to the buffer
		 *
		 * Edit function to edit the data held in a derived Buffer object
		 */
		virtual void Edit(void* vertices, uint32_t size, uint32_t offset) const = 0;

		//! Inline function GetRenderingID returns the member uint32_t used as the identifier on the gpu for the derived buffer object
		virtual inline uint32_t GetRenderingID() const = 0;
		//! Inline function GetLayout returns a const reference to the member BufferLayout object associated to the derived buffer object
		virtual inline BufferLayout& GetLayout() /*const*/ = 0;

		/*!
		 * \param vertices
		 * is a void pointer to vertices data stored in an array to be added to the derived buffer object upon creation
		 * \param size
		 * is a uint32_t holding the size of the buffer to be created upon creation of the derived vertex buffer object
		 * \param layout
		 * is a BufferLayout object holding some of the basic information and functions needed to fill the derived buffer object
		 *
		 * Static function used by derived buffer objects in order to instantiate and initialise them, carried out at external scope by the renderInterface.
		 */
		static VertexBuffer* CreateVertexBuffer(void* vertices, uint32_t size, BufferLayout layout);
	};
}