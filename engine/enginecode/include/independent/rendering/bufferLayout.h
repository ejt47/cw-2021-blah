/*!
 * \file bufferLayout.h
 * \brief BufferLAyout header file containing a struct defining the contents of a single BufferElement object, and a class defining the requisite
 * information for a BufferLayout class.
 */

#pragma once

#include "shaderDataType.h"

#include <vector>

 /*!
  * \namespace Engine
  * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
  * every defintion and declaration. Keeps the codebase looking more organised and tidy.
  */

namespace Engine
{
	/*!
	 * \struct BufferLayout
	 * \brief A struct which holds data of a single element in the buffer layout
	 */
	struct BufferElement
	{
	public:
		//! ShaderDataType member object m_dataType to store the shader data type of the element
		ShaderDataType m_dataType;
		//! Member fixed width 32 bit unsigned integer m_ui32Size to hold the size of m_dataType
		uint32_t m_ui32Size;
		//! Member fixed width 32 bit unsigned integer m_ui32Offset to hold the starting position of value type in buffer array row(i.e position / colour)
		uint32_t m_ui32Offset;
		// Member boolean variable m_bNormalised, flag indicating whether data values are normalised
		bool m_bNormalised;

		//! Default constructor
		BufferElement() {};
		/*!
		 * \param dataType
		 * is a ShaderDataType object, indicating the data type of the BufferElement object
		 * \param normalised
		 * is a bool indicating whether or not the data values held by the buffer element are normalised
		 *
		 * Constructor intialising members through an intialiser list 
		 */
		BufferElement(ShaderDataType dataType, bool normalised = false) : 
			m_dataType(dataType),
			m_ui32Size(SDT::Size(dataType)),
			m_ui32Offset(0),
			m_bNormalised(normalised)
		{};
	};
	
	/*!
	 * \class BufferLayout
	 * \brief BufferLayout class to abstract the layout of a buffer
	 */
	class BufferLayout
	{
	public:
		//! Default constructor
		BufferLayout() {};
		/*!
		 * \param element
		 * is a const reference to a BufferElement object intialiser list
		 *
		 * Constructor intialises member vector of BufferElement objects through an intialiser list, function body contains a call to StrideAndOffset 
		 * to calculate the stride and the offset values of the BufferLayout object based on the BufferElement objects contained in the passed 
		 * intialiser list.
		 */
		BufferLayout(const std::initializer_list<BufferElement>& element) : m_bufferElements(element) { StrideAndOffset(); };
		//! Inline function returns the member unsigned 32 bit fixed width integer m_ui32Stride
		inline const uint32_t GetStride() const { return m_ui32Stride; };
		/*!
		 * \param element
		 * is a BufferElement object that is to be added to the member vector m_bufferElements.
		 *
		 * AddElement function adds the passed BufferElement onto the end of the m_bufferElements vector.
		 */
		void AddElement(BufferElement element)
		{
			// Call push_back on the member vector m_bufferElements to add the BufferElement to the vector data structure
			m_bufferElements.push_back(element);
			// Calculate the stride and offset of the BufferElement object in terms of the BufferLayout object
			StrideAndOffset();
		};

		//ALL OF THESE ITERATOR BEGIN AND END FUNCTIONS MUST NOT START CAPITALISED OTHERWISE THEY CANNOT BE USED IN THE INTENDED WAY!!!
		//! Inline function returning an iterator to the beginning of the m_bufferElements vector 
		inline std::vector<BufferElement>::iterator begin() { return m_bufferElements.begin(); };
		//! Inline function returning an iterator to the end of the m_bufferElements vector 
		inline std::vector<BufferElement>::iterator end() { return m_bufferElements.end(); };
		//! Inline function returning a const iterator to the beginning of the m_bufferElements vector
		inline std::vector<BufferElement>::const_iterator begin() const { return m_bufferElements.begin(); };
		//! Inline function returning a const iterator to the end of the m_bufferElements vector
		inline std::vector<BufferElement>::const_iterator end() const { return m_bufferElements.end(); };

	private:
		//! Private member vector m_bufferElements containing BufferElement objects
		std::vector<BufferElement> m_bufferElements;
		//! Private unsigned fixed width 32 bit integer member variable
		uint32_t m_ui32Stride;
		/*! 
		 *	Function StrideAndOffset calculates the stride and offset of the BufferLayout object, based off the BufferElement objects
		 *	contained in the member vector containing the BufferElement objects m_bufferElements
		 */
		void StrideAndOffset()
		{
			// Local uint32_t uiOffset is initialised as 0
			uint32_t ui32Offset = 0;

			// Range based for loop repeats until it has gone through all of the BufferElement objects in a stride(row of a buffer array)
			for (auto& element : m_bufferElements)
			{
				// The member m_ui32Offset of the current BufferElement is equal to current value of the local ui32Offset
				element.m_ui32Offset = ui32Offset;
				// The local ui32Offset is initialised to be the value returned when the member m_ui32Size of the current element is added to ui32Offset
				ui32Offset += element.m_ui32Size;
			}

			/*
				BufferLayout object member m_ui32Stride is equal to the final value held in the local ui32Offset after the completion of the range base
				for loop.
			*/
			m_ui32Stride = ui32Offset;
		};
	};
}
