/*! 
 *\file indexBuffer.h
 *\brief Api agnostic interface for the index buffers
 */

#pragma once

#include <cstdint>

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine 
{
	/*!
	 * \class IndexBuffer
	 * \brief Base IndexBuffer class to encapsulate the functionality to be implemented for construction, binding, editing and destruction of IndexBuffers that
	 * are to be derived from this IndexBuffer base class.
	 */
	class IndexBuffer 
	{
	public:
		/*!
		 * Virtual IndexBuffer destructor initialised to the default destructor, forces compiler to generate a destructor special member function so that derived classes
		 * can be destroyed properly in case of triviality
		 */
		virtual ~IndexBuffer() = default;

		/*!
		 * Virtual inline function GetRenderingID is used by derived classes to return with constness the member uint32_t  used to
		 * identify derived Index Buffer Object
		 */
		virtual inline uint32_t GetRenderingID() const = 0;
		/*!
		 * Virtual inline uint32_t function returning the count of indices stored in a derived Index Buffer Object
		 */
		virtual inline uint32_t GetCount() const = 0;
		
		/*!
		 * \param indices
		 * is a uint32_T fixed width unsigned integer representing the indices data to be used for indexed rendering
		 * \param count
		 * is a fixed width 32 bit unsigned integer representing the count of the total indices 
		 *
		 * Virtual function that in dervied classes is used to Instantiate at run time the external static instance of the instantiated and initalised Index Buffer
		 * Object created by derived classes
		 */
		static IndexBuffer* CreateIndexBuffer(uint32_t* indices, uint32_t count);

	};
}