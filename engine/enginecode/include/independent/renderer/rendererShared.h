/*!
 * \file rendererShared.h
 * \brief Header file holding the shared characteristics between Renderer3D and Renderer2D classes
 */

#include <glm/glm.hpp>
#include <unordered_map>
#include <memory>

#include "rendering/shaderDataType.h"
#include "rendering/texture.h"
#include "rendering/shader.h"
#include "rendering/VertexArray.h"

#pragma once

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine
{
	/*!
	 * Using directive for a SceneWideUniform, declared to be an unordererd map, const char pointer key, that identifies a
	 * paired ShaderDataType and a void pointer
	 */
	using SceneWideUniform = std::unordered_map<const char*, std::pair<ShaderDataType, void*>>;
};
