/*!
 * \file renderer2D.h
 * \brief Renderer2D class header file declaring the funcitonality required of the Renderer2D class
 */

#pragma once

//#include <glm/glm.hpp>
//#include <unordered_map>
//#include <memory.h>
//
//#include "rendering/shaderDataType.h"

#include "ft2build.h"
#include <freetype/freetype.h>

#include "renderer/rendererShared.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine
{
	/*!
	 * \class Quad
	 * \brief Quad class is used to create quad primitives for use in 2D rendering.
	 */
	class Quad
	{
	public:
		//! Default constructor for Quad class
		Quad() {};
		//!
		static Quad createHalfExtents(const glm::vec2& centre, const glm::vec2& halfExtents);
		static Quad createHalfExtents(const glm::vec2& centre, float halfExtents);
		static Quad createTopLeftSize(const glm::vec2& topLeft, const glm::vec2& size);
		static Quad createTopLeftSize(const glm::vec2& topLeft, float size);
		static Quad createTopLeftBottomRight(const glm::vec2& topLeft, const glm::vec2& bottomRight);
	private:
		//! Member translation vector, initial translation vector of 0.f in all components gives a default position at the origin of render space(x, y)
		glm::vec3 m_translate = glm::vec3(0.f);
		//! Member scale vector, initial scale factor of 1.f in all components gives a default 1 to 1 size of the vertex array positions
		glm::vec3 m_scale = glm::vec3(1.f);

		//! Name Renderer2D as a friend class to allow easy/dirty access to private and protected members of the Renderer2D instance
		friend class Renderer2D;
	};

	/*!
	 * \class Renderer2D
	 * \brief Renderer2D class is used to render in 2 dimensions using quads
	 */
	class Renderer2D
	{
	public:
		//! Init function used to initialise the Renderer2D instance
		static void Init();
		//! Begin function begins a new 2D rendering scene
		static void Begin(const SceneWideUniform& sceneWideUniform);
		//! Submit function submits quad primitives for rendering with tint only
		static void Submit(const Quad& quad, glm::vec4& tint);
		//! Submit function submits quad primitives for rendering with texture only
		static void Submit(const Quad& quad, std::shared_ptr<Texture>& texture);
		//! Submit function submits quad primitives for rendering with tint and texture
		static void Submit(const Quad& quad, glm::vec4& tint, std::shared_ptr<Texture>& texture);
		//! Submit function submits quad primitives for rendering with tint, and orientation(in degree, func converts to rad)
		static void Submit(const Quad& quad, glm::vec4& tint, float angle);
		//! Submit function submits quad primitives for rendering with texture, and orientation(in degree, func converts to rad)
		static void Submit(const Quad& quad, std::shared_ptr<Texture>& texture, float angle);
		//! Submit function submits quad primitives for rendering with tint, texture, and orientation(in degree, func converts to rad)
		static void Submit(const Quad& quad, glm::vec4& tint, std::shared_ptr<Texture>& texture, float angle);

		//! Submit a text character 'glyph' to be rendered at a position with a coloured tint
		static void Submit(char glyph, const glm::vec2& position, float& advance, const glm::vec4 tint);
		//! Submit a string of 'glyphs' to be rendered at a position with a coloured tint and an advance
		static void Submit(const char* glyphString, const glm::vec2& position, const glm::vec4 tint);

		//! Ends the open render scene
		static void End();
	private:
		/*!
		 * \struct InternalData
		 * \brief InternalData struct to hold the attributes rquired to upload rendering attributes to the shader program used for rendering
		*/
		struct InternalData
		{
			//! Shared pointer to a Texture object pointing to the texture attribute of the InternalData object, initialised with a default white texture
			std::shared_ptr<Texture> defaultTexture;
			//! glm::vec4 holding the tint attribute's value, also used to store the default value if none is set
			glm::vec4 defaultTint;
			//! Shared pointer to a Shader object to load the uniforms to for rendering
			std::shared_ptr<Shader> s_pShader;
			//! Shared pointer to a VertexArray attribute to store the vertex render information, adds vertexbuffer and sets index buffer
			std::shared_ptr<VertexArray> s_pVertexArray;
			//! glm::mat4 attribute to store a model matrix for clip space transform by renderer
			glm::mat4 model;
			//! FT_Library free type library innterface instance
			FT_Library freeTypeLib;
			//! FT_Face free type face object
			FT_Face fontFace;
			//! Shared pointer to a texture to point to the font texture to be used
			std::shared_ptr<Texture> fontTexture;
			//! glm::ivec2 object to hold the dimensions of the glyh buffer
			glm::ivec2 glyphBufferDimensions;
			//! uint32_t unsigned 32bit fixed width integer to store the size of the data withing the glyph buffer
			uint32_t ui32GlyphBufferSize;
			//! InternalData object uint32_t channel count
			//uint32_t ui32Channels;
			//! Shared pointer to an unsigned char for the glyph buffer
			std::shared_ptr<unsigned char> glyphBuffer;
		};

		//! Static shared pointer to an InternalData struct object
		static std::shared_ptr<InternalData> ss_pData;

		static void uPCMonochrome2Alpha(unsigned char* monoBuffer, uint32_t width, uint32_t height);
	};
}