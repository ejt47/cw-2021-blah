/*! 
 *\file renderer3D.h
 *\brief Renderer3D class header file, declaring required functionality for the 3D rederer
 */

#pragma once

#include <glad/glad.h>
//#include <glm/glm.hpp>
//#include <unordered_map>
//#include <memory>


//#include "rendering/texture.h"
//#include "rendering/shader.h"
//#include "rendering/shaderDataType.h"
#include "rendererShared.h"
#include "rendering/renderInterface.h"
//#include "rendering/VertexArray.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine
{
	/*! 
	 * Using directive for a SceneWideUniform, declared to be an unordererd map, const char pointer key, that identifies a 
	 * paired ShaderDataType and a void pointer
	 */
	using SceneWideUniform = std::unordered_map<const char*, std::pair<ShaderDataType, void*>>;

	/*!
	 * \class Material
	 * \brief Material class to define a shader and the data neccessary for a shader 
	 */
	class Material
	{
	public:
		/*!
		 * \param shader
		 * is a const reference to a shared pointer that points to a shader object, to be used in intiailisation of the Material class to assign the shader to be used
		 *
		 * default constructor, sets default values for a material initialised with just a shader(shader is a requisite componenet to init)
		 */
		Material(const std::shared_ptr<Shader>& shader) :
			ms_pShader(shader),
			m_ui32Flags(0),
			ms_pTexture(nullptr),
			m_Tint(glm::vec4(0.f)) {};
		/*!
		 * \param shader
		 * is a const reference to a shared pointer that points to a shader object, to be used in intiailisation of the Material class to assign the shader, with texture and tint to be used
		 *
		 * \param texture 
		 * is a const reference to a shared pointer to a texture object, used to initialise the texture that is to be wrapped to the model object
		 *
		 * \param tint
		 * is a const reference to a glm::vec4 (4d vector) that gives colour bit information in 4 channels to apply to a model as a "tint" colour, oversaturates(right word?) by the tint colour bit values
		 * 
		 * Full constructor that can be passed all currently required items, a shader, a texture, and a tint, sets the category bit flags to indicate texture and tint loaded through a bitwise and operator
		 */
		Material(const std::shared_ptr<Shader>& shader, const std::shared_ptr<Texture>& texture, const glm::vec4& tint) :
			ms_pShader(shader),
			ms_pTexture(texture),
			m_Tint(tint) 
		{
			SetFlag(flagTexture | flagTint);
		};
		/*!
		 * \param shader
		 * is a const reference to a shared pointer that points to a shader object, to be used in intiailisation of the Material class to assign the shader, with texture and tint to be used
		 *
		 * \param texture 
		 * is a const reference to a shared pointer to a texture object, used to initialise the texture that is to be wrapped to the model object
		 * 
		 * Constructor that can be passed  a shader, and a texture, sets the category bit flags to indicate texture aspect has been loaded, but not tint which is set to the default value
		 */
		Material(const std::shared_ptr<Shader>& shader, const std::shared_ptr<Texture>& texture) :
			ms_pShader(shader),
			ms_pTexture(texture),
			m_Tint(glm::vec4(0.f))
		{
			SetFlag(flagTexture);
		};
		/*!
		 * \param shader
		 * is a const reference to a shared pointer that points to a shader object, to be used in intiailisation of the Material class to assign the shader, with texture and tint to be used
		 *
		 * \param tint
		 * is a const reference to a glm::vec4 (4d vector) that gives colour bit information in 4 channels to apply to a model as a "tint" colour, oversaturates(right word?) by the tint colour bit values
		 * 
		 * Constructor that can be passed a shader, and a tint, sets the category bit flags to indicate tint aspect has been loaded, but not texture which is set to the default value
		 */
		Material(const std::shared_ptr<Shader>& shader, const glm::vec4& tint) :
			ms_pShader(shader),
			ms_pTexture(nullptr),
			m_Tint(tint)
		{
			SetFlag(flagTint);
		};

		//! Inline accessor to retriev the shader member shared pointer of the material object
		inline std::shared_ptr<Shader> GetShader() const { return ms_pShader; };
		//! Inline accessor to retriev the texture member shared pointer of the material object
		inline std::shared_ptr<Texture> GetTexture() const { return ms_pTexture; };
		//! Inline accessor to retrieve the tint member glm::vec4 of the material object
		inline glm::vec4 GetTint() const { return m_Tint; };
		/*!
		 * \param flag 
		 * is a uint32_t fixed width 32bit unsigned integer for bitwise comparison against the material object's member uint32_t m_ui32Flags
		 * 
		 * FlagCheck is a function used to compare the passed flag uint32_t against the initialised member flag uint32_t, where the return value
		 * through the bitwise AND operator(1) will only return true if the passed flag is the same as the member flag
		 */
		bool FlagCheck(uint32_t flag) const { return m_ui32Flags & flag; };

		/*!
		 * \param texture
		 * is a const reference to a shared pointer to a Texture object to be wrapped to the Material object
		 *
		 * SetTexture is a function used to initialise the texture attribute of the material object
		 */
		void SetTexture(const std::shared_ptr<Texture>& texture) { ms_pTexture = texture; };
		/*!
		 * \param tint
		 * is a const glm::vec4 used to tint the Material object
		 *
		 * SetTint is a function used to initialise the tint attribute of the Material object
		 */
		void SetTint(const glm::vec4 tint) { m_Tint = tint; };

		//! static constexpr allows for runtime evaluation of the flagTexture category bit (1), can be combined with flagTint to give a combined category bit(3)
		constexpr static uint32_t flagTexture = 1 << 0; // 00000001

		//! static constexpr allows for runtime evaluation of the flagTint category bit (2), can be combined with flagTexture to give a combined category bit(3)
		constexpr static uint32_t flagTint = 1 << 1; // 00000010
		// notes specifically say that simon says "not allowed to flag for shader bcus can query other flags, if a flag no set no shader"
		//TODO WRITE ABOVE MENTIONED QUERY FUNC?

		//! Category bit flag to identify shader settings
		uint32_t m_ui32Flags = 0; 
		//! Shared pointer to the material object's shader
		std::shared_ptr<Shader> ms_pShader;
		//! Shared pointer to the material object's Texture
		std::shared_ptr<Texture> ms_pTexture;
		//! glm::vec4 to hold the material object's tint colour data
		glm::vec4 m_Tint;
		/*!
		 * \param flags
		 * is a uint32_t 32bit unsigned integer used to initialise the bitwise category flag member m_ui32Flags
		 */
		void SetFlag(uint32_t flags) { m_ui32Flags = flags; };

	private:
	}; 

	/*!
	 * \class Renderer3D
	 * \brief Renderer3D class used to construct the 3D renderer used to render objects in 3D
	 * 
	 * NOT API AGNOSTIC, SIMON GIVES REASON BETTER PERFORMANCE, FOR NOW AT LEAST RENDERER ASSUMES USE OF OPENGL, FINE
	 * BY ME AS WAS NOT PLANNING TO IMPLEMENT ANYTHING ELSE
	 */
	class Renderer3D
	{
	public:
		//! Init function used to initialise the Renderer3D instance
		static void Init();
		/*! 
		 * \param sceneWideUniform
		 * is a const reference to a SceneWideUniform struct object
		 *
		 * Begin function begins a new render scene
		 */
		static void Begin(const SceneWideUniform& sceneWideUniform);
		/*!
		 * \param geometry
		 * is a const reference to a shared pointer to a Vertex Array object used to describe the geomtry to be submitted
		 *
		 * \param material
		 * is a const reference to a shared pointer to a Material object(class not yet implemented), gives material of the rendered geometry
		 *
		 * \param model
		 * is a reference to a glm::mat4, a model matrix PREVIOUSLY A CONST REF BUT UPLOADMAT4 WOULD NOT EXCEPT WHEN APPLYING MATERIAL UNIFORMS
		 *
		 * Submit function submits the geometry to be rendered in order to associated rendered geometry to a model matrix
		 */
		static void Submit(const std::shared_ptr<VertexArray>& geometry, const std::shared_ptr<Material>& material, glm::mat4& model);
		//! End function ends the current render scene
		static void End();
	private:
		struct InternalData
		{
			//! SceneWideUniform type, to give values such as light position or view position
			SceneWideUniform sceneWideUniform;
			//! Shared pointer to a default texture object
			std::shared_ptr<Texture> defaultTexture;
			//! glm::vec4 describing a default tint
			glm::vec4 defaultTint;
		};

		// Static shared pointer to the external InternalData struct object, describing data internal to the renderer
		static std::shared_ptr<InternalData> ss_pData;
	};
};
