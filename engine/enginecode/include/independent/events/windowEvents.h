/*!
 * \file windowEvents.h
 * \brief WindowEvent class header file, to declare the various WindowEvent sub-classes inheriting fom the Events class.
 */

#pragma once

#include "events.h"
#include <glm/glm.hpp>

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	/*!
	 * \class WindowClosed
	 * \brief WindowClosed class inheriting from Events that declares what is necessary for a window closing event.
	 */
	class WindowClosed : public Events
	{
	public:
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case WindowClosed
		 */
		static EventTypes GetStaticEventType() { return EventTypes::WindowClosed; };
		//! Get the event type WindowClosed from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		//! Get the category flag of the event from the EventCategories enum
		virtual inline int32_t GetEventCategory() const override { return WindowEvent; };
	};

	/*!
	 * \class WindowResized
	 * \brief WindowResized class inheriting from Events that declares what is necessary for a window resizing event.
	 */
	class WindowResized : public Events
	{
	public:
		/*!
		 * \param width
		 * is a int32_t, a fixed width 32bit integer, the size by which the window has changed in width. Used to intialise member variable, m_ui32Width via
		 * initialisation list.
		 * \param height
		 * is a int32_t, a fixed width 32bit integer, the size by which the window has changed in height. Used to intialise member variable, m_ui32Height via
		 * initialisation list.
		 *
		 *	Constructor for the WindowResized event, as there is data to be initialised, this is done through an initialisation list, arguments are used to initialise data
		 *	members m_ui32Width and m_ui32Height.
		 */
		WindowResized(int32_t width, int32_t height) :
			m_ui32Width(width),
			m_ui32Height(height) {};
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case WindowResized
		 */
		static EventTypes GetStaticEventType() { return EventTypes::WindowResized; };
		//! Get the event type WindowResized from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		//! Get the category flag of the event from the EventCategories enum
		virtual inline int32_t GetEventCategory() const override { return WindowEvent; };

		//! Accessor function to access m_ui32Width, const keyword required to ensure new resized width value is not modifiable when it is retrieved
		inline uint32_t GetResizedWidth() const { return m_ui32Width; };
		//! Accessor function to access m_ui32Height, const keyword required to ensure new resized height value is not modifiable when it is retrieved
		inline uint32_t GetResizedHeight() const { return m_ui32Height; };
		//! Accessor function to access the new size of the window after resizing as a 2d vector using glm::ivec2 as the return value (integer values returned)
		inline glm::ivec2 GetResizedSize() const { return { m_ui32Width, m_ui32Height }; };
	private:
		//! Fixed width unsigned 32 bit integer to hold the new resized value in the x axis representing width.
		uint32_t m_ui32Width;
		//! Fixed width unsigned 32 bit integer to hold the new resized value in the y axis representing height.
		uint32_t m_ui32Height;
	};

	/*!
	 * \class WindowFocus
	 * \brief WindowFocus class that declares what is necessary for a window being in focus event.
	 */
	class WindowFocus : public Events
	{
	public:
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case WindowFocus
		 */
		static EventTypes GetStaticEventType() { return EventTypes::WindowFocus; };
		//! Get the event type WindowFocus from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		//! Get the category flag of the event from the EventCategories enum
		virtual inline int32_t GetEventCategory() const override { return WindowEvent; };
	};

	/*!
	 * \class WindowLostFocus
	 * \brief WindowLostFocus class that declares what is necessary for a window losing focus event.
	 */
	class WindowLostFocus : public Events
	{
	public:
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case WindowLostFocus
		 */
		static EventTypes GetStaticEventType() { return EventTypes::WindowLostFocus; };
		//! Get the event type WindowLostFocus from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		//! Get the category flag of the event from the EventCategories enum
		virtual inline int32_t GetEventCategory() const override { return WindowEvent; };
	};

	/*!
	 * \class WindowMoved
	 * \brief WindowMoved class that declares what is necessary for a window moving event.
	 */
	class WindowMoved : public Events
	{
	public:
		/*!
		 * \param positionX
		 * is a int32_t, a fixed width 32bit integer, the position of which the window has changed in the x-axis. Used to intialise member variable, m_i32PositionX via
		 * initialisation list.
		 * \param positionY
		 * is a int32_t, a fixed width 32bit integer, the position of which the window has changed in the y-axis. Used to intialise member variable, m_i32PositionY via
		 * initialisation list.
		 *
		 *	Constructor for the WindowMoved event, as there is data to be initialised, this is done through an initialisation list, arguments are used to initialise data
		 *	members m_i32PositionX and m_i32PositionY.
		 */
		WindowMoved(int32_t positionX, int32_t positionY) :
			m_i32PositionX(positionX),
			m_i32PositionY(positionY) {};
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case WindowMoved
		 */
		static EventTypes GetStaticEventType() { return EventTypes::WindowMoved; };
		//! Get the event type WindowMoved from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		//! Get the category flag of the event from the EventCategories enum
		virtual inline int32_t GetEventCategory() const override { return WindowEvent; };

		//! Accessor function to access m_i32PositionX, const keyword required to ensure new X axis position value is not modifiable when it is retrieved
		inline int32_t GetWindowPositionX() const { return m_i32PositionX; };
		//! Accessor function to access m_i32PositionY, const keyword required to ensure new Y axis position value is not modifiable when it is retrieved
		inline int32_t GetWindowPositionY() const { return m_i32PositionY; };
		//! Accessor function to access the new position of the window after moving as a 2d vector using glm::ivec2 as the return value (integer values returned)
		inline glm::ivec2 GetWindowPositionVector() const { return { m_i32PositionX, m_i32PositionY }; };
	private:
		//! Fixed width 32 bit integer to hold the new X position 
		int32_t m_i32PositionX;
		//! Fixed width 32 bit integer to hold the new Y position 
		int32_t m_i32PositionY;
	};
}
