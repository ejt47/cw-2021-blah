/*!
 * \file eventHeaders.h
 * \brief EventHeaders header file to hold the various headers for all the different event sub-classes.
 */

#pragma once

#include "include/platform/GLFW/GLFWCodes.h" // Simon has this wrapped inside another file codes, as these defines can be used cross-platform, whats the point? better to just have one codes.h??
#include "windowEvents.h"
#include "keyEvents.h"
#include "mouseEvents.h"
