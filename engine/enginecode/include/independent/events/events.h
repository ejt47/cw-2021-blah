/*!
 * \file events.h
 * \brief Events class header file, declaring EventTypes enum class, EventCategories enum, and the Events abstract base class.
 */
#pragma once

#include <inttypes.h>

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */


namespace Engine 
{
	/*!
	 * \enum EventTypes
	 * C++ enum class to hold the types of events
	 */

	// enum class disadvantage over enum is cant directly cast to integer, need to do a reinterpret cast, can't perform bitwise operations, more type safe however
	enum class EventTypes
	{
		None = 0,
		WindowClosed, WindowResized, WindowFocus, WindowLostFocus, WindowMoved,
		KeyPressed, KeyReleased, KeyTyped,
		MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
	};

	/*!
	 * \enum EventCategories
	 * c style enum for categorising EventTypes as bool flags, enum is a 32bit integer implementation under the hood, can have up to 32 possible flags, therefore
	 * expandable if needed up to a certain point.
	 */

	enum EventCategories
	{
		None = 0,
		WindowEvent = 1 << 0,	// 00000001
		InputEvent = 1 << 1,	// 00000010
		KeyboardEvent = 1 << 2, // 00000100
		MouseDeltaEvent = 1 << 3, // 00001000
		MouseButtonEvent = 1 << 4 // 00010000
	};

	/*!
	 * \class Events
	 * \brief Events class, not instantiable because of some pure virtual functions, but not a pure interface because some defined functions, an abstract base class.
	 */

	class Events
	{
	public:
		//! Get the event type from the EventTypes enum class
		virtual EventTypes GetEventType() const = 0;
		//! Get the category flag of the event from the EventCategories enum
		virtual int32_t GetEventCategory() const = 0; 
		/*! 
		 * Inline bool function EventHandled, const keyword infers the function is const-safe, allowing const objects and variables to call the function, it needs
		 * this keyword because the returned handled event CANNOT be accidentally modified to a state where the system infers it has not been handled when it has 
		 * been handled. The function returns the m_bEventHandled bool flag, which will be true if the event was handled, and false if not.
		 */
		inline bool EventHandled() const { return m_bEventHandled; };
		/*! 
		 * \param handleEvent
		 * bool handleEvent takes an Event function object wrapped using std::function and std::bind to return a bool.
		 *
		 * Inline function HandleEvent returns void, an Event function object to handle that is wrapped to be treated as a bool, the Event function object being treated as
		 * a bool can then initialise its m_bEventHandled member bool flag to true or false, depending on the logic encapsulating its handling.
		 */
		inline void HandleEvent(bool handleEvent) { m_bEventHandled = handleEvent; };
		/*!
		 * \param categoryToCheck 
		 * int32_t categorytoCheck is passed an EventCategories enum type, which is returned as an int32_t to compare, more than one EventCategories enumerated type
		 * can be passed as the correct category using the bitwise or operator '|'
		 *
		 * Inline bool function CategoryCheck is passed an EventCategories enum type, the expected category of the event being handled, it is compared against the
		 * bit pattern returned on the GetEventCategory function call using the bitwise & operator (NOT A REFERENCE, DON'T BE FOOLED, C++ IS CONFUSING), if the
		 * bit pattern returned via the bitwise & operator is > 0 when converted to decimal(no conversions necessary in code because computers understand binary), 
		 * then the event being treated as a bool will return true, otherwise the returned bit pattern should be 00000000 which is 0 which as a bool = false
		 */
		inline bool CategoryCheck(int32_t categoryToCheck) const { return GetEventCategory() & categoryToCheck; };
	protected:
		//! Bool member flag to infer the event has or hasn't been handled
		bool m_bEventHandled = false;
	};

}
