/*!
 * \file eventHandler.h 
 * \brief EventHandler class header file, class used to handle direct binding of events to specific abstract callback functions.
 */
#pragma once

#include "eventHeaders.h"
#include <functional>

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */
namespace Engine
{
	/*!
	 * \class EventHandler
	 * \brief EventHandler class to directly bind each specific event to a function and handle these events
	 *
	 * Each event requires: A private member function to directly bind behaviour to, public accessor functions to initialise the directly bound behaviour via std::bind
	 * and retrieve the private member function that has had its behaviour bound via std::bind, finally a bool function definining default behaviour.
	 *
	 * Everything is bound to a wrapped function returning a bool because event handling requires a returned value indicating that the event was or was not handled.
	 *
	 * The private member function is a std::function object, which explicitly stores the default behaviour as the target callable function object upon intialisation. 
	 * This will be then initialised with the desired behaviour through the setter function when required.
	 *
	 * The setter function is passed a const std::function object that is used to store a target callable function object as a reference (the event) and wrap it to return a
	 * a bool. This is passed using std::bind, which returns a std::function object, directly bound to the required behaviour, through its parameters. The function then
	 * initialises the private member function, with the passed std::function object storing the directly bound behaviour.
	 *
	 * The getter function returns the private member std::function object, which by definition is wrapped to return a bool value indicating the event has or has not been
	 * handled.
	 *
	 * The default behaviour is expressed as a regular function that returns a bool, which by default is false, indicating the event was not handled.
	 */
	class EventHandler
	{
	public:
		/*! 
		 * \param fn 
		 * const std::function<bool(WindowClosed&)> fn is a const reference to a WindowClosed event wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance 
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnCloseCallBack initialises the private m_onWindowCloseFunction with fn. Returns void.
		 */
		inline void SetOnWindowCloseCallback(const std::function<bool(WindowClosed&)>&fn) { m_onWindowCloseFunction = fn; };
		/*! 
		 * Inline function GetOnWindowCloseCallback returns the private m_onWindowCloseFunction. Returns an std::function object that wraps a reference to a WindowResized event to
		 * return a bool value.
		 */
		inline std::function<bool(WindowClosed&)>& GetOnWindowCloseCallback() { return m_onWindowCloseFunction; };

		/*! 
		 * \param fn 
		 * const std::function<bool(WindowResized&)> fn is a const reference to a WindowResized event object wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance 
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnWindowResizeCallback initialises the private m_onWindowResizeFunction with fn. Returns void.
		 */
		inline void SetOnWindowResizeCallback(const std::function<bool(WindowResized&)>&fn) { m_onWindowResizeFunction = fn; };
		/*!
		 * Inline function GetOnWindowResizeCallback returns the private m_onWindowResizeFunction. Returns an std::function object that wraps a reference to a WindowResized event to
		 * return a bool value.
		 */
		inline std::function<bool(WindowResized&)>& GetOnWindowResizeCallback() { return m_onWindowResizeFunction; };

		/*!
		 * \param fn
		 * const std::function<bool(WindowFocus&)> fn is a const reference to a WindowFocus event object wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnWindowFocusCallback initialises the private m_onWindowFocusFunction with fn. Returns void.
		 */
		inline void SetOnWindowFocusCallback(const std::function<bool(WindowFocus&)>&fn) { m_onWindowFocusFunction = fn; };
		/*!
		 * Inline function GetOnWindowFocusCallback returns the private m_onWindowFocusFunction. Returns an std::function object that wraps a reference to a WindowFocus event to
		 * return a bool value.
		 */
		inline std::function<bool(WindowFocus&)>& GetOnWindowFocusCallback() { return m_onWindowFocusFunction; };

		/*!
		 * \param fn
		 * const std::function<bool(WindowLostFocus&)> fn is a const reference to a WindowLostFocus event object wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnWindowLostFocusCallback initialises the private m_onWindowLostFocusFunction with fn. Returns void.
		 */
		inline void SetOnWindowLostFocusCallback(const std::function<bool(WindowLostFocus&)>&fn) { m_onWindowLostFocusFunction = fn; };
		/*!
		 * Inline function GetOnWindowLostFocusCallback returns the private m_onWindowLostFocusFunction. Returns an std::function object that wraps a reference to a WindowLostFocus 
		 * event to return a bool value.
		 */
		inline std::function<bool(WindowLostFocus&)>& GetOnWindowLostFocusCallback() { return m_onWindowLostFocusFunction; };

		/*!
		 * \param fn
		 * const std::function<bool(WindowMoved&)> fn is a const reference to a WindowMoved event object wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnWindowMoveCallback initialises the private m_onWindowMoveFunction with fn. Returns void.
		 */
		inline void SetOnWindowMoveCallback(const std::function<bool(WindowMoved&)>&fn) { m_onWindowMoveFunction = fn; };
		/*!
		 * Inline function GetOnWindowMoveCallback returns the private m_onWindowMoveFunction. Returns an std::function object that wraps a reference to a WindowMoved event to
		 * return a bool value.
		 */
		inline std::function<bool(WindowMoved&)>& GetOnWindowMoveCallback() { return m_onWindowMoveFunction; };

		/*!
		 * \param fn
		 * const std::function<bool(KeyPressed&)> fn is a const reference to a KeyPressed event object wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnKeyPressCallback initialises the private m_onKeyPressFunction with fn. Returns void.
		 */
		inline void SetOnKeyPressCallback(const std::function<bool(KeyPressed&)>&fn) { m_onKeyPressFunction = fn; };
		/*!
		 * Inline function GetOnKeyPressCallback returns the private m_onKeyPressFunction. Returns an std::function object that wraps a reference to a KeyPressed event to
		 * return a bool value.
		 */
		inline std::function<bool(KeyPressed&)>& GetOnKeyPressCallback() { return m_onKeyPressFunction; };

		/*!
		 * \param fn
		 * const std::function<bool(KeyReleased&)> fn is a const reference to a KeyReleased event object wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnKeyReleaseCallback initialises the private m_onKeyReleaseFunction with fn. Returns void.
		 */
		inline void SetOnKeyReleaseCallback(const std::function<bool(KeyReleased&)>&fn) { m_onKeyReleaseFunction = fn; };
		/*!
		 * Inline function GetOnKeyReleaseCallback returns the private m_onKeyReleaseFunction. Returns an std::function object that wraps a reference to a KeyReleased event to
		 * return a bool value.
		 */
		inline std::function<bool(KeyReleased&)>& GetOnKeyReleaseCallback() { return m_onKeyReleaseFunction; };

		/*!
		 * \param fn
		 * const std::function<bool(KeyTyped&)> fn is a const reference to a KeyTyped event object wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnKeyTypedCallback initialises the private m_onKeyTypedFunction with fn. Returns void.
		 */
		inline void SetOnKeyTypedCallback(const std::function<bool(KeyTyped&)>&fn) { m_onKeyTypedFunction = fn; };
		/*!
		 * Inline function GetOnKeyTypedCallback returns the private m_onKeyTypedFunction. Returns an std::function object that wraps a reference to a KeyTyped event to
		 * return a bool value.
		 */
		inline std::function<bool(KeyTyped&)>& GetOnKeyTypedCallback() { return m_onKeyTypedFunction; };
		
		/*!
		 * \param fn
		 * const std::function<bool(MouseButtonPressed&)> fn is a const reference to a MouseButtonPressed event object wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnMouseButtonPressCallback initialises the private m_onMouseButtonPressFunction with fn. Returns void.
		 */
		inline void SetOnMouseButtonPressCallback(const std::function<bool(MouseButtonPressed&)>&fn) { m_onMouseButtonPressFunction = fn; };
		/*!
		 * Inline function GetOnMouseButtonPressCallback returns the private m_onMouseButtonPressFunction. Returns an std::function object that wraps a reference to a MouseButtonPressed 
		 * event to return a bool value.
		 */
		inline std::function<bool(MouseButtonPressed&)>& GetOnMouseButtonPressCallback() { return m_onMouseButtonPressFunction; };

		/*!
		 * \param fn
		 * const std::function<bool(MouseButtonReleased&)> fn is a const reference to a MouseButtonReleased event object wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnMouseButtonReleaseCallback initialises the private m_onMouseButtonReleaseFunction with fn. Returns void.
		 */
		inline void SetOnMouseButtonReleaseCallback(const std::function<bool(MouseButtonReleased&)>&fn) { m_onMouseButtonReleaseFunction = fn; };
		/*!
		 * Inline function GetOnMouseButtonReleaseCallback returns the private m_onMouseButtonReleaseFunction. Returns an std::function object that wraps a reference to a 
		 * MouseButtonReleased event to return a bool value.
		 */
		inline std::function<bool(MouseButtonReleased&)>& GetOnMouseButtonReleaseCallback() { return m_onMouseButtonReleaseFunction; };

		/*!
		 * \param fn
		 * const std::function<bool(MouseMoved&)> fn is a const reference to a MouseMoved event object wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnMouseMoveCallback initialises the private m_onMouseMoveFunction with fn. Returns void.
		 */
		inline void SetOnMouseMoveCallback(const std::function<bool(MouseMoved&)>&fn) { m_onMouseMoveFunction = fn; };
		/*!
		 * Inline function GetOnMouseMoveCallback returns the private m_onMouseMoveFunction. Returns an std::function object that wraps a reference to a
		 * MouseMoved event to return a bool value.
		 */
		inline std::function<bool(MouseMoved&)>& GetOnMouseMoveCallback() { return m_onMouseMoveFunction; };

		/*!
		 * \param fn
		 * const std::function<bool(MouseScrolled&)> fn is a const reference to a MouseScrolled event object wrapped as a callable function object returning a bool value.
		 * Where in practice it is passed an std::bind function that binds the target callable object(a reference to the callable event function), to the current instance
		 * (using the this pointer), with an argument count of one (using std::placeholders_1(the stored reference to the callable event function)).
		 *
		 * Inline function SetOnMouseScrollCallback initialises the private m_onMouseScrollFunction with fn. Returns void.
		 */
		inline void SetOnMouseScrollCallback(const std::function<bool(MouseScrolled&)>&fn) { m_onMouseScrollFunction = fn; };
		/*!
		 * Inline function GetOnMouseScrollCallback returns the private m_onMouseScrollFunction. Returns an std::function object that wraps a reference to a
		 * MouseMoved event to return a bool value.
		 */
		inline std::function<bool(MouseScrolled&)>& GetOnMouseScrollCallback() { return m_onMouseScrollFunction; };
	private:
		/*! 
		 * Private member m_onWindowCloseFunction is a std::function object wrapping a reference to a WindowClosed event and returning a bool, initialised using an std::bind 
		 * function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer), with an argument
		 * count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(WindowClosed&)> m_onWindowCloseFunction = std::bind(&EventHandler::DefaultOnClose, this, std::placeholders::_1);
		/*! 
		 * \param defaultClose
		 * WindowClosed& defaultClose is a reference to a WindowClosed event object.
		 *
		 * Inline function DefaultOnClose defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnClose(WindowClosed& defaultClose) { return false; };

		/*!
		 * Private member m_onWindowResizeFunction is a std::function object wrapping a reference to a WindowResized event and returning a bool, initialised using an std::bind
		 * function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer), with an argument
		 * count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(WindowResized&)> m_onWindowResizeFunction = std::bind(&EventHandler::DefaultOnResize, this, std::placeholders::_1);
		/*!
		 * \param defaultResize
		 * WindowResized& defaultResize is a reference to a WindowResized event object.
		 *
		 * Inline function DefaultOnResize defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnResize(WindowResized& defaultResize) { return false; };

		/*!
		 * Private member m_onWindowFocusFunction is a std::function object wrapping a reference to a WindowFocus event and returning a bool, initialised using an std::bind
		 * function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer), with an argument
		 * count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(WindowFocus&)> m_onWindowFocusFunction = std::bind(&EventHandler::DefaultOnFocus, this, std::placeholders::_1);
		/*!
		 * \param defaultFocus
		 * WindowFocus& defaultFocus is a reference to a WindowFocus event object.
		 *
		 * Inline function DefaultOnFocus defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnFocus(WindowFocus& defaultFocus) { return false; };

		/*!
		 * Private member m_onWindowLostFocusFunction is a std::function object wrapping a reference to a WindowLostFocus event and returning a bool, initialised using an std::bind
		 * function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer), with an argument
		 * count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(WindowLostFocus&)> m_onWindowLostFocusFunction = std::bind(&EventHandler::DefaultOnLostFocus, this, std::placeholders::_1);
		/*!
		 * \param defaultLostFocus
		 * WindowLostFocus& defaultLostFocus is a reference to a WindowLostFocus event object.
		 *
		 * Inline function DefaultOnLostFocus defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnLostFocus(WindowLostFocus& defaultLostFocus) { return false; };

		/*!
		 * Private member m_onWindowMoveFunction is a std::function object wrapping a reference to a WindowMoved event and returning a bool, initialised using an std::bind
		 * function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer), with an argument
		 * count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(WindowMoved&)> m_onWindowMoveFunction = std::bind(&EventHandler::DefaultOnMove, this, std::placeholders::_1);
		/*!
		 * \param defaultMove
		 * WindowMoved& defaultMove is a reference to a WindowMoved event object.
		 *
		 * Inline function DefaultOnMove defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnMove(WindowMoved& defaultMove) { return false; };

		/*!
		 * Private member m_onKeyPressFunction is a std::function object wrapping a reference to a KeyPressed event and returning a bool, initialised using an std::bind
		 * function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer), with an argument
		 * count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(KeyPressed&)> m_onKeyPressFunction = std::bind(&EventHandler::DefaultOnKeyPress, this, std::placeholders::_1);
		/*!
		 * \param defaultPress
		 * KeyPressed& defaultPress is a reference to a KeyPressed event object.
		 *
		 * Inline function DefaultOnKeyPress defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnKeyPress(KeyPressed& defaultPress) { return false; };

		/*!
		 * Private member m_onKeyReleaseFunction is a std::function object wrapping a reference to a KeyReleased event and returning a bool, initialised using an std::bind
		 * function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer), with an argument
		 * count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(KeyReleased&)> m_onKeyReleaseFunction = std::bind(&EventHandler::DefaultOnKeyRelease, this, std::placeholders::_1);
		/*!
		 * \param defaultRelease
		 * KeyReleased& defaultRelease is a reference to a KeyReleased event object.
		 *
		 * Inline function DefaultOnKeyRelease defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnKeyRelease(KeyReleased& defaultRelease) { return false; };

		/*!
		 * Private member m_onKeyTypedFunction is a std::function object wrapping a reference to a KeyTyped event and returning a bool, initialised using an std::bind
		 * function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer), with an argument
		 * count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(KeyTyped&)> m_onKeyTypedFunction = std::bind(&EventHandler::DefaultOnKeyTyped, this, std::placeholders::_1);
		/*!
		 * \param defaultTyped
		 * KeyTyped& defaultTyped is a reference to a KeyTyped event object.
		 *
		 * Inline function DefaultOnKeyTyped defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnKeyTyped(KeyTyped& defaultTyped) { return false; };

		/*!
		 * Private member m_onMouseButtonPressFunction is a std::function object wrapping a reference to a MouseButtonPressed event and returning a bool, initialised 
		 * using an std::bind function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer), 
		 * with an argument count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(MouseButtonPressed&)> m_onMouseButtonPressFunction = std::bind(&EventHandler::DefaultOnMouseButtonPress, this, std::placeholders::_1);
		/*!
		 * \param defaultPress
		 * MouseButtonPressed& defaultPress is a reference to a MouseButtonPressed event object.
		 *
		 * Inline function DefaultOnMouseButtonPress defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnMouseButtonPress(MouseButtonPressed& defaultPress) { return false; };

		/*!
		 * Private member m_onMouseButtonReleaseFunction is a std::function object wrapping a reference to a MouseButtonReleased event and returning a bool, initialised
		 * using an std::bind function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer),
		 * with an argument count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(MouseButtonReleased&)> m_onMouseButtonReleaseFunction = std::bind(&EventHandler::DefaultOnMouseButtonRelease, this, std::placeholders::_1);
		/*!
		 * \param defaultRelease
		 * MouseButtonReleased& defaultRelease is a reference to a MouseButtonReleased event object.
		 *
		 * Inline function DefaultOnMouseButtonRelease defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnMouseButtonRelease(MouseButtonReleased& defaultRelease) { return false; };

		/*!
		 * Private member m_onMouseMoveFunction is a std::function object wrapping a reference to a MouseMoved event and returning a bool, initialised
		 * using an std::bind function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer),
		 * with an argument count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(MouseMoved&)> m_onMouseMoveFunction = std::bind(&EventHandler::DefaultOnMouseMove, this, std::placeholders::_1);
		/*!
		 * \param defaultMove
		 * MouseMoved& defaultMove is a reference to a MouseMoved event object.
		 *
		 * Inline function DefaultOnMouseMoved defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnMouseMove(MouseMoved& defaultMove) { return false; };

		/*!
		 * Private member m_onMouseScrollFunction is a std::function object wrapping a reference to a MouseScrolled event and returning a bool, initialised
		 * using an std::bind function that binds the target callable object(a reference to the default event function), to the current instance (using the this pointer),
		 * with an argument count of one (using std::placeholders_1(the stored reference to the default event function)).
		 */
		std::function<bool(MouseScrolled&)> m_onMouseScrollFunction = std::bind(&EventHandler::DefaultOnMouseScroll, this, std::placeholders::_1);
		/*!
		 * \param defaultScroll
		 * MouseScrolled& defaultScroll is a reference to a MouseScrolled event object.
		 *
		 * Inline function DefaultOnMouseScrolled defines the default behaviour where the bool value false is returned indicating nothing happened.
		 */
		inline bool DefaultOnMouseScroll(MouseScrolled& defaultScroll) { return false; };
	};
}
