/*!
 * \file keyEvents.h
 * \brief KeyEvents class header file, to declare the KeyEvent class inheriting from the Events class, as well as the various KeyEvent sub-classes.
 */

#pragma once

#include "events.h"

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	/*!
	 * \class KeyEvent
	 * \brief KeyEvent parent class inheriting from Events that declares what is necessary for a key event, parent class to all other keyboard event types.
	 */
	class KeyEvent : public Events
	{
	protected:
		/*!
		 * \param keyCode
		 * is an int32_t, a fixed width 32bit integer, corresponds to a certain key on the keyboard. Used to intialise member variable, m_i32KeyCode via initialisation
		 * list.
		 *
		 * The constructor for the KeyEvent parent class. Initialises member variables via initialisation list.
		 */
		KeyEvent(int32_t keyCode) :
			m_i32KeyCode(keyCode) {};
		//! Fixed width 32 bit integer to hold the keycode so we can know which key pressed reference: https://en.cppreference.com/w/cpp/language/ascii
		int32_t m_i32KeyCode;
	public:
		//! Get the value stored by the m_i32KeyCode member variable, the key code of the key pressed
		inline int32_t GetKeyCode() const { return m_i32KeyCode; };
		//! Get the category flag of the event from the EventCategories enum, keyboard events are KeyboardEvent bit pattern or InputEvent bit pattern (effectively an 'and' silly bitwise operators are silly)
		virtual inline int32_t GetEventCategory() const override { return KeyboardEvent | InputEvent; };
	};

	/*!
	 * \class KeyPressed
	 * \brief KeyPressed class that declares what is necessary for a key pressed event.
	 */
	class KeyPressed : public KeyEvent
	{
	public:
		/*!
		 * \param keyCode
		 * is a int32_t, a fixed width 32bit integer, corresponds to a certain key on the keyboard. Used to intialise member variable, inherited m_i32KeyCode via
		 * initialisation list.
		 * \param pressCount 
		 * is a uint32_t, an unsigned fixed width 32bit integer, used to count successive key presses, initialises member variable m_ui32KeyPressCounter via 
		 * an initialisation list
		 *
		 * Constructor for the KeyPressed event, as there is data to be initialised, this is done through an initialisation list, initialising data member
		 * m_i32KeyCode through the parent class constructor KeyEvent(), as well as m_ui32KeyPressCounter with the passed pressCount. 
		 */
		KeyPressed(int32_t keyCode, uint32_t pressCount) :
			KeyEvent(keyCode),
			m_ui32KeyPressCounter(pressCount) {};
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case KeyPressed
		 */
		static EventTypes GetStaticEventType() { return EventTypes::KeyPressed; };
		//! Get the event type KeyPressed from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		// Get the category flag of the event from the EventCategories enum
		//virtual inline int32_t GetEventCategory() const override { return KeyboardEvent; };
		//! Get the value stored by the m_ui32KeyPressCounter member variable, the count of consecutive presses
		inline uint32_t GetKeyPressCount() const { return m_ui32KeyPressCounter; };
		// Get the value stored by the m_i32KeyCode member variable, the key code of the key pressed
		//inline int32_t GetKeyCode() const { return m_i32KeyCode; };
	private:
		/*!
			Fixed width 32 bit unsigned int m_uiKeyPressCounter, to count how many times a key has been pressed in succession, unsigned int because should never be negative 
			integer, initialized at 0 because it will count from its lowest possible value
		*/
		uint32_t m_ui32KeyPressCounter = 0;
		// Fixed width 32 bit integer to hold the keycode so we can know which key pressed reference: https://en.cppreference.com/w/cpp/language/ascii
		//int32_t m_i32KeyCode;
	};

	/*!
	 * \class KeyReleased
	 * \brief KeyReleased class that declares what is necessary for a key release event.
	 */
	class KeyReleased : public KeyEvent
	{
	public:
		/*!
		 * \param keyCode
		 * is a int32_t, a fixed width 32bit integer, corresponds to a certain key on the keyboard. Used to intialise member variable, inherited m_i32KeyCode via
		 * initialisation list.
		 *
		 *	Constructor for the KeyReleased event, as there is data to be initialised, this is done through an initialisation list, argument is used to initialise data member
		 *	m_i32KeyCode through KeyEvent parent class constructor..
		 */
		KeyReleased(int32_t keyCode) :
			KeyEvent(keyCode) {}
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case KeyReleased
		 */
		static EventTypes GetStaticEventType() { return EventTypes::KeyReleased; };
		//! Get the event type KeyReleased from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		//! Get the category flag of the event from the EventCategories enum
		//virtual inline int32_t GetEventCategory() const override { return KeyboardEvent; };
		//! Get the value stored by the m_i32KeyCode member variable, the key code of the key released
		//inline int32_t GetKeyCode() const { return m_i32KeyCode; };
	private:
		//! Integer to hold the keycode so we can know which key released reference: https://en.cppreference.com/w/cpp/language/ascii
		//int32_t m_i32KeyCode;
	};

	/*!
	 * \class KeyTyped
	 * \brief KeyTyped class that declares what is necessary for a key typed event.
	 */
	class KeyTyped : public KeyEvent
	{
	public:
		/*!
		 * \param keyCode
		 * is a int32_t, a fixed width 32bit integer, corresponds to a certain key on the keyboard. Used to intialise member variable, inherited m_i32KeyCode via
		 * initialisation list.
		 * 
		 *	Constructor for the KeyTyped event, as there is data to be initialised, this is done through an initialisation list, argument is used to initialise data member 
		 *	m_i32KeyCode.
		 */
		KeyTyped(int32_t keyCode) :
			KeyEvent(keyCode) {}
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case KeyTyped
		 */
		static EventTypes GetStaticEventType() { return EventTypes::KeyTyped; };
		//! Get the event type KeyTyped from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		// Get the category flag of the event from the EventCategories enum
		//virtual inline int32_t GetEventCategory() const override { return KeyboardEvent; };
		// Get the value stored by the m_i32KeyCode member variable, the key code of the key typed
		//inline int32_t GetKeyCode() const { return m_i32KeyCode; };
	//private:
		// Integer to hold the keycode so we can know which key typed reference: https://en.cppreference.com/w/cpp/language/ascii
		//int32_t m_i32KeyCode;
	};
}
