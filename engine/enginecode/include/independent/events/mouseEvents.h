/*!
 * \file mouseEvents.h
 * \brief MouseEvent class header file, to declare the MouseEvent sub-class inheriting from the Events class,  as well as the various MouseEvent sub-classes.
 */

#pragma once

#include "events.h"
#include <glm/glm.hpp>

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine
{
	/*!
	 * \class MouseEvent
	 * \brief MouseEvent parent class inheriting from Events that declares what is necessary for a mouse event
	 */
	class MouseEvent : public Events
	{
	public:
		/*!
		 *	Get the category flag of the event from the EventCategories enum, mouse events can be MouseDeltaEvent bit pattern or MouseButtonEvent bit pattern or
		 *	InputEvent bit pattern
		 */
		virtual inline int32_t GetEventCategory() const override { return MouseDeltaEvent | MouseButtonEvent | InputEvent; };
	};

	/*!
	 * \class MouseButtonPressed 
	 * \brief MouseButtonPressed class that declares what is necessary for a mouse button pressed event
	 */
	class MouseButtonPressed : public MouseEvent
	{
	public:
		/*!
		 * \param buttonCode 
		 * is an int32_t, a fixed width 32bit integer, corresponds to a certain button on the mouse. Used to intialise member variable, m_i32MouseButton via 
		 * initialisation list.
		 *
		 * Constructor for the MouseButtonPressed event, as there is data to be initialised, this is done through an initialisation list, argument is used to initialise
		 * data member m_i32MouseButton.
		 */
		MouseButtonPressed(int32_t buttonCode) :
			m_i32MouseButton(buttonCode) {};
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case MouseButtonPressed
		 */
		static EventTypes GetStaticEventType() { return EventTypes::MouseButtonPressed; };
		//! Get the event type MouseButtonPressed from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		// Get the category flag of the event from the EventCategories enum
		//virtual inline int32_t GetEventCategory() const override { return MouseButtonEvent; };
		// Get the value stored in m_i32MouseButton, the key code for the mouse button pressed
		inline int32_t GetMouseButtonCode() const { return m_i32MouseButton; };
	private:
		//! Fixed width 32bit integer to hold the keycode so we can know which mouse button pressed (is an int according to lab sheet)
		int32_t m_i32MouseButton;
	};

	/*!
	 * \class MouseButtonReleased
	 * \brief MouseButtonReleased class that declares what is necessary for a mouse button released event
	 */
	class MouseButtonReleased : public MouseEvent
	{
	public:
		/*! 
		 * \param buttonCode 
		 * is an int32_t, a fixed width 32bit integer, corresponds to a certain button on the mouse. Used to intialise member variable, m_i32MouseButton via 
		 * initialisation list.
		 *
		 *	Constructor for the MouseButtonReleased event, as there is data to be initialised, this is done through an initialisation list, argument is used to initialise 
		 *	data member m_i32MouseButton.
		 */
		MouseButtonReleased(int32_t buttonCode) :
			m_i32MouseButton(buttonCode) {};
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case MouseButtonReleased
		 */
		static EventTypes GetStaticEventType() { return EventTypes::MouseButtonReleased; };
		//! Get the event type MouseButtonReleased from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		// Get the category flag of the event from the EventCategories enum
		//virtual inline int32_t GetEventCategory() const override { return MouseButtonEvent; };
		//! Get the value stored in m_i32MouseButton, the key code for the mouse button released
		inline int32_t GetMouseButtonCode() const { return m_i32MouseButton; };
	private:
		//! Fixed width 32bit integer to hold the keycode so we can know which mouse button released
		int32_t m_i32MouseButton;
	};

	/*!
	 * \class MouseMoved
	 * \brief MouseMoved class that declares what is necessary for a mouse that is moving event
	 */
	class MouseMoved : public MouseEvent
	{
	public:
		/*!
		 * \param xDelta 
		 * is a float, records the change in position(delta) of the mouse in the x-axis. Used to intialise member variable, fMousePosDeltaX via 
		 * initialisation list.
		 * \param yDelta 
		 * is a float, records the change in position(delta) of the mouse in the y-axis. Used to intialise member variable, fMousePosDeltaY via 
		 * initialisation list.
		 *
		 * Constructor for the MouseMoved event, as there is data to be initialised, this is done through an initialisation list, arguments are used to initialise data members
		 * fMousePosDeltaX and fMousePosDeltaY.
		 */
		MouseMoved(float xDelta, float yDelta) :
			fMousePosDeltaX(xDelta),
			fMousePosDeltaY(yDelta) {};
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case MouseMoved
		 */
		static EventTypes GetStaticEventType() { return EventTypes::MouseMoved; };
		//! Get the event type MouseMoved from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		//! Get the category flag of the event from the EventCategories enum
		//virtual inline int32_t GetEventCategory() const override { return MouseDeltaEvent; };
		//! Get the position delta (offset, change in position) of the mouse in the X axis stored in fMousePosDeltaX
		inline float GetMousePosDeltaX() const { return fMousePosDeltaX; };
		//! Get the position delta (offset, change in position) of the mouse in the Y axis stored in fMousePosDeltaY
		inline float GetMousePosDeltaY() const { return fMousePosDeltaY; };
		//! Get the position delta (offset, change in position) of the mouse as a vector (float values returned)
		inline glm::vec2 GetMousePosDeltaVector() const { return { fMousePosDeltaX, fMousePosDeltaY }; };
	private:
		//! Float to hold the change in position in the x axis of the mouse when it is moved
		float fMousePosDeltaX;
		//! Float to hold the change in position in the y axis of the mouse when it is moved
		float fMousePosDeltaY;
	};

	/*!
	 * \class MouseScrolled
	 * \brief MouseScrolled class that declares what is necessary for a mouse wheel that is scrolling event
	 */
	class MouseScrolled : public MouseEvent
	{
	public:
		/*!
		 * \param xDelta 
		 * is a float, records the change in position(delta) of the mouse in the x-axis. Used to intialise member variable, fMouseScrollDeltaX via 
		 * initialisation list.
		 * \param yDelta 
		 * is a float, records the change in position(delta) of the mouse in the y-axis. Used to intialise member variable, fMouseScrollDeltaY via 
		 * initialisation list.
		 *
		 *	Constructor for the MouseScrolled event, as there is data to be initialised, this is done through an initialisation list, arguments are used to initialise data members 
		 *	fMouseScrollDeltaX and fMouseScrollDeltaY.
		 */
		MouseScrolled(float xDelta, float yDelta) :
			fMouseScrollDeltaX(xDelta),
			fMouseScrollDeltaY(yDelta) {};
		/*!
		 * Static enum class EventTypes function to return a static EventTypes enum class type for logic comparisons, we need something that is definite to compare to,
		 * in this case MouseScrolled
		 */
		static EventTypes GetStaticEventType() { return EventTypes::MouseScrolled; };
		//! Get the event type MouseScrolled from the EventTypes enum class
		virtual inline EventTypes GetEventType() const override { return GetStaticEventType(); };
		// Get the category flag of the event from the EventCategories enum
		//virtual inline int32_t GetEventCategory() const override { return MouseDeltaEvent; };
		//! Get the position delta (offset, change in position) of the mouse scroll in the X axis stored in fMouseScrollDeltaX
		inline float GetMouseScrollDeltaX() const { return fMouseScrollDeltaX; };
		//! Get the position delta (offset, change in position) of the mouse scroll in the Y axis stored in fMouseScrollDeltaY
		inline float GetMouseScrollDeltaY() const { return fMouseScrollDeltaY; };
		//! Get the position delta (offset, change in position) of the mouse scroll as a vector (float values returned)
		inline glm::vec2 GetMouseScrollDeltaVector() const { return { fMouseScrollDeltaX, fMouseScrollDeltaY }; };
	private:
		//! Float to hold the change in position in the x axis of the mouse wheel when it is moved
		float fMouseScrollDeltaX;
		//! Float to hold the change in position in the y axis of the mouse wheel when it is moved
		float fMouseScrollDeltaY;
	};
}
