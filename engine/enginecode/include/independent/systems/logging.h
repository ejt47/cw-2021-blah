/*!
 * \file logging.h
 * \brief Logging class header file, declaring the Logging class functionality, inheriting from the System interface class.
 */

#pragma once

#include "system.h"
#include <spdlog/spdlog.h>

//! \def CONSOLE_LOGGING_CRIT_FAIL generates output to the console that the logging system failed to start
#define CONSOLE_LOGGING_CRIT_FAIL GetConsoleLogger()->critical("CRITICAL CONSOLE LOGGING SYSTEM START FAILURE."); 
//! \def FILE_LOGGING_CRIT_FAIL generates output to a file that the logging system failed to start
#define FILE_LOGGING_CRIT_FAIL GetFileLogger()->critical("CRITICAL FILE LOGGING SYSTEM START FAILURE.");


/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine 
{

	/*!
	 * \class Logging 
	 * \brief Logging class inheriting from the System interface class to handle logging information.
	 *
	 * Logging class to log information, warnings, errors and then output logged items using a well formatted message to the console
	 * or a file. Inherits from the System interface class.
	 */
	class Logging : public System
	{
	public:
		//! Virtual void function to start the logger, 
		virtual void Start(SystemSignal init = SystemSignal::None, ...) override;
		//! Virtual void function to stop the logger
		virtual void Stop(SystemSignal close = SystemSignal::None, ...) override;
		//! Inline static accessor function for the shared pointer to the console logger
		inline static std::shared_ptr<spdlog::logger> GetConsoleLogger() { return s_pConsoleLogger; };
		//! Inline static accessor function for the shared pointer to the file logger
		inline static std::shared_ptr<spdlog::logger> GetFileLogger() { return s_pFileLogger; };
		//! Inline static accessor function for the Logging system state check flag
		inline static bool GetLoggingFlag() { return s_bLoggingFlag; };

		//! Variadic template for critical log messages
		template<class ...Args>
		static void Critical(Args&&... args);

		//! Variadic template for debug log messages
		template<class ...Args>
		static void Debug(Args&&... args);

		//! Variadic template for error log messages
		template<class ...Args>
		static void Error(Args&&... args);

		//! Variadic template for info log messages
		template<class ...Args>
		static void Info(Args&&... args);

		//! Variadic template for trace log messages
		template<class ...Args>
		static void Trace(Args&&... args);

		//! Variadic template for warning log messages
		template<class ...Args>
		static void Warning(Args&&... args);

		//! Variadic template for release mode log messages
		template<class ...Args>
		static void Release(Args&&... args);

		//! Variadic template for file log messages
		template<class ...Args>
		static void File(Args&&... args);

	private:
		/*!
		 * Shared pointer s_pConsoleLogger is the Console Logger, static to create single point of access for console logging functionality,
		 * not initialised here because it does not belong to an instance, will be initialised in source file where instance will be created
		 * in a function, i.e. Start function
		 */
		static std::shared_ptr<spdlog::logger> s_pConsoleLogger;
		/*!
		 * Static bool s_bLoggingFlag is used as flag to check for instantiation of console logger, to ensure it can't be instantiated
		 * more than once.
		 */
		static bool s_bLoggingFlag;
		/*!
		 * Shared pointer s_pFileLogger is the file Logger, static to create single point of access for file logging functionality,
		 * not initialised here because it does not belong to an instance, will be initialised in source file where instance will be created
		 * in a function, i.e. Start function
		 */
		static std::shared_ptr<spdlog::logger> s_pFileLogger;

		//! Friend class declaration, so that the logging system can be used in the Randomisation class
		//friend class Randomisation;
		//! Friend class declaration, so that logging system can be used in WindowInterface sub-classes
		//friend class WindowInterface;
	};

////! \def CONSOLE_LOGGING_CRIT_FAIL generates output to the console that the logging system failed to start
//#define CONSOLE_LOGGING_CRIT_FAIL GetConsoleLogger()->critical("CRITICAL CONSOLE LOGGING SYSTEM START FAILURE."); 
////! \def FILE_LOGGING_CRIT_FAIL generates output to a file that the logging system failed to start
//#define FILE_LOGGING_CRIT_FAIL GetFileLogger()->critical("CRITICAL FILE LOGGING SYSTEM START FAILURE.");

	template<class ...Args>
	static void Logging::Critical(Args&&... args)
	{
#ifdef NG_DEBUG
		if(GetLoggingFlag()) GetConsoleLogger()->critical(std::forward<Args>(args) ...);
		else CONSOLE_LOGGING_CRIT_FAIL;
#endif
	}

	template<class ...Args>
	static void Logging::Debug(Args&&... args)
	{
#ifdef NG_DEBUG
		if (GetLoggingFlag()) GetConsoleLogger()->debug(std::forward<Args>(args) ...);
		else CONSOLE_LOGGING_CRIT_FAIL;
#endif
	}

	template<class ...Args>
	static void Logging::Error(Args&&... args)
	{
#ifdef NG_DEBUG
		if (GetLoggingFlag()) GetConsoleLogger()->error(std::forward<Args>(args) ...);
		else CONSOLE_LOGGING_CRIT_FAIL;
#endif
	}

	template<class ...Args>
	static void Logging::Info(Args&&... args)
	{
#ifdef NG_DEBUG
		if (GetLoggingFlag()) GetConsoleLogger()->info(std::forward<Args>(args) ...);
		else CONSOLE_LOGGING_CRIT_FAIL;
#endif
	}

	template<class ...Args>
	static void Logging::Trace(Args&&... args)
	{
#ifdef NG_DEBUG
		if (GetLoggingFlag()) GetConsoleLogger()->trace(std::forward<Args>(args) ...);
		else CONSOLE_LOGGING_CRIT_FAIL;
#endif
	}

	template<class ...Args>
	static void Logging::Warning(Args&&... args)
	{
#ifdef NG_DEBUG
		if (GetLoggingFlag()) GetConsoleLogger()->warn(std::forward<Args>(args) ...);
		else CONSOLE_LOGGING_CRIT_FAIL;
#endif
	}

	template<class ...Args>
	static void Logging::Release(Args&&... args)
	{
#ifdef NG_RELEASE
		if (GetLoggingFlag()) GetConsoleLogger()->trace(std::forward<Args>(args) ...);
		else CONSOLE_LOGGING_CRIT_FAIL;
#endif
	}

	template<class ...Args>
	static void Logging::File(Args&&... args)
	{
#ifdef NG_RELEASE
		if (GetLoggingFlag()) GetFileLogger()->trace(std::forward<Args>(args) ...);
		else FILE_LOGGING_CRIT_FAIL
#endif
	}
}