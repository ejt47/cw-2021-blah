/*!
 * \file randomisation.h
 * \brief Randomisation class header file, declaring the Randomisation class functionality, inheriting from the System interface class.
 */
#pragma once

//#include "system.h" // this is commented out as if I want to lof stuff inside randomisation, don't need two whole system.h in translation unit, original interface is accessed through logging.h include
#include "systems/logging.h"
#include <random>

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

/*
	REFER TO THIS BLOCK COMMENT IF YOU GET CONFUSED BOIIIIIIIIIII
	-GENERAL NOTES AND REFERENCE MATERIALS ON RANDOMISATION-
	https://en.cppreference.com/w/cpp/numeric/random
	https://en.cppreference.com/w/cpp/header/random

	pseudo-random number generation through the <random> header essentially boils down to two types of provided classes that
	can be accessed: 
		GENERATORS: 
		There are quite a few types and there is no need to go into all of them and exactly what they do but generally
		they are random number engines that takes some form of seed data to generate a sequence of random numbers of varying types.
		The <random> header provides access to three main random number engines that apply different algorithms, these engines
		all have predefined type aliases that make use of commonly used parameter sets in the <random> header, the video example 
		makes use of the std::mersenne_twister_engine through the type alias std::mt19937 (32bit), a 64bit version is also available
		you could use the engine itself, but it is a lot more work than using the provided std lib type aliases.

		DISTRIBUTIONS:
		There are a variety of random number distribution types provided by the <random> header, these are used to post-process
		the output of the generator used, there are a variety of types of distribution accessible through the <random> header and
		they provide for the generation of integers as well as real numbers. The video example makes use of the provided uniform
		distributions (there is one uniform distribution type for integers and one for real numbers), as well as demonstrating the
		usage of std::normal_distribution to generate integers and real numbers.

		Syntactically the way distribution objects are passed varies dependent on the distribution type, i.e std::normal_distribution
		is passed (a mean, and a standard deviation), whereas std::uniform_int_distribution is passed a range as a (min value, max value)

		I will have to look at cppreference if I want to implement more than uniform and normal distribution generation, realistically
		after watching the video example and doing this reading on how these things work I think uniform dist is the best approach so
		I am going to implement that, I'm also going to implement normal dist aswell just because Simon did it in his video example.

	-NOTES/REFERENCE ON FIXED WIDTH TYPES-
	https://stackoverflow.com/questions/9239558/what-is-the-difference-between-intxx-t-and-int-fastxx-t
	https://en.cppreference.com/w/cpp/types/integer

	After implementing most of this class using int_fast64_t, which on my first go around at looking for information on the int32_t that Simon
	had used in his video demonstration I misunderstood the various fixed width integer types denoted by _t, I mistakenly made the assumption
	that as the project I am developing is for x64 architectures that using a int64_t would make the most sense, furthermore after some not so
	in depth reading on the subject I saw int_fastN_t variations and read "fastest signed integer type with width of at least 8, 16, 32 and 64 bits 
	respectively", I did not really take into consideration what this entailed and just said to myself GOTTA GO FAST BOI, I defined all my integer
	distribution functions with int_fast64_t for their return type and parameter types.

	What it actually means is that, the integer will ALWAYS be at LEAST the specified n bits long, but if the system the code is executed on supports
	integer types of n bits LARGER than the stated LEAST n bits integer type, and the bus width to the CPU supports the LARGER n bits integer, the
	LARGER n bits integer will be faster, so the specified LEAST n bits integer will instead be defined as the LARGER n bits integer on compilation 
	for that system. Theoretically. 

	From what I can gather currently, there is no guarantee that the LEAST bits integer will ALWAYS be compiled optimally to the LARGER n bits integer.

	So it does seem pretty pointless, and it is compiler dependent. However, int_fastN_t and int_leastN_t (I'm not going to bother with writing an
	explanation for myself for int_leastN_t), are REQUIRED in C++ Standard compiler implementations, whereas the intN_t fixed width integer types
	are OPTIONAL. Therefore I have concluded that I will continue to use int_fastN_t fixed width integer types for portability purposes, even if the
	above linked stack overflow question answers suggest that only the most exotic of compilers will not support intN_t, for this same reason I have 
	also decided to redefine any aspects of my code here that previously used int_fast64_t to int_fast32_t just in case, as it should always compile
	to the LARGER fixed width 64 bit integer on an x64 architecture system (potentially). 

	I should be doing a unit test retroactively for this system soon, so I can verify these things when I do that.
*/

namespace Engine
{
	/*!
	 * \class Randomisation
	 * \brief Randomisation class inheriting from the System class interface to handle randomising numbers.
	 */
	class Randomisation : public System
	{
	public:
		//! Variadic virtual void function Start takes variadic quantity of arguments
		virtual void Start(SystemSignal init = SystemSignal::None, ...);
		//! Variadic virtual void function Stop takes variadic quantity of arguments
		virtual void Stop(SystemSignal close = SystemSignal::None, ...);
		/*! 
		 * \param minBound 
		 * int32_t minBound is the minimum value a fixed width 32bit integer can be.
		 * \param maxBound 
		 * int32_t maxBound is the maximum value a fixed width 32bit integer can be.
		 *
		 * Static function for uniform distribution returning a pseudo-random int32_t fixed width integer.
		 */
		static int32_t RandomIntUniformDistribution(int32_t minBound, int32_t maxBound);
		/*!
		 * \param minBound
		 * float minBound is the minimum value a 32bit floating point real number can be.
		 * \param maxBound
		 * float maxBound is the maximum value a 32bit floating point real number can be.
		 *
		 * Static function for uniform distribution returning a pseudo-random a 32bit floating point real number.
		 */
		static float RandomRealUniformDistribution(float minBound, float maxBound);
		/*!
		 * \param mean
		 * float mean is the number that should appear the most on average.
		 * \param sigma
		 * float sigma is the standard deviation from the mean.
		 *
		 * Static function for a normal distribution returning a pseudo-random int32_t fixed width integer.
		 */
		static int32_t RandomIntNormalDistribution(float mean, float sigma);
		//! Static function for normal dist returning a float
		/*!
		 * \param mean
		 * float mean is the number that should appear the most on average.
		 * \param sigma
		 * float sigma is the standard deviation from the mean.
		 *
		 * Static function for a normal distribution returning a pseudo-random a 32bit floating point real number.
		 */
		static float RandomRealNormalDistribution(float mean, float sigma);

	private:
		/*! 
		 * Static shared pointer to a std library 32bit implementation of an random number generation algorithm using std::mersenne_twister_engine
		 * which has the type alias std::mt19937 so that it can be used as a type (wow c++ is complicated), initialised in randomisation.src 
		 * because it is static as a nullptr, which is then initialised conditionally as needed in the system interface class Start function implementation.
		 */
		static std::shared_ptr<std::mt19937> s_pMersenneGenerator;
		/*!
		 * Static std::uniform_int_distribution<int32_t> object s_i32UniformDistribution to be used in processing the generated random
		 * number provided by s_pMersenneGenerator to constrain it between a uniform range, initialised in randomisation.src because it
		 * is static, initialised using std::numeric_limits min and max functions for its range boundaries
		 */
		static std::uniform_int_distribution<int32_t> s_i32UniformDistribution;
		/*!
		 * Static std::uniform_real_distribution<float> object s_fUniformDistribution to be used in processing the generated random
		 * number provided by s_pMersenneGenerator to constrain it between a uniform range, initialised in randomisation.src because it
		 * is static, initialised using std::numeric_limits min and max functions for its range boundaries
		 */
		static std::uniform_real_distribution<float> s_fUniformDistribution;
	};
}
