/*!
 * \file system.h
 * \brief System interface class header file, declaring a pure virtual interface to program sub-systems to.
 */

#pragma once

#include <cstdarg>

/*!
 * \namespace Engine
 * Provides engine scope namespace for all of the systems that make up the engine, essentially prevents having to write Engine:: before
 * every defintion and declaration. Keeps the codebase looking more organised and tidy.
 */

namespace Engine 
{

	/*! 
	 * \enum SystemSignal 
	 * where Enumerated class  "None" is initialised as 0
	 */
	enum class SystemSignal { None = 0 };

	/*!
	 * \class System 
	 * \brief System class providing pure virtual variadic virtual void functions Start and Stop, for initialisation and termination of systems inheriting from
	 * this interface class.
	 */

	class System
	{
	public:
		//! Virtual destructor for the System interface class
		virtual ~System() {};
		//! Variadic virtual void function Start takes variadic quantity of arguments
		virtual void Start(SystemSignal init = SystemSignal::None, ...) = 0; 
		//! Variadic virtual void function Stop takes variadic quantity of arguments
		virtual void Stop(SystemSignal close = SystemSignal::None, ...) = 0; 
	};
}
