#pragma once

//#include "core/windows/windowInterface.h"
#include "systems/system.h"

//#include "engine_pch.h"
#include "GLFW/GLFWSystem.h"
#include "GLFW/windowGLFW.h"

#include "systems/logging.h"

#include <gtest/gtest.h>

class winGLFWTestApplication
{
public:
	std::shared_ptr<Engine::WindowInterface> pWindow;
	std::shared_ptr<Engine::System> pWindowSystem;

	void winGLFWTestApplication::InitEventCallbacks()
	{
		pWindow->GetEventHandler().SetOnWindowCloseCallback(std::bind(&winGLFWTestApplication::OnWindowClose, this, std::placeholders::_1));
		pWindow->GetEventHandler().SetOnWindowResizeCallback(std::bind(&winGLFWTestApplication::OnWindowResize, this, std::placeholders::_1));
		pWindow->GetEventHandler().SetOnWindowFocusCallback(std::bind(&winGLFWTestApplication::OnWindowFocus, this, std::placeholders::_1));
		pWindow->GetEventHandler().SetOnWindowLostFocusCallback(std::bind(&winGLFWTestApplication::OnWindowLostFocus, this, std::placeholders::_1));
		pWindow->GetEventHandler().SetOnWindowMoveCallback(std::bind(&winGLFWTestApplication::OnWindowMove, this, std::placeholders::_1));
		pWindow->GetEventHandler().SetOnKeyPressCallback(std::bind(&winGLFWTestApplication::OnKeyPress, this, std::placeholders::_1));
		pWindow->GetEventHandler().SetOnKeyReleaseCallback(std::bind(&winGLFWTestApplication::OnKeyRelease, this, std::placeholders::_1));
		pWindow->GetEventHandler().SetOnKeyTypedCallback(std::bind(&winGLFWTestApplication::OnKeyTyped, this, std::placeholders::_1));
		pWindow->GetEventHandler().SetOnMouseButtonPressCallback(std::bind(&winGLFWTestApplication::OnMouseButtonPress, this, std::placeholders::_1));
		pWindow->GetEventHandler().SetOnMouseButtonReleaseCallback(std::bind(&winGLFWTestApplication::OnMouseButtonRelease, this, std::placeholders::_1));
		pWindow->GetEventHandler().SetOnMouseMoveCallback(std::bind(&winGLFWTestApplication::OnMouseMove, this, std::placeholders::_1));
		pWindow->GetEventHandler().SetOnMouseScrollCallback(std::bind(&winGLFWTestApplication::OnMouseScroll, this, std::placeholders::_1));
	}

private:
	// All the below functions simply defined with logic to check if the event was handled, testing of the events themselves done in other unit tests
	bool winGLFWTestApplication::OnWindowClose(Engine::WindowClosed& windowClosed)
	{
		windowClosed.HandleEvent(true);
		return windowClosed.EventHandled();
	}

	bool winGLFWTestApplication::OnWindowResize(Engine::WindowResized& windowResized)
	{
		windowResized.HandleEvent(true);
		return windowResized.EventHandled();
	}

	bool winGLFWTestApplication::OnWindowFocus(Engine::WindowFocus& windowFocus)
	{
		windowFocus.HandleEvent(true);
		return windowFocus.EventHandled();
	}

	bool winGLFWTestApplication::OnWindowLostFocus(Engine::WindowLostFocus& windowLostFocus)
	{
		windowLostFocus.HandleEvent(true);
		return windowLostFocus.EventHandled();
	}

	bool winGLFWTestApplication::OnWindowMove(Engine::WindowMoved& windowMoved)
	{
		windowMoved.HandleEvent(true);
		return windowMoved.EventHandled();
	}

	bool winGLFWTestApplication::OnKeyPress(Engine::KeyPressed& keyPressed)
	{
		keyPressed.HandleEvent(true);
		return keyPressed.EventHandled();
	}

	bool winGLFWTestApplication::OnKeyRelease(Engine::KeyReleased& keyReleased)
	{
		keyReleased.HandleEvent(true);
		return keyReleased.EventHandled();
	}

	bool winGLFWTestApplication::OnKeyTyped(Engine::KeyTyped& keyTyped)
	{
		keyTyped.HandleEvent(true);
		return keyTyped.EventHandled();
	}

	bool winGLFWTestApplication::OnMouseButtonPress(Engine::MouseButtonPressed& buttonPressed)
	{
		buttonPressed.HandleEvent(true);
		return buttonPressed.EventHandled();
	}

	bool winGLFWTestApplication::OnMouseButtonRelease(Engine::MouseButtonReleased& buttonReleased)
	{
		buttonReleased.HandleEvent(true);
		return buttonReleased.EventHandled();
	}

	bool winGLFWTestApplication::OnMouseMove(Engine::MouseMoved& mouseMoved)
	{
		mouseMoved.HandleEvent(true);
		return mouseMoved.EventHandled();
	}

	bool winGLFWTestApplication::OnMouseScroll(Engine::MouseScrolled& mouseScrolled)
	{
		mouseScrolled.HandleEvent(true);
		return mouseScrolled.EventHandled();
	}
};