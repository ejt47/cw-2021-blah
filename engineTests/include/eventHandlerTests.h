#pragma once

#include "events/eventHandler.h"
#include <gtest/gtest.h>

class TestApplication
{
public:
	// Member object to access the EventHandler class event handling functions  
	Engine::EventHandler m_eventHandler;
	// Function to initialise the abstract event callback functions
	void TestApplication::InitEventCallbacks()
	{
		m_eventHandler.SetOnWindowCloseCallback(std::bind(&TestApplication::OnWindowClose, this, std::placeholders::_1));
		m_eventHandler.SetOnWindowResizeCallback(std::bind(&TestApplication::OnWindowResize, this, std::placeholders::_1));
		m_eventHandler.SetOnWindowFocusCallback(std::bind(&TestApplication::OnWindowFocus, this, std::placeholders::_1));
		m_eventHandler.SetOnWindowLostFocusCallback(std::bind(&TestApplication::OnWindowLostFocus, this, std::placeholders::_1));
		m_eventHandler.SetOnWindowMoveCallback(std::bind(&TestApplication::OnWindowMove, this, std::placeholders::_1));
		m_eventHandler.SetOnKeyPressCallback(std::bind(&TestApplication::OnKeyPress, this, std::placeholders::_1));
		m_eventHandler.SetOnKeyReleaseCallback(std::bind(&TestApplication::OnKeyRelease, this, std::placeholders::_1));
		m_eventHandler.SetOnKeyTypedCallback(std::bind(&TestApplication::OnKeyTyped, this, std::placeholders::_1));
		m_eventHandler.SetOnMouseButtonPressCallback(std::bind(&TestApplication::OnMouseButtonPress, this, std::placeholders::_1));
		m_eventHandler.SetOnMouseButtonReleaseCallback(std::bind(&TestApplication::OnMouseButtonRelease, this, std::placeholders::_1));
		m_eventHandler.SetOnMouseMoveCallback(std::bind(&TestApplication::OnMouseMove, this, std::placeholders::_1));
		m_eventHandler.SetOnMouseScrollCallback(std::bind(&TestApplication::OnMouseScroll, this, std::placeholders::_1));
	}

private:
	// All the below functions simply defined with logic to check if the event was handled, testing of the events themselves done in other unit tests
	bool TestApplication::OnWindowClose(Engine::WindowClosed& windowClosed)
	{
		windowClosed.HandleEvent(true);
		return windowClosed.EventHandled();
	}

	bool TestApplication::OnWindowResize(Engine::WindowResized& windowResized)
	{
		windowResized.HandleEvent(true);
		return windowResized.EventHandled();
	}

	bool TestApplication::OnWindowFocus(Engine::WindowFocus& windowFocus)
	{
		windowFocus.HandleEvent(true);
		return windowFocus.EventHandled();
	}

	bool TestApplication::OnWindowLostFocus(Engine::WindowLostFocus& windowLostFocus)
	{
		windowLostFocus.HandleEvent(true);
		return windowLostFocus.EventHandled();
	}

	bool TestApplication::OnWindowMove(Engine::WindowMoved& windowMoved)
	{
		windowMoved.HandleEvent(true);
		return windowMoved.EventHandled();
	}

	bool TestApplication::OnKeyPress(Engine::KeyPressed& keyPressed)
	{
		keyPressed.HandleEvent(true);
		return keyPressed.EventHandled();
	}

	bool TestApplication::OnKeyRelease(Engine::KeyReleased& keyReleased)
	{
		keyReleased.HandleEvent(true);
		return keyReleased.EventHandled();
	}

	bool TestApplication::OnKeyTyped(Engine::KeyTyped& keyTyped)
	{
		keyTyped.HandleEvent(true);
		return keyTyped.EventHandled();
	}

	bool TestApplication::OnMouseButtonPress(Engine::MouseButtonPressed& buttonPressed)
	{
		buttonPressed.HandleEvent(true);
		return buttonPressed.EventHandled();
	}

	bool TestApplication::OnMouseButtonRelease(Engine::MouseButtonReleased& buttonReleased)
	{
		buttonReleased.HandleEvent(true);
		return buttonReleased.EventHandled();
	}

	bool TestApplication::OnMouseMove(Engine::MouseMoved& mouseMoved)
	{
		mouseMoved.HandleEvent(true);
		return mouseMoved.EventHandled();
	}

	bool TestApplication::OnMouseScroll(Engine::MouseScrolled& mouseScrolled)
	{
		mouseScrolled.HandleEvent(true);
		return mouseScrolled.EventHandled();
	}
};
