//RETURNING TO UNIT TESTS FOR windowGLFW class GOD HELP ME

/*
	actually think about what it is that you are trying to test, previously things weren't working, you were trying to open a window
	and use the window to test the functionality of the implementation.

	the problem being that you aren't testing to see if a window is opened, you can see the the engine project can open a window and do
	various things, that much is clear.

	you are testing the data, and how it is processed, not the ability to open a window. you are trying to see that the window created
	is created correctly in terms of data!
	
	i.e. the window is constructed using the default windowproperties, you should expect that the WIDTH = 800, HEIGHT = 600, FULLSCREEN
	= FALSE, VSYNC = FALSE 

	you shouldn't need to actually make a graphical window to test this!! Remember you are testing this particular module of code.

	Well I'm confused again actually. Going back to the code, it is apparent that you cannot construct a windowGLFW instance without calling
	the GLFW function to create a window.

	So what am I misunderstanding here?

	WELL looking at the old code on github, I was trying to test the window close event, in terms of a windowGLFW instance. I was treating 
	the testing very much the same as how EventHandler was tested. This is not correct, how is one supposed to test the local internal contents
	of a lambda expression? The test really only requires a check on the returns of glfwSetWindowCloseCallback, which, according to:
	https://www.glfw.org/docs/latest/group__window.html#gada646d775a7776a95ac000cfc1885331 will return either the previously set call back
	(in the case of the windowGLFW class this is the event handler class function SetOnWindowCloseCallback), or NULL if no callback was set.

	WHAT I now believe that I should have been testing was something like "it is expected that glfwSetWindowCloseCallback returns a function
	pointer to the EventHandler function SetOnWindowCloseCallback, it is expected that glfwSetOnCloseCallback does not return NULL".

	HOW in practice do I go about doing this? Before I implemented explicitlly a mock application,
	where a shared pointer to a GLFWSystem allocated memory for a GLFWSystem object, and then called the Start function, and then constructed a default 
	WindowProperties struct, to pass to a WindowCreate function passed to the reset function of a shared pointer to a windowGLFW object, where the 
	the test threw a error, before being able to instantiate a WindowClosed object that was tested similarly to the EventHandler.
	I should (I think) instead, not do all of that, I should only call glfwInit(), then just call the windowGLFW constructor, then test that calling
	glfwSetWindowCloseCallback returns a GLFWwindowclosefun pointer, and not NULL. I may need to include the EventHandler header file to instantiate
	a EventHandler pointer to access the SetOnWindowCallback function to be used in the lambda expression pass as the function pointer recognised 
	by GLFW as GLFWwindowclosefun ... I think I may be on to something here ... does glfw beed to be linked to the enginetests project through the
	lua script? I think it will probably need to be... Or maybe not if I am linking the Engine project, to access the EventHandler class
	
	LMAO YOU WERE CORRECT THE WHOLE TIME EXCEPT GOOGLE TEST GOT CONFUSED BY EVENT HANDLER AND WINDOWGLFW TEST BOTH HAVING A TESTAPPLICATION CLASS
	NAMED EXACTLY THE SAME IN THEIR HEADER............ 
*/

#include "windowGLFWTests.h"
#include "include/platform/GLFW/GLFWSystem.h"
#include "include/platform/GLFW/windowGLFW.h"
//#include "engine_pch.h"

//THINK IT WOULD ACTUALLY BE BETTER TO MAKE A TEST PER FUNCTION, I.E. THIS FUNCTION SHOULD ONLY BE IN REGARD TO windowGLFW InitCallbacks func!! 
TEST(WindowGLFW, OnWindowClose)
{
	winGLFWTestApplication winGLFWTestApplication;

	winGLFWTestApplication.pWindowSystem.reset(new Engine::GLFWSystem);
	winGLFWTestApplication.pWindowSystem->Start();

	Engine::WindowProperties windowProperties;
	//it appears this below line causes crash because I am calling the Static WindowCreate out of scope... 
	winGLFWTestApplication.pWindow.reset(Engine::WindowInterface::WindowCreate(windowProperties));

	// Instantiate a WindowClosed event object to test event handling with
	Engine::WindowClosed windowClosed;

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = windowClosed.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowCloseCallback function's returned wrapped function, returning as a bool,
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& closeCallbackDefault = winGLFWTestApplication.pWindow->GetEventHandler().GetOnWindowCloseCallback();
	// Pass the windowClosed event to the closeCallbackDefault object to handle the event
	closeCallbackDefault(windowClosed);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = windowClosed.EventHandled();

	winGLFWTestApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowCloseCallback function's returned wrapped function, returning a bool
		(now initialised)
	*/
	auto& closeCallback = winGLFWTestApplication.pWindow->GetEventHandler().GetOnWindowCloseCallback();
	// Pass the windowClosed event to the closeCallback object to handle the event
	closeCallback(windowClosed);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = windowClosed.EventHandled();


	auto WindowCloseLambda = [](GLFWwindow* window)
	{
		// Cast the stored user pointer (known to be an EventHandler pointer), back out to an EventHandler pointer from its void pointer state.
		Engine::EventHandler* pEventHandler = static_cast<Engine::EventHandler*>(glfwGetWindowUserPointer(window));
		/*
			Instantiate using the auto keyword a reference to an EventHandler class bool wrapped std::function for the WindowClosed event callback, and assign its
			functional value using the appropriate accessor function GetWindowCloseCallback, named OnWindowClose.
		*/
		auto& OnWindowClose2 = pEventHandler->GetOnWindowCloseCallback();

		// Instantiate a WindowClosed event object named windowClosed
		Engine::WindowClosed windowClosed2;
		// Pass the WindowClosed event object to the OnWindowClose callback function
		OnWindowClose2(windowClosed2);
	};

	// Call glfwSetWindowCloseCallback and pass the WindowGLFW class member pointer to a GLFWwindow m_nativeWindowGLFW, and the WindowCloseLambda as the arguments
	glfwSetWindowCloseCallback(static_cast<GLFWwindow*>(winGLFWTestApplication.pWindow->GetNativeWindow()), WindowCloseLambda);

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}
