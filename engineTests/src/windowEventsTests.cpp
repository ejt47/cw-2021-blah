/*!
 * \file windowEventsTests.cpp
 */

#include "windowEventsTests.h"

/*!
 * \test WindowClosed
 * Test to make sure WindowClosed class functions as expected
 */
TEST(WindowEvents, WindowClosed)
{
	//! Construct a WindowClosed object using default constructor
	Engine::WindowClosed closedConstructor;
	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = closedConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = closedConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = closedConstructor.GetStaticEventType();

	// Perform a test for each initialised variable or object based on what is expected return value or type, using the appropriate GoogleTest macro
	EXPECT_EQ(eventCategory, Engine::EventCategories::WindowEvent);
	EXPECT_EQ(eventType, Engine::EventTypes::WindowClosed);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(closedConstructor.CategoryCheck(Engine::EventCategories::WindowEvent));
}

/*! 
 * \test WindowResized
 * Test to make sure WindowResized class functions as expected
 */
TEST(WindowEvents, WindowResized)
{
	//! Construct a WindowResized object, pass it parameters of expected type
	Engine::WindowResized resizeConstructor(800, 600);

	//! Fixed width 32bit integer width1 is initialised with its expected accessor function GetResizedWidth using auto keyword to infer type from function call
	auto width1 = resizeConstructor.GetResizedWidth();
	//! Fixed width 32bit integer height1 is initialised with its expected accessor function GetResizedHeight using auto keyword to infer type from function call
	auto height1 = resizeConstructor.GetResizedHeight();

	//! glm::ivec2 object is initialised with its expected accessor function GetResizedSize using auto keyword to infer type from function call
	auto size = resizeConstructor.GetResizedSize();
	//! Fixed width 32bit integer width2 is initialised with its expected accessor for size's x axis parameter using auto keyword to infer type
	auto width2 = size.x;
	//! Fixed width 32bit integer height2 is initialised with its expected accessor for size's y axis parameter using auto keyword to infer type
	auto height2 = size.y;
	
	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = resizeConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = resizeConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = resizeConstructor.GetStaticEventType();

	// Perform a test for each initialised variable or object based on what is expected return value or type, using the appropriate GoogleTest macro
	EXPECT_EQ(width1, 800);
	EXPECT_EQ(width2, 800);
	EXPECT_EQ(height1, 600);
	EXPECT_EQ(height2, 600);
	EXPECT_EQ(eventCategory, Engine::EventCategories::WindowEvent);
	EXPECT_EQ(eventType, Engine::EventTypes::WindowResized);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(resizeConstructor.CategoryCheck(Engine::EventCategories::WindowEvent));
}

/*!
 * \test WindowFocus
 * Test to make sure WindowFocus class functions as expected
 */
TEST(WindowEvents, WindowFocus)
{
	//! Construct a WindowFocus object using default constructor
	Engine::WindowFocus focusConstructor;
	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = focusConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = focusConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = focusConstructor.GetStaticEventType();

	// Perform a test for each initialised variable or object based on what is expected return value or type, using the appropriate GoogleTest macro
	EXPECT_EQ(eventCategory, Engine::EventCategories::WindowEvent);
	EXPECT_EQ(eventType, Engine::EventTypes::WindowFocus);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(focusConstructor.CategoryCheck(Engine::EventCategories::WindowEvent));
}

/*!
 * \test WindowLostFocus
 * Test to make sure WindowLostFocus class functions as expected
 */
TEST(WindowEvents, WindowLostFocus)
{
	//! Construct a WindowLostFocus object, pass it parameters of expected type
	Engine::WindowLostFocus lostFocusConstructor;
	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = lostFocusConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = lostFocusConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = lostFocusConstructor.GetStaticEventType();

	// Perform a test for each initialised variable or object based on what is expected return value or type, using the appropriate GoogleTest macro
	EXPECT_EQ(eventCategory, Engine::EventCategories::WindowEvent);
	EXPECT_EQ(eventType, Engine::EventTypes::WindowLostFocus);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(lostFocusConstructor.CategoryCheck(Engine::EventCategories::WindowEvent));
}

/*!
 * \test WindowMoved
 * Test to make sure WindowMoved class functions as expected
 */
TEST(EventTest, WindowMoved)
{
	//! Construct a WindowMoved object, pass it parameters of expected type
	Engine::WindowMoved moveConstructor(800, 600);

	//! Fixed width 32bit integer xAxis1 is initialised with its expected accessor function GetWindowPositionX using auto keyword to infer type from function call
	auto xAxis1 = moveConstructor.GetWindowPositionX();
	//! Fixed width 32bit integer yAxis1 is initialised with its expected accessor function GetWindowPositionY using auto keyword to infer type from function call
	auto yAxis1 = moveConstructor.GetWindowPositionY();

	//! glm::ivec2 object is initialised with its expected accessor function GetWindowPositionVector using auto keyword to infer type from function call
	auto position = moveConstructor.GetWindowPositionVector();
	//! Fixed width 32bit integer xAxis2 is initialised with its expected accessor for position's x axis parameter using auto keyword to infer type
	auto xAxis2 = position.x;
	//! Fixed width 32bit integer yAxis2 is initialised with its expected accessor for position's y axis parameter using auto keyword to infer type
	auto yAxis2 = position.y;

	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = moveConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = moveConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = moveConstructor.GetStaticEventType();

	// Perform a test for each initialised variable or object based on what is expected return value or type, using the appropriate GoogleTest macro
	EXPECT_EQ(xAxis1, 800);
	EXPECT_EQ(xAxis2, 800);
	EXPECT_EQ(yAxis1, 600);
	EXPECT_EQ(yAxis2, 600);
	EXPECT_EQ(eventCategory, Engine::EventCategories::WindowEvent);
	EXPECT_EQ(eventType, Engine::EventTypes::WindowMoved);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(moveConstructor.CategoryCheck(Engine::EventCategories::WindowEvent));
}