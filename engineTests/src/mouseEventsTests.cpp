/*!
 * \file mouseEventsTests.cpp
 */

#include "mouseEventsTests.h"

// MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled

TEST(MouseEvents, MouseButtonPressed)
{
	//! Construct a MouseButtonPressed object, pass some expected parameters, a buttoncode int32_t 
	Engine::MouseButtonPressed mouseButtonPressedConstructor(47);

	//! Fixed width 32bit integer testButtonCode is initialised with its expected accessor function GeMouseButtonCode using auto keyword to infer type from function call
	auto testButtonCode = mouseButtonPressedConstructor.GetMouseButtonCode();

	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = mouseButtonPressedConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = mouseButtonPressedConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = mouseButtonPressedConstructor.GetStaticEventType();

	// test macros
	EXPECT_EQ(testButtonCode, 47);
	EXPECT_EQ(eventCategory, (Engine::EventCategories::MouseDeltaEvent | Engine::EventCategories::MouseButtonEvent | Engine::EventCategories::InputEvent));
	EXPECT_EQ(eventType, Engine::EventTypes::MouseButtonPressed);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(mouseButtonPressedConstructor.CategoryCheck(Engine::EventCategories::MouseDeltaEvent | Engine::EventCategories::MouseButtonEvent | Engine::EventCategories::InputEvent));
};

TEST(MouseEvents, MouseButtonReleased)
{
	//! Construct a MouseButtonPressed object, pass some expected parameters, a buttoncode int32_t 
	Engine::MouseButtonReleased mouseButtonReleasedConstructor(23);

	//! Fixed width 32bit integer testButtonCode is initialised with its expected accessor function GeMouseButtonCode using auto keyword to infer type from function call
	auto testButtonCode = mouseButtonReleasedConstructor.GetMouseButtonCode();

	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = mouseButtonReleasedConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = mouseButtonReleasedConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = mouseButtonReleasedConstructor.GetStaticEventType();

	// test macros
	EXPECT_EQ(testButtonCode, 23);
	EXPECT_EQ(eventCategory, (Engine::EventCategories::MouseDeltaEvent | Engine::EventCategories::MouseButtonEvent | Engine::EventCategories::InputEvent));
	EXPECT_EQ(eventType, Engine::EventTypes::MouseButtonReleased);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(mouseButtonReleasedConstructor.CategoryCheck(Engine::EventCategories::MouseDeltaEvent | Engine::EventCategories::MouseButtonEvent | Engine::EventCategories::InputEvent));
};

TEST(MouseEvents, MouseMoved)
{
	//! Construct a  MouseMoved object, pass it parameters of expected type
	Engine::MouseMoved mouseMovedConstructor(23.47f, 47.23f);

	//! Fixed width 32bit integer xAxis1 is initialised with its expected accessor function GetMousePosDeltaX using auto keyword to infer type from function call
	auto xAxis1 = mouseMovedConstructor.GetMousePosDeltaX();
	//! Fixed width 32bit integer yAxis1 is initialised with its expected accessor function GetMousePosDeltaY using auto keyword to infer type from function call
	auto yAxis1 = mouseMovedConstructor.GetMousePosDeltaY();

	//! glm::ivec2 object is initialised with its expected accessor function GetMousePosDeltaVector using auto keyword to infer type from function call
	auto position = mouseMovedConstructor.GetMousePosDeltaVector();
	//! Fixed width 32bit integer xAxis2 is initialised with its expected accessor for position's x axis parameter using auto keyword to infer type
	auto xAxis2 = position.x;
	//! Fixed width 32bit integer yAxis2 is initialised with its expected accessor for position's y axis parameter using auto keyword to infer type
	auto yAxis2 = position.y;

	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = mouseMovedConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = mouseMovedConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = mouseMovedConstructor.GetStaticEventType();

	// Perform a test for each initialised variable or object based on what is expected return value or type, using the appropriate GoogleTest macro
	EXPECT_EQ(xAxis1, 23.47f);
	EXPECT_EQ(xAxis2, 23.47f);
	EXPECT_EQ(yAxis1, 47.23f);
	EXPECT_EQ(yAxis2, 47.23f);
	EXPECT_EQ(eventCategory, Engine::EventCategories::MouseDeltaEvent | Engine::EventCategories::MouseButtonEvent | Engine::EventCategories::InputEvent);
	EXPECT_EQ(eventType, Engine::EventTypes::MouseMoved);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(mouseMovedConstructor.CategoryCheck(Engine::EventCategories::MouseDeltaEvent | Engine::EventCategories::MouseButtonEvent | Engine::EventCategories::InputEvent));
}

TEST(MouseEvents, MouseScrolled)
{
	//! Construct a  MouseMoved object, pass it parameters of expected type
	Engine::MouseScrolled mouseScrolledConstructor(23.47f, 47.23f);

	//! Fixed width 32bit integer xAxis1 is initialised with its expected accessor function GetMousePosDeltaX using auto keyword to infer type from function call
	auto xAxis1 = mouseScrolledConstructor.GetMouseScrollDeltaX();
	//! Fixed width 32bit integer yAxis1 is initialised with its expected accessor function GetMousePosDeltaY using auto keyword to infer type from function call
	auto yAxis1 = mouseScrolledConstructor.GetMouseScrollDeltaY();

	//! glm::ivec2 object is initialised with its expected accessor function GetMousePosDeltaVector using auto keyword to infer type from function call
	auto delta = mouseScrolledConstructor.GetMouseScrollDeltaVector();
	//! Fixed width 32bit integer xAxis2 is initialised with its expected accessor for delta's x axis parameter using auto keyword to infer type
	auto xAxis2 = delta.x;
	//! Fixed width 32bit integer yAxis2 is initialised with its expected accessor for delta's y axis parameter using auto keyword to infer type
	auto yAxis2 = delta.y;

	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = mouseScrolledConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = mouseScrolledConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = mouseScrolledConstructor.GetStaticEventType();

	// Perform a test for each initialised variable or object based on what is expected return value or type, using the appropriate GoogleTest macro
	EXPECT_EQ(xAxis1, 23.47f);
	EXPECT_EQ(xAxis2, 23.47f);
	EXPECT_EQ(yAxis1, 47.23f);
	EXPECT_EQ(yAxis2, 47.23f);
	EXPECT_EQ(eventCategory, Engine::EventCategories::MouseDeltaEvent | Engine::EventCategories::MouseButtonEvent | Engine::EventCategories::InputEvent);
	EXPECT_EQ(eventType, Engine::EventTypes::MouseScrolled);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(mouseScrolledConstructor.CategoryCheck(Engine::EventCategories::MouseDeltaEvent | Engine::EventCategories::MouseButtonEvent | Engine::EventCategories::InputEvent));
}