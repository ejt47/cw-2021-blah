#include "eventHandlerTests.h"

TEST(EventHandler, OnWindowClose)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a WindowClosed event object to test event handling with
	Engine::WindowClosed windowClosed;

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = windowClosed.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowCloseCallback function's returned wrapped function, returning as a bool, 
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& closeCallbackDefault = testApplication.m_eventHandler.GetOnWindowCloseCallback();
	// Pass the windowClosed event to the closeCallbackDefault object to handle the event
	closeCallbackDefault(windowClosed);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon 
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = windowClosed.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/* 
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowCloseCallback function's returned wrapped function, returning a bool 
		(now initialised)
	*/
	auto& closeCallback = testApplication.m_eventHandler.GetOnWindowCloseCallback();
	// Pass the windowClosed event to the closeCallback object to handle the event
	closeCallback(windowClosed);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = windowClosed.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}

TEST(EventHandler, OnWindowResize)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a WindowResized event object to test event handling with
	Engine::WindowResized windowResized(23, 47);

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = windowResized.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowResizeCallback function's returned wrapped function, returning as a bool, 
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& resizeCallbackDefault = testApplication.m_eventHandler.GetOnWindowResizeCallback();
	// Pass the windowResized event to the resizeCallbackDefault object to handle the event
	resizeCallbackDefault(windowResized);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = windowResized.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowResizeCallback function's returned wrapped function, returning a bool 
		(now initialised)
	*/
	auto& resizeCallback = testApplication.m_eventHandler.GetOnWindowResizeCallback();
	// Pass the windowResized event to the resizeCallback object to handle the event
	resizeCallback(windowResized);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = windowResized.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}

TEST(EventHandler, OnWindowFocus)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a WindowFocus event object to test event handling with
	Engine::WindowFocus windowFocus;

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = windowFocus.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowFocusCallback function's returned wrapped function, returning as a bool, 
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& focusCallbackDefault = testApplication.m_eventHandler.GetOnWindowFocusCallback();
	// Pass the windowFocus event to the focusCallbackDefault object to handle the event
	focusCallbackDefault(windowFocus);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = windowFocus.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowFocusCallback function's returned wrapped function, returning a bool 
		(now initialised)
	*/
	auto& focusCallback = testApplication.m_eventHandler.GetOnWindowFocusCallback();
	// Pass the windowFocus event to the focusCallback object to handle the event
	focusCallback(windowFocus);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = windowFocus.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}

TEST(EventHandler, OnWindowLostFocus)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a WindowLostFocus event object to test event handling with
	Engine::WindowLostFocus windowLostFocus;

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = windowLostFocus.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowLostFocusCallback function's returned wrapped function, returning as a bool,
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& lostFocusCallbackDefault = testApplication.m_eventHandler.GetOnWindowLostFocusCallback();
	// Pass the windowLostFocus event to the lostFocusCallbackDefault object to handle the event
	lostFocusCallbackDefault(windowLostFocus);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = windowLostFocus.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowLostFocusCallback function's returned wrapped function, returning a bool 
		(now initialised).
	*/
	auto& lostFocusCallback = testApplication.m_eventHandler.GetOnWindowLostFocusCallback();
	// Pass the windowLostFocus event to the lostFocusCallback object to handle the event
	lostFocusCallback(windowLostFocus);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = windowLostFocus.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}

TEST(EventHandler, OnWindowMove)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a WindowMoved event object to test event handling with
	Engine::WindowMoved windowMoved(23, 47);

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = windowMoved.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowMoveCallback function's returned wrapped function, returning as a bool,
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& moveCallbackDefault = testApplication.m_eventHandler.GetOnWindowMoveCallback();
	// Pass the windowMoved event to the moveCallbackDefault object to handle the event
	moveCallbackDefault(windowMoved);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = windowMoved.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnWindowMoveCallback function's returned wrapped function, returning a bool
		(now initialised)
	*/
	auto& moveCallback = testApplication.m_eventHandler.GetOnWindowMoveCallback();
	// Pass the windowMoved event to the moveCallback object to handle the event
	moveCallback(windowMoved);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = windowMoved.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}

TEST(EventHandler, OnKeyPress)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a KeyPressed event object to test event handling with
	Engine::KeyPressed keyPressed(47, 23);

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = keyPressed.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnKeyPressCallback function's returned wrapped function, returning as a bool,
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& pressCallbackDefault = testApplication.m_eventHandler.GetOnKeyPressCallback();
	// Pass the keyPressed event to the pressCallbackDefault object to handle the event
	pressCallbackDefault(keyPressed);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = keyPressed.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnKeyPressCallback function's returned wrapped function, returning a bool
		(now initialised)
	*/
	auto& pressCallback = testApplication.m_eventHandler.GetOnKeyPressCallback();
	// Pass the keyPressed event to the pressCallback object to handle the event
	pressCallback(keyPressed);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = keyPressed.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}

TEST(EventHandler, OnKeyRelease)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a KeyReleased event object to test event handling with
	Engine::KeyReleased keyReleased(47);

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = keyReleased.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnKeyReleaseCallback function's returned wrapped function, returning as a bool,
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& releaseCallbackDefault = testApplication.m_eventHandler.GetOnKeyReleaseCallback();
	// Pass the keyReleased event to the releaseCallbackDefault object to handle the event
	releaseCallbackDefault(keyReleased);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = keyReleased.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnKeyReleaseCallback function's returned wrapped function, returning a bool
		(now initialised)
	*/
	auto& releaseCallback = testApplication.m_eventHandler.GetOnKeyReleaseCallback();
	// Pass the keyReleased event to the releaseCallback object to handle the event
	releaseCallback(keyReleased);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = keyReleased.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}

TEST(EventHandler, OnKeyTyped)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a KeyTyped event object to test event handling with
	Engine::KeyTyped keyTyped(47);

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = keyTyped.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnKeyTypedCallback function's returned wrapped function, returning as a bool,
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& typedCallbackDefault = testApplication.m_eventHandler.GetOnKeyTypedCallback();
	// Pass the keyTyped event to the typedCallbackDefault object to handle the event
	typedCallbackDefault(keyTyped);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = keyTyped.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnKeyTypedCallback function's returned wrapped function, returning a bool
		(now initialised)
	*/
	auto& typedCallback = testApplication.m_eventHandler.GetOnKeyTypedCallback();
	// Pass the keyTyped event to the typedCallback object to handle the event
	typedCallback(keyTyped);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = keyTyped.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}

TEST(EventHandler, OnMouseButtonPress)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a MouseButtonPressed event object to test event handling with
	Engine::MouseButtonPressed buttonPressed(47);

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = buttonPressed.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnMouseButtonPressCallback function's returned wrapped function, returning as a bool,
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& pressCallbackDefault = testApplication.m_eventHandler.GetOnMouseButtonPressCallback();
	// Pass the buttonPressed event to the pressCallbackDefault object to handle the event
	pressCallbackDefault(buttonPressed);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = buttonPressed.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnMouseButtonPressCallback function's returned wrapped function, returning a bool
		(now initialised)
	*/
	auto& pressCallback = testApplication.m_eventHandler.GetOnMouseButtonPressCallback();
	// Pass the buttonPressed event to the pressCallback object to handle the event
	pressCallback(buttonPressed);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = buttonPressed.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}

TEST(EventHandler, OnMouseButtonRelease)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a MouseButtonReleased event object to test event handling with
	Engine::MouseButtonReleased buttonReleased(47);

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = buttonReleased.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnMouseButtonReleaseCallback function's returned wrapped function, returning as a bool,
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& releaseCallbackDefault = testApplication.m_eventHandler.GetOnMouseButtonReleaseCallback();
	// Pass the buttonReleased event to the releaseCallbackDefault object to handle the event
	releaseCallbackDefault(buttonReleased);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = buttonReleased.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnMouseButtonReleaseCallback function's returned wrapped function, returning a bool
		(now initialised)
	*/
	auto& releaseCallback = testApplication.m_eventHandler.GetOnMouseButtonReleaseCallback();
	// Pass the buttonReleased event to the releaseCallback object to handle the event
	releaseCallback(buttonReleased);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = buttonReleased.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}

TEST(EventHandler, OnMouseMove)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a MouseMoved event object to test event handling with
	Engine::MouseMoved mouseMoved(47.23f, 23.47f);

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = mouseMoved.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnMouseMoveCallback function's returned wrapped function, returning as a bool,
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& moveCallbackDefault = testApplication.m_eventHandler.GetOnMouseMoveCallback();
	// Pass the mouseMoved event to the moveCallbackDefault object to handle the event
	moveCallbackDefault(mouseMoved);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = mouseMoved.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnMouseMoveCallback function's returned wrapped function, returning a bool
		(now initialised)
	*/
	auto& moveCallback = testApplication.m_eventHandler.GetOnMouseMoveCallback();
	// Pass the mouseMoved event to the moveCallback object to handle the event
	moveCallback(mouseMoved);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = mouseMoved.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}

TEST(EventHandler, OnMouseScroll)
{
	// Instantiate a test application object, for the event callback instance
	TestApplication testApplication;

	// Instantiate a MouseScrolled event object to test event handling with
	Engine::MouseScrolled mouseScrolled(47.23f, 23.47f);

	/*
		Bool variable to test the event behaviour, at this point should return false, as event object member variable m_bEventHandled is initialised as false upon event
		object initialisation, and event function has not yet flipped the m_bEventHandled member variable. Needs to be initialised before event callback functions are
		initialised, to mitigate potential interference on the expected bool value(false).
	*/
	auto bHandledBefore = mouseScrolled.EventHandled();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnMouseScrollCallback function's returned wrapped function, returning as a bool,
		the default behaviour which does nothing but return false(the initial state of the returned uninitialised wrapped function returning a bool)
	*/
	auto& scrollCallbackDefault = testApplication.m_eventHandler.GetOnMouseScrollCallback();
	// Pass the mouseScrolled event to the scrollCallbackDefault object to handle the event
	scrollCallbackDefault(mouseScrolled);

	/*
		Bool variable to test the default event behaviour, at this point should return the false, as event object member variable m_bEventHandled is initialised as false upon
		event object initialisation, and default behaviour does nothing to change this.
	*/
	auto bHandledDefault = mouseScrolled.EventHandled();

	// Initialise the event callback function through the InitEventCallbacks function (as this is a test performance unimportant but this function does init all event callbacks)
	testApplication.InitEventCallbacks();

	/*
		Instantiates a reference to an event function callback object, initialised to the GetOnMouseScrollCallback function's returned wrapped function, returning a bool
		(now initialised)
	*/
	auto& scrollCallback = testApplication.m_eventHandler.GetOnMouseScrollCallback();
	// Pass the mouseScrolled event to the scrollCallback object to handle the event
	scrollCallback(mouseScrolled);

	// Bool variable to test the event behaviour, at this point should return true after initialisation of event callback funtion through InitEventCallbacks function
	auto bHandledAfter = mouseScrolled.EventHandled();

	// Expect that bHandledDefault is equal to false
	EXPECT_EQ(bHandledDefault, false);
	// Expect that bHandledBefore is equal to false 
	EXPECT_EQ(bHandledBefore, false);
	// Expect that bHandledAfter is equal to true
	EXPECT_EQ(bHandledAfter, true);
	// Expect that bHandledBefore (false) is equal to bHandledDefault (false)
	EXPECT_EQ(bHandledBefore, bHandledDefault);
	// Expect that bHandledBefore (false) is not equal to bHandledAfter (true)
	EXPECT_NE(bHandledBefore, bHandledAfter);
}