/*! 
 * /file keyEventsTests.cpp
 */

#include "keyEventsTests.h"

// KeyEvent parent class (can't be tested directly as abstract class can't be instantiated, assumed correct if other classes work), KeyPressed, KeyReleased, KeyTyped

//! /test KeyPressed
TEST(KeyEvents, KeyPressed)
{
	//! Construct a KeyPressed object, pass some expected parameters, a keycode int32_t, and a presscount uint32_t
	Engine::KeyPressed keyPressedConstructor(23, 2);

	//! Fixed width 32bit integer testKeyCode is initialised with its expected accessor function GetKeyCode using auto keyword to infer type from function call
	auto testKeyCode = keyPressedConstructor.GetKeyCode();
	//! Fixed width 32bit integer testKeyCode is initialised with its expected accessor function GetKeyPressCount using auto keyword to infer type from function call
	auto testPressCount = keyPressedConstructor.GetKeyPressCount();

	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = keyPressedConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = keyPressedConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = keyPressedConstructor.GetStaticEventType();

	EXPECT_EQ(testKeyCode, 23);
	EXPECT_EQ(testPressCount, 2);
	EXPECT_EQ(eventCategory, (Engine::EventCategories::KeyboardEvent | Engine::EventCategories::InputEvent));
	EXPECT_EQ(eventType, Engine::EventTypes::KeyPressed);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(keyPressedConstructor.CategoryCheck((Engine::EventCategories::KeyboardEvent | Engine::EventCategories::InputEvent)));
}

//! /test KeyReleased
TEST(KeyEvents, KeyReleased)
{
	//! Construct a KeyReleased object, pass some expected parameters, a keycode int32_t, and a presscount uint32_t
	Engine::KeyReleased keyReleasedConstructor(23);

	//! Fixed width 32bit integer testKeyCode is initialised with its expected accessor function GetKeyCode using auto keyword to infer type from function call
	auto testKeyCode = keyReleasedConstructor.GetKeyCode();

	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = keyReleasedConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = keyReleasedConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = keyReleasedConstructor.GetStaticEventType();

	// test macros
	EXPECT_EQ(testKeyCode, 23);
	EXPECT_EQ(eventCategory, (Engine::EventCategories::KeyboardEvent | Engine::EventCategories::InputEvent));
	EXPECT_EQ(eventType, Engine::EventTypes::KeyReleased);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(keyReleasedConstructor.CategoryCheck((Engine::EventCategories::KeyboardEvent | Engine::EventCategories::InputEvent)));
}

//! /test KeyTyped
TEST(KeyEvents, KeyTyped)
{
	//! Construct a KeyTyped object, pass some expected parameters, a keycode int32_t, and a presscount uint32_t
	Engine::KeyTyped keyTypedConstructor(23);

	//! Fixed width 32bit integer testKeyCode is initialised with its expected accessor function GetKeyCode using auto keyword to infer type from function call
	auto testKeyCode = keyTypedConstructor.GetKeyCode();

	//! EventCategories enum eventCategory is initialised with its expected accessor function GetEventCategory using auto keyword to infer type from function call
	auto eventCategory = keyTypedConstructor.GetEventCategory();
	//! EventTypes enum class eventType is initialised with its expected accessor function GetEventType using auto keyword to infer type from function call
	auto eventType = keyTypedConstructor.GetEventType();
	//! EventTypes enum class staticeventType is initialised with its expected accessor function GetStaticEventType using auto keyword to infer type from function call
	auto staticEventType = keyTypedConstructor.GetStaticEventType();

	// test macros
	EXPECT_EQ(testKeyCode, 23);
	EXPECT_EQ(eventCategory, (Engine::EventCategories::KeyboardEvent | Engine::EventCategories::InputEvent));
	EXPECT_EQ(eventType, Engine::EventTypes::KeyTyped);
	EXPECT_EQ(eventType, staticEventType);
	EXPECT_TRUE(keyTypedConstructor.CategoryCheck((Engine::EventCategories::KeyboardEvent | Engine::EventCategories::InputEvent)));
}